LIBS := -lm -lpthread

C_SRCS += \
antichain.c \
benes.c \
canonical.c \
globals.c \
lattEnum.c \
lattice.c \
main.c \
permgrp.c \
permutation.c \
threaded.c 

OBJS += \
antichain.o \
benes.o \
canonical.o \
globals.o \
lattEnum.o \
lattice.o \
main.o \
permgrp.o \
permutation.o \
threaded.o 


%.o: %.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc $(CFLAGS) -O3 -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


all: UnlabelledLattices

UnlabelledLattices: $(OBJS) $(USER_OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: GCC C Linker'
	gcc  -o "UnlabelledLattices" $(CFLAGS) $(OBJS) $(USER_OBJS) $(LIBS)
	@echo 'Finished building target: $@'
	@echo ' '

clean:
	-$(RM) $(OBJS)$(C_DEPS)$(EXECUTABLES) UnlabelledLattices
	-@echo ' '

.PHONY: all clean dependents
