/*
 * lattEnum.c
 *
 * Created on: 22 Mar 2014
 * Last modified: 23 Feb 2019
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2019 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "lattEnum.h"
#include "antichain.h"


#ifdef FILTER_GARSIDE
#include "garside.h"
#endif

#ifdef THREADED
#include <pthread.h>
pthread_mutex_t  mutex_out = PTHREAD_MUTEX_INITIALIZER;
#endif



lattEnum* lattEnum_Count_create(lattice* L, elt N, elt Nmin, globals* GD)
/*
 * Return a structure for counting the descendants of *L of size equal to N, for which all intermediate lattices
 * have size at most Nmin.
 */
{
	lattEnum_Count*  E;

	if (L && (N-2 < L->n || Nmin-2 < L->n)) {
		printf("BAD PARAMETERS in lattEnum_Count_create: given lattice larger than target/intermediate size!\n");
		erri(-1);
	}
	E = (lattEnum_Count*)malloc(sizeof(lattEnum_Count));
	E->L = L;
	E->N = N-2;
	E->Nmin = Nmin-2;
	E->count = 0;
	E->GD = GD;
	E->reg = lattEnum_Count_register;
#ifdef COUNT_MAXIMAL_CHAINS
    E->max_mc = 0;
#endif
	return (lattEnum*)E;
}


lattEnum* lattEnum_stdout_create(lattice* L, elt N, elt Nmin, globals* GD)
/*
 * Return a structure for writing string representations of the descendants of *L of size N to stdout, for
 * which all intermediate lattices have size at most Nmin..
 */
{
	lattEnum_stdout*  E;

	if (L && (N-2 < L->n || Nmin-2 < L->n)) {
		printf("BAD PARAMETERS in lattEnum_stdout_create: given lattice larger than target/intermediate size!\n");
		erri(-1);
	}
	E = (lattEnum_stdout*)malloc(sizeof(lattEnum_stdout));
	E->L = L;
	E->N = N-2;
	E->Nmin = Nmin-2;
	E->count = 0;
	E->GD = GD;
	E->reg = lattEnum_stdout_register;
#ifdef THREADED
	E->output = (char*)malloc(THREADED_OUTPUT_BUFFER_SIZE);
	*(E->output) = 0;
	E->outpos = 0;
	E->outsize = THREADED_OUTPUT_BUFFER_SIZE;
#endif
	return (lattEnum*)E;
}


void lattEnum_stdout_flush(lattEnum_stdout* E)
/*
 * Flush the output buffer.
 */
{
#ifdef THREADED
	*(E->output+E->outpos) = 0;
	pthread_mutex_lock(&mutex_out);
	printf("%s", E->output);
	pthread_mutex_unlock(&mutex_out);
	E->outpos = 0;
#endif
}


void lattEnum_Count_register(lattEnum* E, lattice* L)
/*
 * Register the lattice L.
 */
{
	if (L->n == E->N) {
#ifdef FILTER_GARSIDE
		if (!possiblyGarside(L))
			return;
#endif
		E->count++;
#ifdef COUNT_MAXIMAL_CHAINS
		unsigned int mc = lattice_numberOfMaximalChains(L);
		lattEnum_Count* EE = (lattEnum_Count*)E;
		if (mc > EE->max_mc)
			EE->max_mc = mc;
#endif
	}
}


void lattEnum_stdout_register(lattEnum* E, lattice* L)
/*
 * Register the lattice L.
 */
{
	bool  valid;

#ifdef FILTER_GARSIDE
	valid = L->n == E->N && possiblyGarside(L);
#else
	valid = L->n == E->N;
#endif
	if (valid) {
    	E->count++;
#ifndef THREADED
		char  buffer[(MAXN*(MAXN-1))/2+1];

    	lattice_toString(buffer, L);
    	printf("%s\n", buffer);
#else
    	lattEnum_stdout* EE = (lattEnum_stdout*)E;

#if 0
    	if (EE->outpos+(L->n+2)*(L->n+1)/2+2 >= EE->outsize) {
			EE->outsize *= 2;
			EE->output = (char*)realloc(EE->output, EE->outsize);
		}
#else
    	if (EE->outpos+(L->n+2)*(L->n+1)/2+2 >= EE->outsize)
    		lattEnum_stdout_flush(EE);
#endif
    	lattice_toString(EE->output+EE->outpos, L);
    	EE->outpos += (L->n+2)*(L->n+1)/2;
    	*(EE->output+(EE->outpos++)) = '\n';
#endif
	}
}


void lattEnum_Count_free(lattEnum* E)
/*
 * Free all memory (and print the maximal number of maximal chains if enabled).
 */
{
#ifdef COUNT_MAXIMAL_CHAINS
	lattEnum_Count* EE = (lattEnum_Count*)E;
	printf("at most %u maximal chains\n", EE->max_mc);
#endif
	free((lattEnum_Count*)E);
}


void lattEnum_stdout_free(lattEnum* E)
/*
 * Free all memory (and flush buffer).
 */
{
	lattEnum_stdout*  EE = (lattEnum_stdout*)E;
#ifdef THREADED
	lattEnum_stdout_flush(EE);
	free(EE->output);
#endif
	free(EE);
}


void lattEnum_growLattice(lattEnum* E, elt N, lattice* L, elt nmin)
/*
 * Construct canonical lattices with N elements by recursively adding new levels to the canonical lattice L,
 * where the first added level contains at least nmin elements.  For every canonical lattice found during
 * this process (also for those with fewer than N elements), the function *reg is called.
 */
{
	lattice        LA;
	antichaindata  AD;
	elt            k;

#ifdef DOTEST
	if (!L->n) {
		printf("BAD CALL [lattEnum_growLattice]: argument is the lattice with 2 elements!\n");
		lattice_print(L);
	}
#endif

#ifdef VERBOSE
	printf("\n[>>> entering lattEnum_growLattice]:\n"); lattice_print(L);
#endif
	antichaindata_init(L, nmin, &AD, E->GD);

	antichaindata_prepareLattice(&AD, L, &LA);
	if (nmin == 1) {
#ifdef VERBOSE
		printf("\n=== adding a level with %d elements\n", 1);
#endif
#if defined(DOTEST) && defined(FILTER_INDECOMPOSABLE) && defined(FILTER_GRADED)
		printf("BAD CALL [lattEnum_growLattice]: asked for a graded indecomposable lattice with a level of size 1!\n");
#endif
		while (antichaindata_next_1(&AD)) {
			antichaindata_generateLattice_1(&AD, L, &LA);
#ifdef DOTEST
			if (!antichaindata_test(&AD)) {
				printf("BAD LATTICE-ANTICHAIN DATA\n");
				printf("\n### lattice:\n"); lattice_print(L);
				printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
				printf(">>> lattice:\n"); lattice_print(&LA);
				erri(-4);
			}
			if (!lattice_test(&LA)) {
				printf("INCONSISTENT LATTICE!\n");
				printf("\n### lattice:\n"); lattice_print(L);
				printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
				printf(">>> lattice:\n"); lattice_print(&LA);
				erri(-4);
			}
			if (!lattice_isCanonical(&LA)) {
				printf("NON-CANONICAL LATTICE!\n");
				printf("\n### lattice:\n"); lattice_print(L);
				printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
				printf(">>> lattice:\n"); lattice_print(&LA);
				erri(-4);
			}
#endif
#ifdef VERBOSE
			printf("\n### lattice:\n"); lattice_print(L);
			printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
			printf(">>> lattice:\n"); lattice_print(&LA);
#endif
#ifdef FILTER_INDECOMPOSABLE
			if (LA.up[L->n] != BIT(LA.n)-1) {
				(*(E->reg))(E, &LA);
#ifdef FILTER_GRADED
				if (LA.n < N-1)
					lattEnum_growLattice(E, N, &LA, 2);
#else
				if (LA.n < N)
					lattEnum_growLattice(E, N, &LA, 1);
#endif
			} else {
#ifdef VERBOSE
				printf("VERTICALLY DECOMPOSABLE; SKIPPING!\n");
#endif
			}
#else
			(*(E->reg))(E, &LA);
			if (LA.n < N)
				lattEnum_growLattice(E, N, &LA, 1);
#endif
			lattice_clearStabiliser(&LA);
#ifdef DOTEST
			if (!antichaindata_step_1_FUNC(&AD))
				break;
#else
			if (!antichaindata_step_1(&AD))
				break;
#endif
		}
		k = 2;
	} else {
		k = nmin;
	}

	for (; k<=N-L->n; k++) {
#ifdef VERBOSE
		printf("\n=== adding a level with %d elements\n", k);
#endif
		antichaindata_reinit(&AD, k);
		antichaindata_prepareLattice(&AD, L, &LA);
		while (antichaindata_next(&AD)) {
			antichaindata_generateLattice(&AD, L, &LA);
#ifdef DOTEST
			if (!antichaindata_test(&AD)) {
				printf("BAD LATTICE-ANTICHAIN DATA\n");
				printf("\n### lattice:\n"); lattice_print(L);
				printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
				printf(">>> lattice:\n"); lattice_print(&LA);
				erri(-4);
			}
			if (!lattice_test(&LA)) {
				printf("INCONSISTENT LATTICE!\n");
				printf("\n### lattice:\n"); lattice_print(L);
				printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
				printf(">>> lattice:\n"); lattice_print(&LA);
				erri(-4);
			}
			if (!lattice_isCanonical(&LA)) {
				printf("NON-CANONICAL LATTICE!\n");
				printf("\n### lattice:\n"); lattice_print(L);
				printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
				printf(">>> lattice:\n"); lattice_print(&LA);
				erri(-4);
			}
#endif
#ifdef VERBOSE
			printf("\n### lattice:\n"); lattice_print(L);
			printf("+++ antichain data: "); antichaindata_printAntichains(&AD);
			printf(">>> lattice:\n"); lattice_print(&LA);
#endif
			(*(E->reg))(E, &LA);
#if defined(FILTER_INDECOMPOSABLE) && defined(FILTER_GRADED)
			if (LA.n < N-1)
				lattEnum_growLattice(E, N, &LA, 2);
#else
			if (LA.n < N)
				lattEnum_growLattice(E, N, &LA, 1);
#endif
			lattice_clearStabiliser(&LA);
#ifdef DOTEST
			if (!antichaindata_step_FUNC(&AD))
				break;
#else
			if (!antichaindata_step(&AD))
				break;
#endif
		}
	}

	antichaindata_clear(&AD);
#ifdef VERBOSE
	printf("\n[<<< leaving lattEnum_growLattice]:\n"); lattice_print(L);
#endif
}


void lattEnum_growLattice_2(lattEnum* E, elt N, lattice* L, elt nmin)
/*
 * Construct canonical lattices with N elements by recursively adding new levels to the lattice L with 2
 * elements, where the first added level contains at least nmin elements.  For every canonical lattice
 * found during this process (also for those with fewer than N elements), the function *reg is called.
 *
 * The function assumes that *L is the lattice with 2 elements.
 */
{
	lattice  LA;
	elt      k;

#ifdef DOTEST
	if (L->n) {
		printf("BAD CALL [lattEnum_growLattice_2]: argument is not the lattice with 2 elements!\n");
		lattice_print(L);
	}
#endif

#ifdef FILTER_INDECOMPOSABLE
	for (k=nmin>1?nmin:2; k<=N-L->n; k++) {
#else
	for (k=nmin; k<=N-L->n; k++) {
#endif
		lattice_init_kFan(&LA, k);
		(*(E->reg))(E, &LA);
#if defined(FILTER_INDECOMPOSABLE) && defined(FILTER_GRADED)
			if (LA.n < N-1)
				lattEnum_growLattice(E, N, &LA, 2);
#else
		if (LA.n < N)
			lattEnum_growLattice(E, N, &LA, 1);
#endif
		lattice_clearStabiliser(&LA);
	}
}
