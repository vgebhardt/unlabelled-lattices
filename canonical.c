/*
 * canonical.c
 *
 * Created on: 1 Jun 2014
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include "canonical.h"


#if 0
#define PRINTLARGEORBITS
#define LARGEORBITTHRESHOLD 128

#endif


static inline unsigned long hash_1(flags64 A)
/*
 * Return the hash value of A.
 */
{
	unsigned int hash;

	hash = 0;
	while (A) {
		hash ^= A & ((1UL<<ORBITS_HASHTABLE_LD_SIZE)-1);
		A >>= ORBITS_HASHTABLE_LD_SIZE;
	}
	return hash;
}


static inline unsigned long hash_2(flags64 A[2])
/*
 * Return the hash value of A:B.
 */
{
	return hash_1(A[0]^A[1]);
}
#define HASH_1(A) hash_1(A)
#define HASH_2(A) hash_2(A)


static inline int flags64_cmp(flags64 A, flags64 B)
/*
 * Compare the weight of the antichains A and B.
 *
 * Return value: <0 if wt(A) < wt(B)
 *               0  if A = B
 *               >0 if wt(A) > wt(B)
 */
{
	if (A < B)
		return -1;
	else if (A > B)
		return 1;
	else
		return 0;
}


static inline int antichainList_cmp_p1(flags64 A, flags64 B)
/*
 * Compare in lexicographical order the packed antichain lists A and B.
 *
 * Note that the function knows nothing about the levellised ordering!  The idea is to use this
 * function on a list of antichains that only live on one level, and/or iteratively, starting at
 * the maximal level, with appropriate masking.
 *
 * Return value: <0 if A < B
 *               0  if A = B
 *               >0 if A > B
 */
{
	if (A < B)
		return -1;
	else if (A > B)
		return 1;
	else
		return 0;
}


static inline int antichainList_cmp_p2(flags64 A[2], flags64 B[2])
/*
 * Compare in lexicographical order the packed antichain lists A and B.
 *
 * Note that the function knows nothing about the levellised ordering!  The idea is to use this
 * function on a list of antichains that only live on one level, and/or iteratively, starting at
 * the maximal level, with appropriate masking.
 *
 * Return value: <0 if A < B
 *               0  if A = B
 *               >0 if A > B
 */
{
	if (A[0] < B[0])
		return -1;
	else if (A[0] > B[0])
		return 1;
	else
		if (A[1] < B[1])
			return -1;
		else if (A[1] > B[1])
			return 1;
		else
			return 0;
}


#ifdef DOTEST
static inline void antichain_apply_perm(elt lo, elt hi, permutation p, flags64 A, flags64* R)
/*
 * TEST FUNCTION: Store the result of applying the permutation p to the antichain A, containing the elements
 * lo..(hi-1) only, in *R.
 */
{
	elt  i;

	for (*R=0UL, i=lo; i<hi; i++)
		if (A & BIT(i))
			*R |= BIT(p[i]);
}


static inline void antichainList_apply_perm_p1(elt n, elt lo, elt hi, permutation p, elt k,	flags64 L, flags64* R)
/*
 * TEST FUNCTION: Store the result of applying the permutation p to the packed antichain list L containing
 * k antichains, involving only the elements lo..(hi-1), in the packed antichain list *R.  The action on
 * points points 0..n-1 corresponds to a permutation of the elements of the antichains; the action on points
 * n..n+k-1 corresponds to a permutation of the antichains.
 */
{
	elt  i, j, bits;

	bits = hi-lo;
	*R = 0UL;
	for (i=0; i<k; i++)
		for (j=0; j<bits; j++)
			if (L & (BIT(j) << (bits*(k-1-i))))
				*R |= BIT(p[lo+j]-lo) << (bits*(k-1-(p[n+i]-n)));
}


static inline void antichainList_apply_perm_p2(elt n, elt lo, elt hi, permutation p, elt k,	flags64 L[2], flags64 R[2])
/*
 * TEST FUNCTION: Store the result of applying the permutation p to the packed antichain list L containing
 * k antichains, involving only the elements lo..(hi-1), in the packed antichain list *R.  The action on
 * points points 0..n-1 corresponds to a permutation of the elements of the antichains; the action on points
 * n..n+k-1 corresponds to a permutation of the antichains.
 */
{
	elt  i, j, bits, apf, fi, oi, fpi, opi;

	bits = hi-lo;
	apf = BITSPERFLAGS64/bits;
	R[0] = R[1] = 0UL;
	for (i=0; i<k; i++) {
		fi = 1 - (k-1-i)/apf;
		oi = (k-1-i) % apf;
		fpi = 1 - (k-1-(p[n+i]-n))/apf;
		opi = (k-1-(p[n+i]-n)) % apf;
		for (j=0; j<bits; j++)
			if (L[fi] & (BIT(j) << (bits*oi)))
				R[fpi] |= BIT(p[lo+j]-lo) << (bits*opi);
	}
}
#endif


static inline void antichainList_sort_p1(elt k, elt m, flags64 mask[], flags32 bl, flags64* L, permutation p, elt offset)
/*
 * Sort the packed antichain list *L containing k antichains of size m in ascending order, subject to the
 * permitted swaps of adjacent antichains indicated by bl.  The permutation sorting L is also applied to
 * p[offset]..p[offset+k-1].  The array mask should contain ((1<<m)-1)<<((k-1)*m),..,((1<<m)-1).
 */
{
	flags64  t1, t2;
	elt*     pp;
	elt      tp;
	elt      newn, n, i, lo;

	if (!bl)
		return;
	lo = __builtin_ctz(bl);
	n = 32 - __builtin_clz(bl);
	pp = p+offset;
	do {
		newn = 0;
		for (i=lo; i<n; i++) {
			if ((bl & BIT(i)) && (t1=((*L&mask[i-1])>>m)) > (t2=(*L&mask[i]))) {
				t1 ^= t2;
				*L ^= ((t1<<m) | t1);
				tp = pp[i-1];
				pp[i-1] = pp[i];
				pp[i] = tp;
				newn = i;
			}
		}
		n = newn;
	} while (n);
}


static inline void antichainList_sort_p2(elt k, elt m, elt k0, flags64 M0[], flags64 M1[], flags32 bl, flags64 L[2],
		permutation p, elt offset)
/*
 * Sort the packed antichain list L containing k antichains of size m in ascending order, with L[0] containing
 * k0 antichains and L[1] containing k-k0 antichains, subject to the permitted swaps of adjacent antichains
 * indicated by bl.  The permutation sorting L is also applied to p[offset]..p[offset+k-1].  The arrays M0 and M1
 * should contain ((1<<m)-1)<<((k0-1)*m),..,((1<<m)-1) and ((1<<m)-1)<<((k-k0-1)*m),..,((1<<m)-1) respectively.
 */
{
	flags64  t1, t2;
	elt*     pp;
	elt      tp;
	elt      newn, n, n0, i, lo;

	if (!bl)
		return;
	lo = __builtin_ctz(bl);
	n = 32 - __builtin_clz(bl);
	pp = p+offset;
	do {
		newn = 0;
		if (lo < k0) {
			n0 = k0<n ? k0 : n;
			for (i=lo; i<n0; i++) {
				if ((bl & BIT(i)) && (t1=((L[0]&M0[i-1])>>m)) > (t2=(L[0]&M0[i]))) {
					t1 ^= t2;
					L[0] ^= ((t1<<m) | t1);
					tp = pp[i-1];
					pp[i-1] = pp[i];
					pp[i] = tp;
					newn = i;
				}
			}
		}
		if (lo <= k0 && k0 < n) {
			elt  shift;
			shift = (k-k0-1)*m;
			if ((bl & BIT(k0)) && (t1=((L[0]&M0[k0-1])<<shift)) > (t2=(L[1]&M1[0]))) {
				t1 ^= t2;
				L[0] ^= t1>>shift;
				L[1] ^= t1;
				tp = pp[k0-1];
				pp[k0-1] = pp[k0];
				pp[k0] = tp;
				newn = k0;
			}
		}
		if (k0+1 < n) {
			for (i=lo>k0?lo:k0+1; i<n; i++) {
				if ((bl & BIT(i)) && (t1=((L[1]&M1[i-k0-1])>>m)) > (t2=(L[1]&M1[i-k0]))) {
					t1 ^= t2;
					L[1] ^= ((t1<<m) | t1);
					tp = pp[i-1];
					pp[i-1] = pp[i];
					pp[i] = tp;
					newn = i;
				}
			}
		}
		n = newn;
	} while (n);
}


static inline void antichainList_applySI_1(elt a0, elt m, flags64* L, flags32 SI, flags32* SI_, permutation p)
/*
 * Minimise the packed antichain list *L containing one antichain on the elements a0..(a0+m-1) under the
 * action of the implicit automorphisms given by SI.  The inverse of the permutation minimising L is
 * left-multiplied to p, the rationale being to keep track of the permutation mapping L to some original
 * element.  If SI_ != 0, the implicit stabiliser of the minimised antichain *L is returned in *SI_.
 */
{
	flags32  mask, B, A_;
	elt      t, hi, lo;
	elt*     pp;

	mask = BIT(m) - 1;
	pp = p+a0;
	if (SI_)
		*SI_ = SI & ~(mask << a0);
	SI = (SI >> a0) & mask;
	B = SI ^ (SI >> 1);
	A_ = *L & ~(SI | B);  /* the elements in blocks of size 1 */
	while (extract_MSB32(&B, &hi)) {
		flags32  pmask, SB, UB;
		extract_MSB32(&B, &lo);
		pmask = BIT(hi+1)-BIT(lo);
		SB = *L & pmask;
		UB = ~*L & pmask;
		while (get_MSB32(SB,&hi) && get_LSB32(UB,&lo) && hi>lo) {
			SB ^= BIT(hi) | BIT(lo);
			UB ^= BIT(hi) | BIT(lo);
			t = pp[hi];  /* left-multiplication of p by the transposition (lo hi) */
			pp[hi] = pp[lo];
			pp[lo] = t;
		}
		A_ |= SB;
		if (SB && UB)
			SI ^= BIT(lo);
	}
	*L = A_;
	if (SI_)
		*SI_ |= SI << a0;
}


static inline void antichainList_applySI_p1(elt n, elt k, elt a0, elt m, flags32 bl, globals* GD, flags64* L, flags32 SI,
		flags64 M, permutation p)
/*
 * Minimise the packed antichain list *L containing k antichains on the elements a0..(a0+m-1) under the
 * action of the implicit automorphisms given by SI.  The inverse of the permutation minimising L is
 * left-multiplied to p, the rationale being to keep track of the permutation mapping L to some original
 * element.
 */
{
	SIdata*        SIt;
	unsigned long  SI0size, SI1size, i;
	elt            r, dr, j, dj, t;
	flags64        T;
	flags32        A, A_, A_min, B, S, mask;
	bool           next;
#ifdef DOTEST
	permutation    argpi, pt;
	elt            nLev;
	elt            lev[MAXN];
	flags64        MM[MAXN-2];
	flags64        tmask;
	perm_inv(n+k, p, argpi);
#endif

	mask = BIT(m) - 1;
	GD->SI0[0].rep[0] = *L;
	GD->SI0[0].S = (SI >> a0) & mask;
	perm_cpy(n+k, p, GD->SI0[0].p);
	SI0size = 1;
	for (r=k-1; r>=0; r-=dr) { /* r: position to be filled */
		SI1size = 0;
		A_min = mask;
		dr = 1;
		for (i=0; i<SI0size; i++) { /* loop over candidates */
			next = FALSE;
			for (j=r; !next; j-=dj) { /* j: position of the antichain under consideration */
				elt          hi, lo;
				flags64      P;
				permutation  q;
				elt*         pq = q+a0;
				if (SI1size == GD->SIspace)
					globals_enlargen_SIspace(GD);
				perm_cpy(n+k, GD->SI0[i].p, q);
				S = GD->SI0[i].S;
				P = GD->SI0[i].rep[0];
				A = (P >> j*m) & mask;  /* j: antichain under consideration */
				B = (S >> 1) ^ S;
				A_ = A & ~(S | B);  /* the elements in blocks of size 1 */
				while (A != A_ && extract_MSB32(&B, &hi)) {  /* get an orbit lo..hi ... */
					flags32  pmask, SB, UB;
					extract_MSB32(&B, &lo);
					pmask = BIT(hi+1)-BIT(lo);
					SB = A & pmask;
					UB = ~A & pmask;
					while (get_MSB32(SB,&hi) && get_LSB32(UB,&lo) && hi>lo) { /* ... if highest set > lowest unset: swap them */
						SB ^= BIT(hi) | BIT(lo);
						UB ^= BIT(hi) | BIT(lo);
						T = ((P >> hi) ^ (P >> lo)) & M;
						P ^= (T << hi) | (T << lo);
						t = pq[hi];  /* left-multiplication by (lo hi) */
						pq[hi] = pq[lo];
						pq[lo] = t;
					}
					A_ |= SB;
					if (SB && UB)
						S ^= BIT(lo);
					if (A_ > A_min)
						break;
				}
				dj = 0;
				do {
					if (dj >= j || !(bl & BIT(k-j+dj)))
						next = TRUE;
					dj++;
				} while (!next && ((P>>(j-dj)*m)&mask)==A_);
				if (A_ > A_min || (A_ == A_min && dj < dr))
					continue;  /* not minimal */
				if (A_ < A_min || (A_ == A_min && dj > dr)) {  /* new minimum! */
					A_min = A_;
					dr = dj;
					SI1size = 0;
				}
				perm_cpy(n+k, q, GD->SI1[SI1size].p);
				if (j < r) {  /* insert antichains (j-dr+1)..j at positions (r-dr+1)..r */
					flags64  mask1, mask2;
					elt*     pqq;
					mask1 = (BIT((r-j)*m)-1) << ((j+1)*m);
					mask2 = (BIT(dr*m)-1) << ((j-dr+1)*m);
					P = (P & ~(mask1 | mask2)) | ((P & mask1) >> (dr*m)) | ((P & mask2) << ((r-j)*m));
					/* left-multiply q=GD->SI1[SI1size].p by the inverse of the applied permutation */
					pq = q + n+k-1-j;
					pqq = GD->SI1[SI1size].p + n+k-1-r;
					for (t=dr; t--;)
						pqq[t] = pq[t];
					pq -= (r-j);  /* pq = q + n+k-1-r; */
					pqq += dr;   /* pqq = GD->SI1[SI1size].p + n+k-1-r+dr; */
					for (t=r-j; t--;)
						pqq[t] = pq[t];
				}
				GD->SI1[SI1size].S = S;
				GD->SI1[SI1size].rep[0] = P;
#ifdef DOTEST
				if (((P >> r*m) & mask) != A_min) {
					printf("BAD MINIMISATION [antichainList_applySI_p1]: minimising %lx\n", (unsigned long)(*L));
					printf("   have %lx\n", (unsigned long)P);
					printf("   expected %x (m=%d, r=%d, j=%d, S=%x)\n", A_min, m, r, j, S);
					erri(-4);
				}
				perm_mult(n+k, GD->SI1[SI1size].p, argpi, pt);
				antichainList_apply_perm_p1(n, a0, a0+m, pt, k, GD->SI1[SI1size].rep[0], &T);
				if (antichainList_cmp_p1(T, *L)) {
					printf("BAD ACTION [antichainList_applySI_p1]: "); perm_print(n+k, pt, 0);
					printf("   on %lx\n", (unsigned long)GD->SI1[SI1size].rep[0]);
					printf("   have %lx\n", (unsigned long)T);
					printf("   expected %lx\n", (unsigned long)(*L));
					erri(-4);
				}
#endif
				SI1size++;
			}
		}
		SI0size = SI1size;
		SIt = GD->SI0;
		GD->SI0 = GD->SI1;
		GD->SI1 = SIt;
	}
#ifdef DOTEST
	nLev = 0;
	for (tmask=BIT(m)-1,j=k; j--; tmask<<=m)
		MM[j] = tmask;
	for (j=1; j<=n; j++)
		if (!(SI & BIT(j)))
			lev[nLev++] = j;
	perm_init(n+k, pt);
	while (perm_next(nLev, lev, pt)) {
		antichainList_apply_perm_p1(n, a0, a0+m, pt, k, GD->SI0[0].rep[0], &T);
		antichainList_sort_p1(k, m, MM, bl, &T, p, a0);
		if (antichainList_cmp_p1(T, GD->SI0[0].rep[0]) < 0) {
			printf("BAD MINIMUM [antichainList_applySI_p1]: minimising %lx\n", (unsigned long)(*L));
			printf("   have %lx\n", (unsigned long)GD->SI1[0].rep[0]);
			printf("   but %lx is smaller (a0=%d, m=%d, k=%d, SI=%x, bl=%x)\n", (unsigned long)T, a0, m, k, SI, bl);
			erri(-4);
		}
	}
#endif
	*L = GD->SI0[0].rep[0];
	perm_cpy(n+k, GD->SI0[0].p, p);
	GD->SI0size = SI0size;
}


#ifdef TARGET_UTEST_SI
void antichainList_applySI_p1_f(elt n, elt k, elt a0, elt m, flags32 bl, globals* GD, flags64* L, flags32 SI,
		flags64 M, permutation p)
/*
 * Non-static wrapper for antichainList_applySI_p1.
 */
{
	antichainList_applySI_p1(n, k, a0, m, bl, GD, L, SI, M, p);
}
#endif


static inline void antichainList_applySI_p2(elt n, elt k, elt k1, elt a0, elt m, flags32 bl, globals* GD, flags64 L[2],
		flags32 SI, flags64 M, permutation p)
/*
 * Minimise the packed antichain list L containing k antichains on the elements a0..(a0+m-1), with L[0]
 * containing k-k1 antichains and L[1] containing k1 antichains,  under the action of the implicit
 * automorphisms given by SI.  The inverse of the permutation minimising L is left-multiplied to p, the
 * rationale being to keep track of the permutation mapping L to some original element.
 */
{
	SIdata*        SIt;
	unsigned long  SI0size, SI1size, i;
	elt            r, dr, j, dj, t;
	flags64        T;
	flags32        A, A_, A_min, B, S, mask;
	bool           next;
#ifdef DOTEST
	permutation    argpi, pt;
	elt            nLev, apf;
	elt            lev[MAXN];
	flags64*       M0;
	flags64        M1[MAXN-2];
	flags64        TT[2];
	perm_inv(n+k, p, argpi);
#endif

#define MIN(a,b)  ((a)<(b) ? (a) : (b))
#define MAX(a,b)  ((a)>(b) ? (a) : (b))
#define ANTICHAIN(L,i,m,k1,mask) /* access macro for the antichain in *position* i */          \
		((((i)>=(k1)) ? ((L)[0] >> (((i)-(k1))*(m))) : ((L)[1] >> ((i)*(m)))) & mask)

	mask = BIT(m) - 1;
	GD->SI0[0].rep[0] = L[0];
	GD->SI0[0].rep[1] = L[1];
	GD->SI0[0].S = (SI >> a0) & mask;
	perm_cpy(n+k, p, GD->SI0[0].p);
	SI0size = 1;
	for (r=k-1; r>=0; r-=dr) { /* r: position to be filled */
		SI1size = 0;
		A_min = mask;
		dr = 1;
		for (i=0; i<SI0size; i++) { /* loop over candidates */
			next = FALSE;
			for (j=r; !next; j-=dj) { /* j: position of the antichain under consideration */
				elt          hi, lo;
				flags64      P[2];
				permutation  q;
				elt*         pq = q+a0;
				if (SI1size == GD->SIspace)
					globals_enlargen_SIspace(GD);
				perm_cpy(n+k, GD->SI0[i].p, q);
				S = GD->SI0[i].S;
				P[0] = GD->SI0[i].rep[0];
				P[1] = GD->SI0[i].rep[1];
				A = ANTICHAIN(P,j,m,k1,mask);   /* j: antichain under consideration */
				B = (S >> 1) ^ S;
				A_ = A & ~(S | B);  /* the elements in blocks of size 1 */
				while (A != A_ && extract_MSB32(&B, &hi)) {  /* get an orbit lo..hi ... */
					flags32  pmask, SB, UB;
					extract_MSB32(&B, &lo);
					pmask = BIT(hi+1)-BIT(lo);
					SB = A & pmask;
					UB = ~A & pmask;
					while (get_MSB32(SB,&hi) && get_LSB32(UB,&lo) && hi>lo) { /* ... if highest set > lowest unset: swap them */
						SB ^= BIT(hi) | BIT(lo);
						UB ^= BIT(hi) | BIT(lo);
						T = ((P[0] >> hi) ^ (P[0] >> lo)) & M;
						P[0] ^= (T << hi) | (T << lo);
						T = ((P[1] >> hi) ^ (P[1] >> lo)) & M;
						P[1] ^= (T << hi) | (T << lo);
						t = pq[hi];  /* left-multiplication by (lo hi) */
						pq[hi] = pq[lo];
						pq[lo] = t;
					}
					A_ |= SB;
					if (SB && UB)
						S ^= BIT(lo);
					if (A_ > A_min)
						break;
				}
				dj = 0;
				do {
					if (dj >= j || !(bl & BIT(k-j+dj)))
						next = TRUE;
					dj++;
				} while (!next && ANTICHAIN(P,j-dj,m,k1,mask)==A_);
				if (A_ > A_min || (A_ == A_min && dj < dr))
					continue;  /* not minimal */
				if (A_ < A_min || (A_ == A_min && dj > dr)) {  /* new minimum! */
					A_min = A_;
					dr = dj;
					SI1size = 0;
				}
				perm_cpy(n+k, q, GD->SI1[SI1size].p);
				if (j < r) {  /* move antichains j..(j-dr+1) to positions r..(r-dr+1), shifting r..j+1 to the right
				               *  - move j..(j-dr+1) by r-j positions to the left
				               *  - move r..(j+1) by dr positions to the right
				               */
					elt*  pqq;
					if (j < k1) {
						if (r < k1) {
							flags64  MR11, ML11;
							ML11 = (BIT(dr*m)-1) << ((j-dr+1)*m);
							MR11 = (BIT((r-j)*m)-1) << ((j+1)*m);
							P[1] = (P[1] & ~(ML11 | MR11)) | ((P[1] & ML11) << ((r-j)*m)) | ((P[1] & MR11) >> (dr*m));
						} else {
							flags64  MR00, MR01, MR11, ML10, ML11;
							if (r-dr >= k1) {
								MR00 = (BIT((r-k1-dr+1)*m)-1) << (dr*m);
								MR01 = BIT(dr*m)-1;
							} else {
								MR00 = 0UL;
								MR01 = BIT((r-k1+1)*m)-1;
							}
							MR11 = (BIT((k1-j-1)*m)-1) << (j+1)*m;
							if (r-k1+1 >= dr) {
								ML10 = (BIT(dr*m)-1) << (j-dr+1)*m;
								ML11 = 0UL;
							} else {
								ML10 = (BIT((r-k1+1)*m)-1) << (j-r+k1)*m;
								ML11 = (BIT((dr-(r-k1+1))*m)-1) << (j-dr+1)*m;
							}
							T = (P[0] & ~(MR00|MR01))
									| ((P[0] & MR00) >> (dr*m))
									| (r-j-k1>0 ? ((P[1] & ML10) << ((r-j-k1)*m)) : ((P[1] & ML10) >> (-(r-j-k1)*m)));
							P[1] = (P[1] & ~(MR11|ML10|ML11))
									| ((P[1] & MR11) >> (dr*m))
									| ((P[1] & ML11) << ((r-j)*m))
									| (dr-k1>0 ? ((P[0] & MR01) >> ((dr-k1)*m)) : ((P[0] & MR01) << (-(dr-k1)*m)));
							P[0] = T;
						}
					} else {
						if (j-dr+1 < k1) {
							flags64  MR00, MR01, ML00, ML10, ML11;
							MR00 = (BIT((r+1-k1-dr)*m)-1) << (dr*m);
							MR01 = (BIT((k1+dr-j-1)*m)-1) << ((j+1-k1)*m);
							ML00 = BIT((j-k1+1)*m)-1;
							if (r-dr+1-k1 >= 0) {
								ML10 = ((BIT((k1-1-j+dr)*m)-1) << ((j-dr+1)*m));
								ML11 = 0UL;
							} else {
								ML10 = ((BIT((r-j)*m)-1) << (k1-r+j)*m);
								ML11 = ((BIT(k1-r+dr-1)*m)-1) << ((j-dr+1)*m);
							}
							T = (P[0] & ~(MR00|MR01|ML00))
									| ((P[0] & MR00) >> (dr*m))
									| ((P[0] & ML00) << ((r-j)*m))
									| (r-j-k1>0 ? ((P[1] & ML10) << ((r-j-k1)*m)) : ((P[1] & ML10) >> (-(r-j-k1)*m)));
							P[1] = (P[1] & ~(ML10|ML11))
									| ((P[1] & ML11) << ((r-j)*m))
									| (dr-k1>0 ? ((P[0] & MR01) >> ((dr-k1)*m)) : ((P[0] & MR01) << (-(dr-k1)*m)));
							P[0] = T;
						} else {
							flags64  MR00, ML00;
							ML00 = (BIT(dr*m)-1) << ((j-dr+1-k1)*m);
							MR00 = (BIT((r-j)*m)-1) << ((j+1-k1)*m);
							P[0] = (P[0] & ~(ML00 | MR00)) | ((P[0] & ML00) << ((r-j)*m)) | ((P[0] & MR00) >> (dr*m));
						}
					}
					/* left-multiply q=GD->SI1[SI1size].p by the inverse of the applied permutation */
					pq = q + n+k-1-j;
					pqq = GD->SI1[SI1size].p + n+k-1-r;
					for (t=dr; t--;)
						pqq[t] = pq[t];
					pq -= (r-j);  /* pq = q + n+k-1-r; */
					pqq += dr;   /* pqq = GD->SI1[SI1size].p + n+k-1-r+dr; */
					for (t=r-j; t--;)
						pqq[t] = pq[t];
				}
				GD->SI1[SI1size].S = S;
				GD->SI1[SI1size].rep[0] = P[0];
				GD->SI1[SI1size].rep[1] = P[1];
#ifdef DOTEST
				if (ANTICHAIN(P,r,m,k1,mask) != A_min) {
					printf("BAD MINIMISATION [antichainList_applySI_p2]: minimising %lx|%lx\n",
							(unsigned long)L[0], (unsigned long)L[1]);
					printf("   have %lx|%lx\n", (unsigned long)P[0], (unsigned long)P[1]);
					printf("   expected %x (m=%d, r=%d, j=%d, S=%x)\n", A_min, m, r, j, S);
					erri(-4);
				}
				perm_mult(n+k, GD->SI1[SI1size].p, argpi, pt);
				antichainList_apply_perm_p2(n, a0, a0+m, pt, k, GD->SI1[SI1size].rep, TT);
				if (antichainList_cmp_p2(TT, L)) {
					printf("BAD ACTION [antichainList_applySI_p2]: "); perm_print(n+k, pt, 0);
					printf("   on %lx|%lx\n", (unsigned long)GD->SI1[SI1size].rep[0], (unsigned long)GD->SI1[SI1size].rep[1]);
					printf("   have %lx|%lx\n", (unsigned long)TT[0], (unsigned long)TT[1]);
					printf("   expected %lx|%lx\n", (unsigned long)L[0], (unsigned long)L[1]);
					erri(-4);
				}
#endif
				SI1size++;
			}
		}
		SI0size = SI1size;
		SIt = GD->SI0;
		GD->SI0 = GD->SI1;
		GD->SI1 = SIt;
	}
#ifdef DOTEST
	apf = BITSPERFLAGS64/m;
	for (mask=BIT(m)-1,i=apf; i--; mask<<=m)
		M1[i] = mask;
	M0 = M1 + 2*apf-k;
	nLev = 0;
	for (j=1; j<=n; j++)
		if (!(SI & BIT(j)))
			lev[nLev++] = j;
	perm_init(n+k, pt);
	while (perm_next(nLev, lev, pt)) {
		antichainList_apply_perm_p2(n, a0, a0+m, pt, k, GD->SI0[0].rep, TT);
		antichainList_sort_p2(k, m, k-k1, M0, M1, bl, TT, p, a0);
		if (antichainList_cmp_p2(TT, GD->SI0[0].rep) < 0) {
			printf("BAD MINIMUM [antichainList_applySI_p2]: minimising %lx|%lx\n", (unsigned long)L[0], (unsigned long)L[1]);
			printf("   have %lx|%lx\n", (unsigned long)GD->SI1[0].rep[0], (unsigned long)GD->SI1[0].rep[1]);
			printf("   but %lx|%lx is smaller (a0=%d, m=%d, k=%d, SI=%x, bl=%x)\n",
					(unsigned long)TT[0], (unsigned long)TT[1], a0, m, k, SI, bl);
			erri(-4);
		}
	}
#endif
	L[0] = GD->SI0[0].rep[0];
	L[1] = GD->SI0[0].rep[1];
	perm_cpy(n+k, GD->SI0[0].p, p);
	GD->SI0size = SI0size;
#undef ANTICHAIN
#undef MIN
#undef MAX
}


#ifdef TARGET_UTEST_SI
void antichainList_applySI_p2_f(elt n, elt k, elt k0, elt a0, elt m, flags32 bl, globals* GD, flags64 L[2],
		flags32 SI, flags64 M, permutation p)
/*
 * Non-static wrapper for antichainList_applySI_p2.
 */
{
	antichainList_applySI_p2(n, k, k0, a0, m, bl, GD, L, SI, M, p);
}
#endif


static inline void antichainList_extractStabiliser_p1(elt n, elt k, elt lo, elt hi, globals* GD,
		permgrpc* S, flags32* SI, flags64 L)
/*
 * After a call to antichainList_applySI_p1 that showed that its argument L is minimal under the action of the implicit
 * stabiliser:  Add to S any generator of the stabiliser of L that arises from different ways of reaching the minimal
 * element, and set *SI to the implicit stabiliser of L.  (L is only used in testing mode.)
 */
{
	unsigned long  i;
	permutation    p;
#ifdef DOTEST
	flags64        L_;
#endif

	*SI &= ~(BIT(hi)-BIT(lo));
	*SI |= GD->SI0[0].S << lo;
	for (i=1; i<GD->SI0size; i++) {
		perm_ldiv(n+k, GD->SI0[0].p, GD->SI0[i].p, p);
		if (!perm_isId(n+k, p)) {
#ifdef VERBOSE
			printf("[antichainList_extractStabiliser_p1]: adding stabiliser generator "); perm_print(S->G->n, p, 0);
#endif
			permgrpc_addGenerator(S, p);
#ifdef DOTEST
			antichainList_apply_perm_p1(n, lo, hi, p, k, L, &L_);
			if (antichainList_cmp_p1(L, L_)) {
				/* We don't test here whether the action on the lowest level is correct; this is done
				 * in lattice_test after the new lattice is created.
				 */
				printf("BAD STABILISER ELEMENT [antichainList_extractStabiliser_p1]: ");
				perm_print(n+k, p, 0);
				erri(-4);
			}
#endif
		}
	}
}


static inline void antichainList_extractStabiliser_p2(elt n, elt k, elt lo, elt hi, globals* GD,
		permgrpc* S, flags32* SI, flags64 L[2])
/*
 * After a call to antichainList_applySI_p2 that showed that its argument L is minimal under the action of the implicit
 * stabiliser:  Add to S any generator of the stabiliser of L that arises from different ways of reaching the minimal
 * element, and set *SI to the implicit stabiliser of L.  (L is only used in testing mode.)
 */
{
	unsigned long  i;
	permutation    p;
#ifdef DOTEST
	flags64        L_[2];
#endif

	*SI &= ~(BIT(hi)-BIT(lo));
	*SI |= GD->SI0[0].S << lo;
	for (i=1; i<GD->SI0size; i++) {
		perm_ldiv(n+k, GD->SI0[0].p, GD->SI0[i].p, p);
		if (!perm_isId(n+k, p)) {
#ifdef VERBOSE
			printf("[antichainList_extractStabiliser_p2]: adding stabiliser generator "); perm_print(S->G->n, p, 0);
#endif
			permgrpc_addGenerator(S, p);
#ifdef DOTEST
			antichainList_apply_perm_p2(n, lo, hi, p, k, L, L_);
			if (antichainList_cmp_p2(L, L_)) {
				/* We don't test here whether the action on the lowest level is correct; this is done
				 * in lattice_test after the new lattice is created.
				 */
				printf("BAD STABILISER ELEMENT [antichainList_extractStabiliser_p2]: ");
				perm_print(n+k, p, 0);
				erri(-4);
			}
#endif
		}
	}
}


static inline void permgrpc_compactGenerators(permgrpc* G)
/*
 * Make sure that the generators are stored consecutively in perm, and record which generators are involutions.
 */
{
	static const flags32  allbits = (1<<(MAXN-2))-1;
	elt      i, j;
	flags32  biti;

	while (get_LSB32(G->freeperm,&i) && get_MSB32(allbits^G->freeperm,&j) && i<j) {
		perm_cpy(G->G->n, G->G->perm[j], G->G->perm[i]);
		perm_cpy(G->G->n, G->G->invperm[j], G->G->invperm[i]);
		G->freeperm ^= BIT(i);  /* bit i is set, so this clears it */
		G->freeperm |= BIT(j);
	}
	get_LSB32(G->freeperm,&(G->G->ngens));
	G->G->invol = 0;
	for (i=0,biti=1; i<G->G->ngens; i++,biti<<=1)
		if (!perm_cmp(G->G->n, G->G->perm[i], G->G->invperm[i]))
			G->G->invol |= biti;
}


static inline void permgrp_preprocessGenerators(antichaindata* AD)
/*
 * Generate Beneš networks for the action of the generators on the next level (AD->cl-1)
 * of the current lattice or, if AD->cl==0, the lowest non-trivial level of the new lattice.
 */
{
	elt       i;
	lattice*  L;
	permgrp*  G;

	L = AD->L;
	G = AD->SD[AD_CSL].ST;
#ifndef FILTER_GRADED
	if (AD_CSL) {
		for (i=0; i<G->ngens; i++)
			benes_get(G->perm[i], G->invperm[i], L->lev[AD->cl-1], L->lev[AD->cl], &(G->Benes[AD_CSL-1][i]));
		G->BenesValid |= BIT(AD_CSL-1);
	} else {
		for (i=0; i<G->ngens; i++)
			benes_get(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev], &(G->Benes[L->nLev-1][i]));
		G->BenesValid |= BIT(L->nLev-1);
	}
#else
	for (i=0; i<G->ngens; i++)
		benes_get(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev], &(G->Benes[0][i]));
	G->BenesValid |= BIT(0);
#endif
}


static inline void permgrp_preprocessGenerators_blocked(antichaindata* AD)
/*
 * Generate Beneš networks for the action of the generators on the next level (AD->cl-1)
 * of the current lattice as well as for the action on AD->k antichains of the appropriate
 * size or, if AD->cl==0, the lowest non-trivial level of the new lattice.
 */
{
	elt       i;
	lattice*  L;
	permgrp*  G;

	L = AD->L;
	G = AD->SD[AD_CSL].ST;
#ifndef FILTER_GRADED
	if (AD_CSL) {
		if (G->BenesValid & BIT(L->nLev-1)) {
			for (i=0; i<G->ngens; i++) {
				benes_get(G->perm[i], G->invperm[i], L->lev[AD->cl-1], L->lev[AD->cl], &(G->Benes[AD_CSL-1][i]));
				benes_delete(G->Benes[L->nLev-1][i]);
				benes_get_blocked(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev],
						L->lev[AD->cl]-L->lev[AD->cl-1], &(G->Benes[L->nLev-1][i]));
			}
			G->BenesValid |= BIT(AD_CSL-1);
		} else {
			for (i=0; i<G->ngens; i++) {
				benes_get(G->perm[i], G->invperm[i], L->lev[AD->cl-1], L->lev[AD->cl], &(G->Benes[AD_CSL-1][i]));
				benes_get_blocked(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev],
						L->lev[AD->cl]-L->lev[AD->cl-1], &(G->Benes[L->nLev-1][i]));
			}
			G->BenesValid |= BIT(AD_CSL-1) | BIT(L->nLev-1);
		}
	} else {
		if (G->BenesValid & BIT(L->nLev-1)) {
			for (i=0; i<G->ngens; i++) {
				benes_delete(G->Benes[L->nLev-1][i]);
				benes_get(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev], &(G->Benes[L->nLev-1][i]));
			}
		} else {
			for (i=0; i<G->ngens; i++)
				benes_get(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev], &(G->Benes[L->nLev-1][i]));
			G->BenesValid |= BIT(L->nLev-1);
		}
	}
#else
	if (G->BenesValid & BIT(0)) {
		for (i=0; i<G->ngens; i++) {
			benes_delete(G->Benes[0][i]);
			benes_get(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev], &(G->Benes[0][i]));
		}
	} else {
		for (i=0; i<G->ngens; i++)
			benes_get(G->perm[i], G->invperm[i], L->lev[L->nLev-1], L->lev[L->nLev], &(G->Benes[0][i]));
		G->BenesValid |= BIT(0);
	}
#endif
}


static inline void processElement_1(antichaindata* AD, permgrp* G, permgrpc* S, unsigned long pos, elt gen)
/*
 * Do the housekeeping for a newly computed orbit element.  Special case of a single antichain.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The points on the lowest level of the new lattice correspond to the antichains in that sequence; if a
 * permutation fixes the multiset but permutes its elements, the points of the lowest level of the new
 * lattice need to be permuted accordingly to obtain an element of the stabiliser of the new lattice.
 */
{
	flags64        A;
	unsigned long  Apos;
	permutation    h;

	A = AD->GD->orb[AD->GD->orbsize].data[0];
	Apos = AD->GD->orbsize;
	/* check whether the element is new... */
	if (hashtable_query_insert_1(AD->GD->orbpos, A, HASH_1(A), &Apos)) {
		/* ...if not, note the new stabiliser element */
		if (pos) {
			if (Apos)
				perm_ldiv_mult(S->G->n, AD->GD->orb[pos].toRoot, G->perm[gen], AD->GD->orb[Apos].toRoot, h);
			else
				perm_ldiv(S->G->n, AD->GD->orb[pos].toRoot, G->perm[gen], h);
		} else {
			if (Apos)  /* equivalent to if (AD->GD->orbsort[lpos]) */
				perm_mult(S->G->n, G->perm[gen], AD->GD->orb[Apos].toRoot, h);
			else
				perm_cpy(S->G->n, G->perm[gen], h);
		}
#ifdef DOTEST
		if (AD->GD->orbspace == AD->GD->orbsize)
			globals_enlargen_orbitspace(AD->GD);
		antichain_apply_perm(AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], h, AD->GD->orb[0].data[0] << AD->L->lev[AD->cl],
				AD->GD->orb[AD->GD->orbsize].data);
		if (flags64_cmp(AD->GD->orb[0].data[0] << AD->L->lev[AD->cl], AD->GD->orb[AD->GD->orbsize].data[0])) {
			/* We don't test here whether the action on the lowest level is correct; this is done
			 * in lattice_test after the new lattice is created.
			 */
			printf("BAD STABILISER ELEMENT [processElement_1]: ");
			perm_print(S->G->n, h, 0);
			erri(-4);
		}
#endif
#ifdef VERBOSE
		printf("[processElement_1]: adding stabiliser generator "); perm_print(S->G->n, h, 0);
#endif
		if (!perm_isId(S->G->n, h))
			permgrpc_addGenerator(S, h);
	} else {
		/* ...if yes, note the permutation to the root and the applied generator */
		AD->GD->orb[AD->GD->orbsize].gen = gen;
		if (pos)
			perm_mult(S->G->n, G->invperm[gen], AD->GD->orb[pos].toRoot, AD->GD->orb[AD->GD->orbsize].toRoot);
		else
			perm_cpy(S->G->n, G->invperm[gen], AD->GD->orb[AD->GD->orbsize].toRoot);
		AD->GD->orbsize++;
	}
}


static inline void processElement_p1(antichaindata* AD, permgrp* G, permgrpc* S, unsigned long pos, elt gen, permutation p)
/*
 * Do the housekeeping for a newly computed orbit element.  Special case of a list of antichains that,
 * if packed, fits into one flags64.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The points on the lowest level of the new lattice correspond to the antichains in that sequence; if a
 * permutation fixes the multiset but permutes its elements, the points of the lowest level of the new
 * lattice need to be permuted accordingly to obtain an element of the stabiliser of the new lattice.
 */
{
	flags64        A;
	unsigned long  Apos;
	permutation    h;

	A = AD->GD->orb[AD->GD->orbsize].data[0];
	Apos = AD->GD->orbsize;
	/* check whether the element is new... */
	if (hashtable_query_insert_1(AD->GD->orbpos, A, HASH_1(A), &Apos)) {
		/* ...if no, note the new stabiliser element */
		if (pos) {
			if (Apos)
				perm_mult3_ldiv(S->G->n, p, G->invperm[gen], AD->GD->orb[pos].toRoot,
						AD->GD->orb[Apos].toRoot, h);
			else
				perm_mult3_inv(S->G->n, p, G->invperm[gen], AD->GD->orb[pos].toRoot, h);
		} else {
			if (Apos)
				perm_mult_ldiv(S->G->n, p, G->invperm[gen], AD->GD->orb[Apos].toRoot, h);
			else
				perm_mult_inv(S->G->n, p, G->invperm[gen], h);
		}
#ifdef DOTEST
		if (AD->GD->orbspace == AD->GD->orbsize)
			globals_enlargen_orbitspace(AD->GD);
		antichainList_apply_perm_p1(AD->L->n, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], h, AD->k,
				AD->GD->orb[0].data[0], AD->GD->orb[AD->GD->orbsize].data);
		if (antichainList_cmp_p1(AD->GD->orb[0].data[0], AD->GD->orb[AD->GD->orbsize].data[0])) {
			/* We don't test here whether the action on the lowest level is correct; this is done
			 * in lattice_test after the new lattice is created.
			 */
			printf("BAD STABILISER ELEMENT [processElement_p1]: ");
			perm_print(S->G->n, h, 0);
			erri(-4);
		}
#endif
#ifdef VERBOSE
		printf("[processElement_p1]: adding stabiliser generator "); perm_print(S->G->n, h, 0);
#endif
		if (!perm_isId(S->G->n, h))
			permgrpc_addGenerator(S, h);
	} else {
		/* ...if yes, note the permutation to the root and the applied generator */
		AD->GD->orb[AD->GD->orbsize].gen = gen;
		if (pos)
			perm_mult3(S->G->n, p, G->invperm[gen], AD->GD->orb[pos].toRoot,	AD->GD->orb[AD->GD->orbsize].toRoot);
		else
			perm_mult(S->G->n, p, G->invperm[gen], AD->GD->orb[AD->GD->orbsize].toRoot);
		AD->GD->orbsize++;
#ifdef DOTEST
		if (AD->GD->orbspace == AD->GD->orbsize)
			globals_enlargen_orbitspace(AD->GD);
		antichainList_apply_perm_p1(AD->L->n, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], AD->GD->orb[AD->GD->orbsize-1].toRoot,
				AD->k, AD->GD->orb[AD->GD->orbsize-1].data[0], AD->GD->orb[AD->GD->orbsize].data);
		if (antichainList_cmp_p1(AD->GD->orb[0].data[0], AD->GD->orb[AD->GD->orbsize].data[0])) {
			/* We don't test here whether the action on the lowest level is correct; this is done
			 * in lattice_test after the new lattice is created.
			 */
			printf("BAD ACTION [processElement_p1]: ");
			perm_print(S->G->n, AD->GD->orb[AD->GD->orbsize-1].toRoot, 0);
			erri(-4);
		}
#endif
	}
}


static inline void processElement_p2(antichaindata* AD, permgrp* G, permgrpc* S, unsigned long pos, elt gen,
		permutation p)
/*
 * Do the housekeeping for a newly computed orbit element.    Special case of a list of antichains that,
 * if packed, fits into (and requires) two flags64.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The points on the lowest level of the new lattice correspond to the antichains in that sequence; if a
 * permutation fixes the multiset but permutes its elements, the points of the lowest level of the new
 * lattice need to be permuted accordingly to obtain an element of the stabiliser of the new lattice.
 */
{
	flags64*       A;
	unsigned long  Apos;
	permutation    h;

	A = AD->GD->orb[AD->GD->orbsize].data;
	Apos = AD->GD->orbsize;
	/* check whether the element is new... */
	if (hashtable_query_insert_2(AD->GD->orbpos, A, HASH_2(A), &Apos)) {
		/* ...if no, note the new stabiliser element */
		if (pos) {
			if (Apos)
				perm_mult3_ldiv(S->G->n, p, G->invperm[gen], AD->GD->orb[pos].toRoot,
						AD->GD->orb[Apos].toRoot, h);
			else
				perm_mult3_inv(S->G->n, p, G->invperm[gen], AD->GD->orb[pos].toRoot, h);
		} else {
			if (Apos)
				perm_mult_ldiv(S->G->n, p, G->invperm[gen], AD->GD->orb[Apos].toRoot, h);
			else
				perm_mult_inv(S->G->n, p, G->invperm[gen], h);
		}
#ifdef DOTEST
		if (AD->GD->orbspace == AD->GD->orbsize)
			globals_enlargen_orbitspace(AD->GD);
		antichainList_apply_perm_p2(AD->L->n, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], h, AD->k, AD->GD->orb[0].data,
				AD->GD->orb[AD->GD->orbsize].data);
		if (antichainList_cmp_p2(AD->GD->orb[0].data, AD->GD->orb[AD->GD->orbsize].data)) {
			/* We don't test here whether the action on the lowest level is correct; this is done
			 * in lattice_test after the new lattice is created.
			 */
			printf("BAD STABILISER ELEMENT [processElement_p2]: ");
			perm_print(S->G->n, h, 0);
			erri(-4);
		}
#endif
#ifdef VERBOSE
		printf("[processElement_p2]: adding stabiliser generator "); perm_print(S->G->n, h, 0);
#endif
		if (!perm_isId(S->G->n, h))
			permgrpc_addGenerator(S, h);
	} else {
		/* ...if yes, note the permutation to the root and the applied generator */
		AD->GD->orb[AD->GD->orbsize].gen = gen;
		if (pos)
			perm_mult3(S->G->n, p, G->invperm[gen], AD->GD->orb[pos].toRoot,	AD->GD->orb[AD->GD->orbsize].toRoot);
		else
			perm_mult(S->G->n, p, G->invperm[gen], AD->GD->orb[AD->GD->orbsize].toRoot);
		AD->GD->orbsize++;
#ifdef DOTEST
		if (AD->GD->orbspace == AD->GD->orbsize)
			globals_enlargen_orbitspace(AD->GD);
		antichainList_apply_perm_p2(AD->L->n, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], AD->GD->orb[AD->GD->orbsize-1].toRoot,
				AD->k, AD->GD->orb[AD->GD->orbsize-1].data, AD->GD->orb[AD->GD->orbsize].data);
		if (antichainList_cmp_p2(AD->GD->orb[0].data, AD->GD->orb[AD->GD->orbsize].data)) {
			/* We don't test here whether the action on the lowest level is correct; this is done
			 * in lattice_test after the new lattice is created.
			 */
			printf("BAD ACTION [processElement_p2]: ");
			perm_print(S->G->n, AD->GD->orb[AD->GD->orbsize-1].toRoot, 0);
			erri(-4);
		}
#endif
	}
}


bool antichaindata_isCanonical_1(antichaindata* AD)
/*
 * Whether the antichain data given by AD may yield a canonical antichain list.  Special case of a single
 * antichain.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The function checks whether the multiset of sets chosen on the current level (AD->O[...] & AD->cmc) is
 * minimal under the action of the current stabiliser (AD->SD[AD->cl+1].ST).  If yes, and if the antichain
 * data is not complete (that is, AD->cl > 0]), then the new stabiliser is stored in AD->SD[AD->cl].ST.  If
 * the antichain data is complete and canonical, AD->SD[0].ST will instead be set to the stabiliser of the
 * new lattice obtained from the antichain data.  (The points on the lowest (new) level of the new lattice
 * correspond to the antichains in that sequence; if a permutation fixes the multiset but permutes its
 * elements, the points of the lowest level of the new lattice need to be permuted accordingly to obtain
 * an element of the stabiliser of the new lattice.  Additional generators of the stabiliser of the new
 * lattice are given by all possible permutations of identical antichains, i.e., all possible permutations
 * of points of the lowest level of the new lattice that have identical covering sets.)
 */
{
	unsigned long  pos;
	elt            gen;
	permgrp*       G;
	permgrpc*      S;

#ifdef VERBOSE
	printf("[entering antichaindata_isCanonical_1]:\n"); lattice_print(AD->L); antichaindata_printCounters(AD);
#endif
	G = AD->SD[AD_CSL+1].ST;
#ifdef DOTEST
	if (G->ngens && !(G->BenesValid & BIT(AD_CSL))) {
		printf("ATTEMPTS TO USE INVALID BENEŠ NETWORKS [antichaindata_isCanonical_1]: level %d\n", AD_CSL);
		erri(-4);
	}
#endif
	if (G->ngens > 0) {
		if ((AD->GD->orb[0].data[0] = (AD->O[0] & AD->cmc) >> AD->L->lev[AD->cl])) {
			if (AD->SD[AD_CSL+1].SI) {
				/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
				permutation  p;
				flags64      L;
				L = AD->GD->orb[0].data[0];
				perm_init(AD->L->n+AD->k, p);
				antichainList_applySI_1(AD->L->lev[AD->cl], AD->L->lev[AD->cl+1]-AD->L->lev[AD->cl], &L, AD->SD[AD_CSL+1].SI,
						&(AD->SD[AD_CSL].SI), p);
				if (L != AD->GD->orb[0].data[0])
					return FALSE;
				S = antichaindata_ensureStabiliser(AD,AD_CSL);
				AD->SD[AD_CSL].ST = S->G;
				permgrpc_init(S, AD->L->n+AD->k);
				/* now spin up the orbit of representatives under the action of the (old) implicit stabiliser */
				AD->GD->orbsize = 1;
				AD->GD->orb[0].gen = -1;
				hashtable_clear(AD->GD->orbpos);
				hashtable_insert_1(AD->GD->orbpos, L, HASH_1(L), 0);
#ifndef HARDCODE_MAXN_22
				if (S->G->n > G->n)
					for (gen=0; gen<G->ngens; gen++) {
						perm_resize(G->n, S->G->n, G->perm[gen]);
						perm_resize(G->n, S->G->n, G->invperm[gen]);
					}
#endif
				for (pos=0; pos<AD->GD->orbsize; pos++) {
					for (gen=0; gen<G->ngens; gen++) {
						flags64  A;
						if ((G->invol & BIT(gen)) && AD->GD->orb[pos].gen == gen)
							continue;
						/* apply generator gen to orbit element pos... */
						if (AD->GD->orbspace == AD->GD->orbsize)
							globals_enlargen_orbitspace(AD->GD);
						A = AD->GD->orb[pos].data[0];
						benes_apply_p1(G->Benes[AD_CSL][gen], &A);
						perm_init(AD->L->n+AD->k, p);
						antichainList_applySI_1(AD->L->lev[AD->cl], AD->L->lev[AD->cl+1]-AD->L->lev[AD->cl], &A,
								AD->SD[AD_CSL+1].SI, 0, p);
						/* ...we're done if the result is smaller than the original element */
						if (flags64_cmp(L, A) > 0) {
#ifdef VERBOSE
							printf("                                       NOT canonical\n");
#endif
#ifdef PRINTLARGEORBITS
							if (AD->GD->orbsize > LARGEORBITTHRESHOLD)
								printf("antichaindata_isCanonical_1: %lu [FALSE]\n", AD->GD->orbsize);
#endif
							return FALSE;
						}
						/* ...otherwise, do the necessary housekeeping */
						AD->GD->orb[AD->GD->orbsize].data[0] = A;
						processElement_p1(AD, G, S, pos, gen, p);
					}
				}
				permgrpc_compactGenerators(S);
			} else {
				/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
				permutation  p;
				flags64      L;
				L = AD->GD->orb[0].data[0];
				perm_init(AD->L->n+AD->k, p);
				AD->SD[AD_CSL].SI = 0;
				S = antichaindata_ensureStabiliser(AD,AD_CSL);
				AD->SD[AD_CSL].ST = S->G;
				permgrpc_init(S, AD->L->n+AD->k);
				/* now spin up the orbit of representatives under the action of the (old) implicit stabiliser */
				AD->GD->orbsize = 1;
				AD->GD->orb[0].gen = -1;
				hashtable_clear(AD->GD->orbpos);
				hashtable_insert_1(AD->GD->orbpos, L, HASH_1(L), 0);
#ifndef HARDCODE_MAXN_22
				if (S->G->n > G->n)
					for (gen=0; gen<G->ngens; gen++) {
						perm_resize(G->n, S->G->n, G->perm[gen]);
						perm_resize(G->n, S->G->n, G->invperm[gen]);
					}
#endif
				for (pos=0; pos<AD->GD->orbsize; pos++) {
					for (gen=0; gen<G->ngens; gen++) {
						flags64  A;
						if ((G->invol & BIT(gen)) && AD->GD->orb[pos].gen == gen)
							continue;
						/* apply generator gen to orbit element pos... */
						if (AD->GD->orbspace == AD->GD->orbsize)
							globals_enlargen_orbitspace(AD->GD);
						A = AD->GD->orb[pos].data[0];
						benes_apply_p1(G->Benes[AD_CSL][gen], &A);
						perm_init(AD->L->n+AD->k, p);
						/* ...we're done if the result is smaller than the original element */
						if (flags64_cmp(L, A) > 0) {
#ifdef VERBOSE
							printf("                                       NOT canonical\n");
#endif
#ifdef PRINTLARGEORBITS
							if (AD->GD->orbsize > LARGEORBITTHRESHOLD)
								printf("antichaindata_isCanonical_1: %lu [FALSE]\n", AD->GD->orbsize);
#endif
							return FALSE;
						}
						/* ...otherwise, do the necessary housekeeping */
						AD->GD->orb[AD->GD->orbsize].data[0] = A;
						if (AD->SD[AD_CSL+1].SI)
							processElement_p1(AD, G, S, pos, gen, p);
						else
							processElement_1(AD, G, S, pos, gen);
					}
				}
				permgrpc_compactGenerators(S);
			}
		} else { /* as the antichains must intersect the lowest level, AD->cl < AD->L->nLev-2, so G->n == S->n */
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
		}
	} else if (AD->SD[AD_CSL+1].SI) {
		if ((AD->GD->orb[0].data[0] = (AD->O[0] & AD->cmc) >> AD->L->lev[AD->cl])) {
			/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
			permutation  p;
			flags64      L;
			L = AD->GD->orb[0].data[0];
			perm_init(AD->L->n+AD->k, p);
			antichainList_applySI_1(AD->L->lev[AD->cl], AD->L->lev[AD->cl+1]-AD->L->lev[AD->cl], &L, AD->SD[AD_CSL+1].SI,
					&(AD->SD[AD_CSL].SI), p);
			if (L != AD->GD->orb[0].data[0])
				return FALSE;
			S = antichaindata_ensureStabiliser(AD,AD_CSL);
			AD->SD[AD_CSL].ST = S->G;
			permgrpc_init(S, AD->L->n+AD->k);
		} else { /* as the antichains must intersect the lowest level, AD->cl < AD->L->nLev-2, so G->n == S->n */
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
		}
	} else {
#ifndef FILTER_GRADED
		if (AD->cl == AD->L->nLev-2) {
#endif
			S = antichaindata_ensureStabiliser(AD,AD_CSL);
			AD->SD[AD_CSL].ST = S->G;
			permgrpc_init(S, AD->L->n+AD->k);
#ifndef FILTER_GRADED
		} else {
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
		}
#endif
		AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
	}
	permgrp_preprocessGenerators(AD);
#ifdef VERBOSE
	printf("***** level %d:\n", AD->cl);
	permgrp_printGenerators(AD->SD[AD_CSL].ST, 0);
	printf("                                       canonical\n");
#endif
#ifdef PRINTLARGEORBITS
	if (AD->GD->orbsize > LARGEORBITTHRESHOLD)
		printf("antichaindata_isCanonical_1: %lu [TRUE]\n", AD->GD->orbsize);
#endif
	return TRUE;
}


bool antichaindata_isCanonical_p1(antichaindata* AD, elt bits)
/*
 * Whether the antichain data given by AD may yield a canonical antichain list.  Special case of a list
 * that, if packed, fits into one flags64.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The function checks whether the multiset of sets chosen on the current level (AD->O[...] & AD->cmc) is
 * minimal under the action of the current stabiliser (AD->SD[AD->cl+1].ST and AD->SD[AD->cl+1].SI).  If yes,
 * and if the antichain data is not complete (that is, AD->cl > 0]), then the new stabiliser is stored in
 * AD->SD[AD->cl].ST and AD->SD[AD->cl].SI.  If the antichain data is complete and canonical, AD->SD[0].ST and
 * AD->SD[0].SI will instead be set to the stabiliser of the new lattice obtained from the antichain data.
 * (The points on the lowest (new) level of the new lattice correspond to the antichains in that sequence;
 * if a permutation fixes the multiset but permutes its elements, the points of the lowest level of the
 * new lattice need to be permuted accordingly to obtain an element of the stabiliser of the new lattice.
 * Additional generators of the stabiliser of the new lattice are given by all possible permutations of
 * identical antichains, i.e., all possible permutations of points of the lowest level of the new lattice
 * that have identical covering sets.  The latter may be handled by "implicit" stabilisers.)
 */
{
	elt            i, gen;
	unsigned long  pos;
	flags64        M[MAXN-2];
	flags64        mask, pmask;
	permgrp*       G;
	permgrpc*      S;

#ifdef VERBOSE
	printf("[entering antichaindata_isCanonical_p1]:\n"); lattice_print(AD->L); antichaindata_printCounters(AD);
#endif
	for (mask=BIT(bits)-1,i=AD->k; i--; mask<<=bits)
		M[i] = mask;
	G = AD->SD[AD_CSL+1].ST;
#ifdef DOTEST
	if (G->ngens && !(G->BenesValid & BIT(AD_CSL))) {
		printf("ATTEMPTS TO USE INVALID BENEŠ NETWORKS [antichaindata_isCanonical_1]: level %d\n", AD_CSL);
		erri(-4);
	}
#endif
	if (G->ngens > 0) {
		AD->GD->orb[0].data[0] = 0;
		for(i=0; i<AD->k; i++) {
			AD->GD->orb[0].data[0] <<= bits;
			AD->GD->orb[0].data[0] |= (AD->O[i] & AD->cmc) >> AD->L->lev[AD->cl];
		}
		if (AD->GD->orb[0].data[0]) {
			if (AD->SD[AD_CSL+1].SI) {
				/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
				permutation  p;
				flags64      L;
				L = AD->GD->orb[0].data[0];
				perm_init(AD->L->n+AD->k, p);
				for (pmask=1,i=AD->k; i--;)
					pmask = (pmask << bits) | 1;
				antichainList_applySI_p1(AD->L->n, AD->k, AD->L->lev[AD->cl], bits, AD->SD[AD_CSL+1].bl, AD->GD, &L,
						AD->SD[AD_CSL+1].SI, pmask, p);
				if (antichainList_cmp_p1(L, AD->GD->orb[0].data[0])) {
					/* determine the position up to which we can backtrack */
					elt      m, pi;
					flags64  D;
					m = i = 0;
					D = L ^ AD->GD->orb[0].data[0];
					do {
						pi = p[AD->L->n+i] - AD->L->n;
						if (pi > m)
							m = pi;
					} while (!(D & M[i++]));
					AD->Fpos += m - AD->cp;
					AD->cp = m;
#ifdef VERBOSE
					perm_print(AD->L->n+AD->k, p, 0);
					printf("                                       NOT canonical --> %d\n", AD->cp);
#endif
					return FALSE;
				}
				S = antichaindata_ensureStabiliser(AD,AD_CSL);
				AD->SD[AD_CSL].ST = S->G;
				permgrpc_init(S, AD->L->n+AD->k);
				AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
				antichainList_extractStabiliser_p1(AD->L->n, AD->k, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], AD->GD,
						S, &(AD->SD[AD_CSL].SI), L);
				/* now spin up the orbit of representatives under the action of the (old) implicit stabiliser */
				AD->GD->orbsize = 1;
				AD->GD->orb[0].gen = -1;
				perm_init(S->G->n, AD->GD->orb[0].toRoot);
				hashtable_clear(AD->GD->orbpos);
				hashtable_insert_1(AD->GD->orbpos, L, HASH_1(L), 0);
	#ifndef HARDCODE_MAXN_22
				if (S->G->n > G->n)
					for (gen=0; gen<G->ngens; gen++) {
						perm_resize(G->n, S->G->n, G->perm[gen]);
						perm_resize(G->n, S->G->n, G->invperm[gen]);
					}
	#endif
				for (pos=0; pos<AD->GD->orbsize; pos++) {
					for (gen=0; gen<G->ngens; gen++) {
						flags64      A;
						permutation  p;
						if ((G->invol & BIT(gen)) && AD->GD->orb[pos].gen == gen)
							continue;
						/* apply generator gen to orbit element pos... */
						if (AD->GD->orbspace == AD->GD->orbsize)
							globals_enlargen_orbitspace(AD->GD);
						A = AD->GD->orb[pos].data[0];
						benes_apply_p1(G->Benes[AD_CSL][gen], &A);
#ifndef FILTER_GRADED
						if (AD->cl < AD->L->nLev-2)
							benes_apply_blocked_p1(G->Benes[AD->L->nLev-1][gen], &A);
#endif
						perm_init(S->G->n, p);
						antichainList_applySI_p1(AD->L->n, AD->k, AD->L->lev[AD->cl], bits, AD->SD[AD_CSL+1].bl, AD->GD, &A,
								AD->SD[AD_CSL+1].SI, pmask, p);
						/* ...we're done if the result is smaller than the original element */
						if (antichainList_cmp_p1(L, A) > 0) {
							/* determine the position up to which we can backtrack */
							elt      m, pi;
							flags64  D;
							m = i = 0;
							D = L ^ A;
							do {
								pi = AD->GD->orb[pos].toRoot[G->invperm[gen][p[AD->L->n+i]]] - AD->L->n;
								if (pi > m)
									m = pi;
							} while (!(D & M[i++]));
							AD->Fpos += m - AD->cp;
							AD->cp = m;
	#ifdef VERBOSE
							perm_print(S->G->n, p, 0);
							printf("                                       NOT canonical --> %d\n", AD->cp);
	#endif
	#ifdef PRINTLARGEORBITS
							if (AD->GD->orbsize > LARGEORBITTHRESHOLD) {
								printf("antichaindata_isCanonical_p1: %lu [FALSE]\n", AD->GD->orbsize);
								lattice_print(AD->L);
								antichaindata_printCounters(AD);
							}
	#endif
							return FALSE;
						}
						/* ...otherwise, do the necessary housekeeping */
						AD->GD->orb[AD->GD->orbsize].data[0] = A;
						processElement_p1(AD, G, S, pos, gen, p);
					}
				}
				permgrpc_compactGenerators(S);
			} else {
				/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
				permutation  p;
				flags64      L;
				L = AD->GD->orb[0].data[0];
				perm_init(AD->L->n+AD->k, p);
				S = antichaindata_ensureStabiliser(AD,AD_CSL);
				AD->SD[AD_CSL].ST = S->G;
				permgrpc_init(S, AD->L->n+AD->k);
				AD->SD[AD_CSL].SI = 0;
				/* now spin up the orbit of representatives under the action of the (old) implicit stabiliser */
				AD->GD->orbsize = 1;
				AD->GD->orb[0].gen = -1;
				perm_init(S->G->n, AD->GD->orb[0].toRoot);
				hashtable_clear(AD->GD->orbpos);
				hashtable_insert_1(AD->GD->orbpos, L, HASH_1(L), 0);
	#ifndef HARDCODE_MAXN_22
				if (S->G->n > G->n)
					for (gen=0; gen<G->ngens; gen++) {
						perm_resize(G->n, S->G->n, G->perm[gen]);
						perm_resize(G->n, S->G->n, G->invperm[gen]);
					}
	#endif
				for (pos=0; pos<AD->GD->orbsize; pos++) {
					for (gen=0; gen<G->ngens; gen++) {
						flags64      A;
						permutation  p;
						if ((G->invol & BIT(gen)) && AD->GD->orb[pos].gen == gen)
							continue;
						/* apply generator gen to orbit element pos... */
						if (AD->GD->orbspace == AD->GD->orbsize)
							globals_enlargen_orbitspace(AD->GD);
						A = AD->GD->orb[pos].data[0];
						benes_apply_p1(G->Benes[AD_CSL][gen], &A);
#ifndef FILTER_GRADED
						if (AD->cl < AD->L->nLev-2)
							benes_apply_blocked_p1(G->Benes[AD->L->nLev-1][gen], &A);
#endif
						perm_init(S->G->n, p);
						antichainList_sort_p1(AD->k, bits, M, AD->SD[AD_CSL+1].bl, &A, p, AD->L->n);
						/* ...we're done if the result is smaller than the original element */
						if (antichainList_cmp_p1(L, A) > 0) {
							/* determine the position up to which we can backtrack */
							elt      m, pi;
							flags64  D;
							m = i = 0;
							D = L ^ A;
							do {
								pi = AD->GD->orb[pos].toRoot[G->invperm[gen][p[AD->L->n+i]]] - AD->L->n;
								if (pi > m)
									m = pi;
							} while (!(D & M[i++]));
							AD->Fpos += m - AD->cp;
							AD->cp = m;
	#ifdef VERBOSE
							perm_print(S->G->n, p, 0);
							printf("                                       NOT canonical --> %d\n", AD->cp);
	#endif
	#ifdef PRINTLARGEORBITS
							if (AD->GD->orbsize > LARGEORBITTHRESHOLD) {
								printf("antichaindata_isCanonical_p1: %lu [FALSE]\n", AD->GD->orbsize);
								lattice_print(AD->L);
								antichaindata_printCounters(AD);
							}
	#endif
							return FALSE;
						}
						/* ...otherwise, do the necessary housekeeping */
						AD->GD->orb[AD->GD->orbsize].data[0] = A;
						processElement_p1(AD, G, S, pos, gen, p);
					}
				}
				permgrpc_compactGenerators(S);
			}
		} else { /* as the antichains must intersect the lowest level, AD->cl < AD->L->nLev-2 */
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
		}
	} else if (AD->SD[AD_CSL+1].SI) {
		AD->GD->orb[0].data[0] = 0;
		for(i=0; i<AD->k; i++) {
			AD->GD->orb[0].data[0] <<= bits;
			AD->GD->orb[0].data[0] |= (AD->O[i] & AD->cmc) >> AD->L->lev[AD->cl];
		}
		if (AD->GD->orb[0].data[0]) {
			/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
			permutation  p;
			flags64      L;
			L = AD->GD->orb[0].data[0];
			perm_init(AD->L->n+AD->k, p);
			for (pmask=1,i=AD->k; i--;)
				pmask = (pmask << bits) | 1;
			antichainList_applySI_p1(AD->L->n, AD->k, AD->L->lev[AD->cl], bits, AD->SD[AD_CSL+1].bl, AD->GD, &L,
					AD->SD[AD_CSL+1].SI, pmask, p);
			if (antichainList_cmp_p1(L, AD->GD->orb[0].data[0])) {
				/* determine the position up to which we can backtrack */
				elt      m, pi;
				flags64  D;
				m = i = 0;
				D = L ^ AD->GD->orb[0].data[0];
				do {
					pi = p[AD->L->n+i] - AD->L->n;
					if (pi > m)
						m = pi;
				} while (!(D & M[i++]));
				AD->Fpos += m - AD->cp;
				AD->cp = m;
#ifdef VERBOSE
				perm_print(AD->L->n+AD->k, p, 0);
				printf("                                       NOT canonical --> %d\n", AD->cp);
#endif
				return FALSE;
			}
			S = antichaindata_ensureStabiliser(AD,AD_CSL);
			AD->SD[AD_CSL].ST = S->G;
			permgrpc_init(S, AD->L->n+AD->k);
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
			antichainList_extractStabiliser_p1(AD->L->n, AD->k, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], AD->GD,
					S, &(AD->SD[AD_CSL].SI), L);
			permgrpc_compactGenerators(S);
		} else { /* as the antichains must intersect the lowest level, AD->cl < AD->L->nLev-2 */
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
		}
	} else {
#ifndef FILTER_GRADED
		if (AD->cl == AD->L->nLev-2) {
#endif
			S = antichaindata_ensureStabiliser(AD,AD_CSL);
			AD->SD[AD_CSL].ST = S->G;
			permgrpc_init(S, AD->L->n+AD->k);
#ifndef FILTER_GRADED
		} else {
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
		}
#endif
		AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
	}
#ifndef FILTER_GRADED
	if (AD_CSL == 0) {
#endif
		antichaindata_updateBlocks(AD, AD->SD[1].bl, &(AD->SD[0].bl));
		AD->SD[0].SI |= AD->SD[0].bl << AD->L->n;
#ifndef FILTER_GRADED
	}
#endif
	permgrp_preprocessGenerators_blocked(AD);
#ifdef VERBOSE
	printf("***** level %d:\n", AD->cl);
	permgrp_printGenerators(AD->SD[AD_CSL].ST, 0);
	printf("                                       canonical\n");
#endif
#ifdef PRINTLARGEORBITS
	if (AD->GD->orbsize > LARGEORBITTHRESHOLD) {
		printf("antichaindata_isCanonical_p1: %lu [TRUE]\n", AD->GD->orbsize);
		lattice_print(AD->L);
		antichaindata_printCounters(AD);
	}
#endif
	return TRUE;
}


bool antichaindata_isCanonical_p2(antichaindata* AD, elt bits)
/*
 * Whether the antichain data given by AD may yield a canonical antichain list.  Special case of a list
 * that, if packed, fits into (and requires) two flags64.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The function checks whether the multiset of sets chosen on the current level (AD->O[...] & AD->cmc) is
 * minimal under the action of the current stabiliser (AD->SD[AD->cl+1].ST and AD->SD[AD->cl+1].SI).  If yes,
 * and if the antichain data is not complete (that is, AD->cl > 0]), then the new stabiliser is stored in
 * AD->SD[AD->cl].ST and AD->SD[AD->cl].SI.  If the antichain data is complete and canonical, AD->SD[0].ST and
 * AD->SD[0].SI will instead be set to the stabiliser of the new lattice obtained from the antichain data.
 * (The points on the lowest (new) level of the new lattice correspond to the antichains in that sequence;
 * if a permutation fixes the multiset but permutes its elements, the points of the lowest level of the
 * new lattice need to be permuted accordingly to obtain an element of the stabiliser of the new lattice.
 * Additional generators of the stabiliser of the new lattice are given by all possible permutations of
 * identical antichains, i.e., all possible permutations of points of the lowest level of the new lattice
 * that have identical covering sets.  The latter may be handled by "implicit" stabilisers.)
 */
{
	elt            i, gen, apf;
	unsigned long  pos;
	flags64*       M0;
	flags64        M1[MAXN-2];
	flags64        mask, pmask;
	permgrp*       G;
	permgrpc*      S;

#if defined TEST_P2_16 || defined TEST_P2_32
	static unsigned long hitcount = 0;
	printf("antichaindata_isCanonical_p2 hitcount: %lu\n", ++hitcount);
#endif
#ifdef VERBOSE
	printf("[entering antichaindata_isCanonical_p2]:\n"); lattice_print(AD->L); antichaindata_printCounters(AD);
#endif
	apf = BITSPERFLAGS64/bits;
	for (mask=BIT(bits)-1,i=apf; i--; mask<<=bits)
		M1[i] = mask;
	M0 = M1 + 2*apf-AD->k;
	G = AD->SD[AD_CSL+1].ST;
#ifdef DOTEST
	if (G->ngens && !(G->BenesValid & BIT(AD_CSL))) {
		printf("ATTEMPTS TO USE INVALID BENEŠ NETWORKS [antichaindata_isCanonical_1]: level %d\n", AD_CSL);
		erri(-4);
	}
#endif
	if (G->ngens > 0) {
		AD->GD->orb[0].data[0] = AD->GD->orb[0].data[1] = 0;
		for(i=0; i<AD->k-apf; i++) {
			AD->GD->orb[0].data[0] <<= bits;
			AD->GD->orb[0].data[0] |= (AD->O[i] & AD->cmc) >> AD->L->lev[AD->cl];
		}
		for(; i<AD->k; i++) {
			AD->GD->orb[0].data[1] <<= bits;
			AD->GD->orb[0].data[1] |= (AD->O[i] & AD->cmc) >> AD->L->lev[AD->cl];
		}
		if (AD->GD->orb[0].data[0] || AD->GD->orb[0].data[1]) {
			if (AD->SD[AD_CSL+1].SI) {
				/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
				permutation  p;
				flags64      L[2];
				L[0] = AD->GD->orb[0].data[0];
				L[1] = AD->GD->orb[0].data[1];
				perm_init(AD->L->n+AD->k, p);
				for (pmask=1,i=apf; i--;)
					pmask = (pmask << bits) | 1;
				antichainList_applySI_p2(AD->L->n, AD->k, apf, AD->L->lev[AD->cl], bits, AD->SD[AD_CSL+1].bl, AD->GD, L,
						AD->SD[AD_CSL+1].SI, pmask, p);
				if (antichainList_cmp_p2(L, AD->GD->orb[0].data)) {
					/* determine the position up to which we can backtrack */
					elt      m, pi;
					flags64  D;
					bool     found;
					m = i = 0;
					D = L[0] ^ AD->GD->orb[0].data[0];
					do {
						pi = p[AD->L->n+i] - AD->L->n;
						if (pi > m)
							m = pi;
					} while (!((found=(D & M0[i]))) && (++i < AD->k-apf));
					if (!found) {
						D = L[1] ^ AD->GD->orb[0].data[1];
						do {
							pi = p[AD->L->n+i] - AD->L->n;
							if (pi > m)
								m = pi;
						} while (!(D & M1[(i++)-(AD->k-apf)]));
					}
					AD->Fpos += m - AD->cp;
					AD->cp = m;
#ifdef VERBOSE
					perm_print(AD->L->n+AD->k, p, 0);
					printf("                                       NOT canonical --> %d\n", AD->cp);
#endif
					return FALSE;
				}
				S = antichaindata_ensureStabiliser(AD,AD_CSL);
				AD->SD[AD_CSL].ST = S->G;
				permgrpc_init(S, AD->L->n+AD->k);
				AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
				antichainList_extractStabiliser_p2(AD->L->n, AD->k, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], AD->GD,
						S, &(AD->SD[AD_CSL].SI), L);
				/* now spin up the orbit of representatives under the action of the (old) implicit stabiliser */
				AD->GD->orbsize = 1;
				AD->GD->orb[0].gen = -1;
				perm_init(S->G->n, AD->GD->orb[0].toRoot);
				hashtable_clear(AD->GD->orbpos);
				hashtable_insert_2(AD->GD->orbpos, L, HASH_2(L), 0);
#ifndef HARDCODE_MAXN_22
				if (S->G->n > G->n)
					for (gen=0; gen<G->ngens; gen++) {
						perm_resize(G->n, S->G->n, G->perm[gen]);
						perm_resize(G->n, S->G->n, G->invperm[gen]);
					}
#endif
				for (pos=0; pos<AD->GD->orbsize; pos++) {
					for (gen=0; gen<G->ngens; gen++) {
						flags64      A[2];
						permutation  p;
						if ((G->invol & BIT(gen)) && AD->GD->orb[pos].gen == gen)
							continue;
						/* apply generator gen to orbit element pos... */
						if (AD->GD->orbspace == AD->GD->orbsize)
							globals_enlargen_orbitspace(AD->GD);
						A[0] = L[0];
						A[1] = L[1];
						benes_apply_p2(G->Benes[AD_CSL][gen], A);
#ifndef FILTER_GRADED
						if (AD->cl < AD->L->nLev-2)
							benes_apply_blocked_p2(G->Benes[AD->L->nLev-1][gen], A, apf*bits);
#endif
						perm_init(S->G->n, p);
						antichainList_applySI_p2(AD->L->n, AD->k, apf, AD->L->lev[AD->cl], bits, AD->SD[AD_CSL+1].bl, AD->GD, A,
								AD->SD[AD_CSL+1].SI, pmask, p);
						/* ...we're done if the result is smaller than the original element */
						if (antichainList_cmp_p2(L, A) > 0) {
							/* determine the position up to which we can backtrack */
							elt      m, pi;
							flags64  D;
							bool     found;
							m = i = 0;
							D = L[0] ^ A[0];
							do {
								pi = AD->GD->orb[pos].toRoot[G->invperm[gen][p[AD->L->n+i]]] - AD->L->n;
								if (pi > m)
									m = pi;
							} while (!((found=(D & M0[i]))) && (++i < AD->k-apf));
							if (!found) {
								D = L[1] ^ A[1];
								do {
									pi = AD->GD->orb[pos].toRoot[G->invperm[gen][p[AD->L->n+i]]] - AD->L->n;
									if (pi > m)
										m = pi;
								} while (!(D & M1[(i++)-(AD->k-apf)]));
							}
							AD->Fpos += m - AD->cp;
							AD->cp = m;
#ifdef VERBOSE
							printf("                                       NOT canonical\n");
#endif
#ifdef PRINTLARGEORBITS
							if (AD->GD->orbsize > LARGEORBITTHRESHOLD) {
								printf("antichaindata_isCanonical_p2: %lu [FALSE]\n", AD->GD->orbsize);
								lattice_print(AD->L);
								antichaindata_printCounters(AD);
							}
#endif
							return FALSE;
						}
						/* ...otherwise, do the necessary housekeeping */
						AD->GD->orb[AD->GD->orbsize].data[0] = A[0];
						AD->GD->orb[AD->GD->orbsize].data[1] = A[1];
						processElement_p2(AD, G, S, pos, gen, p);
					}
				}
				permgrpc_compactGenerators(S);
			} else {
				/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
				permutation  p;
				flags64      L[2];
				L[0] = AD->GD->orb[0].data[0];
				L[1] = AD->GD->orb[0].data[1];
				perm_init(AD->L->n+AD->k, p);

				S = antichaindata_ensureStabiliser(AD,AD_CSL);
				AD->SD[AD_CSL].ST = S->G;
				permgrpc_init(S, AD->L->n+AD->k);
				AD->SD[AD_CSL].SI = 0;
				/* now spin up the orbit of representatives under the action of the (old) implicit stabiliser */
				AD->GD->orbsize = 1;
				AD->GD->orb[0].gen = -1;
				perm_init(S->G->n, AD->GD->orb[0].toRoot);
				hashtable_clear(AD->GD->orbpos);
				hashtable_insert_2(AD->GD->orbpos, L, HASH_2(L), 0);
#ifndef HARDCODE_MAXN_22
				if (S->G->n > G->n)
					for (gen=0; gen<G->ngens; gen++) {
						perm_resize(G->n, S->G->n, G->perm[gen]);
						perm_resize(G->n, S->G->n, G->invperm[gen]);
					}
#endif
				for (pos=0; pos<AD->GD->orbsize; pos++) {
					for (gen=0; gen<G->ngens; gen++) {
						flags64      A[2];
						permutation  p;
						if ((G->invol & BIT(gen)) && AD->GD->orb[pos].gen == gen)
							continue;
						/* apply generator gen to orbit element pos... */
						if (AD->GD->orbspace == AD->GD->orbsize)
							globals_enlargen_orbitspace(AD->GD);
						A[0] = L[0];
						A[1] = L[1];
						benes_apply_p2(G->Benes[AD_CSL][gen], A);
#ifndef FILTER_GRADED
						if (AD->cl < AD->L->nLev-2)
							benes_apply_blocked_p2(G->Benes[AD->L->nLev-1][gen], A, apf*bits);
#endif
						perm_init(S->G->n, p);
						antichainList_sort_p2(AD->k, bits, AD->k-apf, M0, M1, AD->SD[AD_CSL+1].bl, A, p, AD->L->n);
						/* ...we're done if the result is smaller than the original element */
						if (antichainList_cmp_p2(L, A) > 0) {
							/* determine the position up to which we can backtrack */
							elt      m, pi;
							flags64  D;
							bool     found;
							m = i = 0;
							D = L[0] ^ A[0];
							do {
								pi = AD->GD->orb[pos].toRoot[G->invperm[gen][p[AD->L->n+i]]] - AD->L->n;
								if (pi > m)
									m = pi;
							} while (!((found=(D & M0[i]))) && (++i < AD->k-apf));
							if (!found) {
								D = L[1] ^ A[1];
								do {
									pi = AD->GD->orb[pos].toRoot[G->invperm[gen][p[AD->L->n+i]]] - AD->L->n;
									if (pi > m)
										m = pi;
								} while (!(D & M1[(i++)-(AD->k-apf)]));
							}
							AD->Fpos += m - AD->cp;
							AD->cp = m;
#ifdef VERBOSE
							printf("                                       NOT canonical\n");
#endif
#ifdef PRINTLARGEORBITS
							if (AD->GD->orbsize > LARGEORBITTHRESHOLD) {
								printf("antichaindata_isCanonical_p2: %lu [FALSE]\n", AD->GD->orbsize);
								lattice_print(AD->L);
								antichaindata_printCounters(AD);
							}
#endif
							return FALSE;
						}
						/* ...otherwise, do the necessary housekeeping */
						AD->GD->orb[AD->GD->orbsize].data[0] = A[0];
						AD->GD->orb[AD->GD->orbsize].data[1] = A[1];
						processElement_p2(AD, G, S, pos, gen, p);
					}
				}
				permgrpc_compactGenerators(S);
			}
		} else { /* as the antichains must intersect the lowest level, AD->cl < AD->L->nLev-2 */
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
		}
	} else if (AD->SD[AD_CSL+1].SI) {
		AD->GD->orb[0].data[0] = AD->GD->orb[0].data[1] = 0;
		apf = BITSPERFLAGS64/bits;
		for(i=0; i<AD->k-apf; i++) {
			AD->GD->orb[0].data[0] <<= bits;
			AD->GD->orb[0].data[0] |= (AD->O[i] & AD->cmc) >> AD->L->lev[AD->cl];
		}
		for(; i<AD->k; i++) {
			AD->GD->orb[0].data[1] <<= bits;
			AD->GD->orb[0].data[1] |= (AD->O[i] & AD->cmc) >> AD->L->lev[AD->cl];
		}
		if (AD->GD->orb[0].data[0] || AD->GD->orb[0].data[1]) {
			/* test minimality under implied stabiliser and extract implied stabiliser generators if minimal */
			permutation  p;
			flags64      L[2];
			L[0] = AD->GD->orb[0].data[0];
			L[1] = AD->GD->orb[0].data[1];
			perm_init(AD->L->n+AD->k, p);
			for (pmask=1,i=apf; i--;)
				pmask = (pmask << bits) | 1;
			antichainList_applySI_p2(AD->L->n, AD->k, apf, AD->L->lev[AD->cl], bits, AD->SD[AD_CSL+1].bl, AD->GD, L,
					AD->SD[AD_CSL+1].SI, pmask, p);
			if (antichainList_cmp_p2(L, AD->GD->orb[0].data)) {
				/* determine the position up to which we can backtrack */
				elt      m, pi;
				flags64  D;
				bool     found;
				m = i = 0;
				D = L[0] ^ AD->GD->orb[0].data[0];
				do {
					pi = p[AD->L->n+i] - AD->L->n;
					if (pi > m)
						m = pi;
				} while (!((found=(D & M0[i]))) && (++i < AD->k-apf));
				if (!found) {
					D = L[1] ^ AD->GD->orb[0].data[1];
					do {
						pi = p[AD->L->n+i] - AD->L->n;
						if (pi > m)
							m = pi;
					} while (!(D & M1[(i++)-(AD->k-apf)]));
				}
				AD->Fpos += m - AD->cp;
				AD->cp = m;
#ifdef VERBOSE
				perm_print(AD->L->n+AD->k, p, 0);
				printf("                                       NOT canonical --> %d\n", AD->cp);
#endif
				return FALSE;
			}
			S = antichaindata_ensureStabiliser(AD,AD_CSL);
			AD->SD[AD_CSL].ST = S->G;
			permgrpc_init(S, AD->L->n+AD->k);
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
			antichainList_extractStabiliser_p2(AD->L->n, AD->k, AD->L->lev[AD->cl], AD->L->lev[AD->cl+1], AD->GD,
					S, &(AD->SD[AD_CSL].SI), L);
			permgrpc_compactGenerators(S);
		} else { /* as the antichains must intersect the lowest level, AD->cl < AD->L->nLev-2 */
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
			AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
		}
	} else {
#ifndef FILTER_GRADED
		if (AD->cl == AD->L->nLev-2) {
#endif
			S = antichaindata_ensureStabiliser(AD,AD_CSL);
			AD->SD[AD_CSL].ST = S->G;
			permgrpc_init(S, AD->L->n+AD->k);
#ifndef FILTER_GRADED
		} else {
			AD->SD[AD_CSL].ST = AD->SD[AD_CSL+1].ST;
		}
#endif
		AD->SD[AD_CSL].SI = AD->SD[AD_CSL+1].SI;
	}
#ifndef FILTER_GRADED
	if (AD_CSL == 0 ) {
#endif
		antichaindata_updateBlocks(AD, AD->SD[1].bl, &(AD->SD[0].bl));
		AD->SD[0].SI |= AD->SD[0].bl << AD->L->n;
#ifndef FILTER_GRADED
	}
#endif
	permgrp_preprocessGenerators_blocked(AD);
#ifdef VERBOSE
	printf("***** level %d:\n", AD->cl);
	permgrp_printGenerators(AD->SD[AD_CSL].ST, 0);
	printf("                                       canonical\n");
#endif
#ifdef PRINTLARGEORBITS
	if (AD->GD->orbsize > LARGEORBITTHRESHOLD) {
		printf("antichaindata_isCanonical_p2: %lu [FALSE]\n", AD->GD->orbsize);
		lattice_print(AD->L);
		antichaindata_printCounters(AD);
	}
#endif
	return TRUE;
}
