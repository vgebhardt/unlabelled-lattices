/*
 * lattice.h
 *
 * Created on: 22 Mar 2014
 * Last modified: 23 Feb 2019
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2019 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef LATTICE_H_
#define LATTICE_H_

#include "build.h"
#include "globals.h"
#include "permgrp.h"


/* We describe a lattice with the elements T,0,..,n-1,L (where T and B are the upper and lower bounds) by the following data:
 *   elt       n              -- number of non-extremal elements (the upper and lower bounds T and B are not stored)
 *   elt       nLev           -- number of levels, excluding T but including B
 *   elt       lev[0..nLev-1] -- first element in each level, with L represented by n; lev[0]=0, ..., lev[nLev-1]=n
 *   permgrp*  S              -- subgroup of the stabiliser of the lattice that generates the stabiliser together with...
 *   flags32   SI             -- ...the transpositions (i-1 i) of elements on level nLev-2 for the bits i set in SI
 *   flags32   up[0..n-1]     -- upper set: up[i] & (1<<j) iff j \in \uparrow i
 *   flags32   lo[0..n-1]     -- lower set: lo[i] & (1<<j) iff j \in \downarrow i
 *
 * no longer stored explicitly:
 *   elt       dep[0..n-1]    -- depth of each non-extremal element (0: depth 1, ..., n-1: depth nLev-2)
 *   flags32   co[0..n-1]     -- covers: co[i] & (1<<j) iff i \prec j (that is, i is covered by j)
 *
 */


typedef struct lattice lattice;
struct lattice {
	flags32   up[MAXN-2];
	flags32   lo[MAXN-2];
	permgrp*  S;
	flags32   SI;
	elt       lev[MAXN-1];
	elt       n;
	elt       nLev;
#if 0
	char      dummy[15];  /* to pad to 3 cache lines */
#endif
};


static inline void lattice_setStabiliser(lattice* L, permgrp* S, flags32 SI)
/*
 * Set the stabiliser data of the lattice *L to *S / SI; the reference count of *S is incremented.
 */
{
	L->S = permgrp_incref(S);
	L->SI = SI;
}


static inline void lattice_clearStabiliser(lattice* L)
/*
 * Decrement the reference count for the stabiliser of *L.
 */
{
	permgrp_delete(L->S);
}


static inline lattice* lattice_alloc()
/*
 * Return a dynamically allocated lattice data structure.
 */
{
	return (lattice*)malloc(sizeof(lattice));
}


static inline void lattice_free(lattice* L)
/*
 * Free the memory used by a dynamically allocated lattice data structure.
 */
{
	lattice_clearStabiliser(L);
	free(L);
}


static inline void lattice_cpy(lattice* L, lattice* M)
/*
 * Copy L to M.
 */
{
	flags32  B;
	elt      i, n;

	memcpy(M, L, sizeof(lattice));
	M->S = permgrp_alloc();
	permgrp_cpy(L->S, M->S); /* This does not copy the Beneš networks, so we do this manually. */
	B = M->S->BenesValid = L->S->BenesValid;
	while (get_LSB32(B,&n)) {
		for (i=0; i<L->S->ngens; i++)
			M->S->Benes[n][i] = benes_incref(L->S->Benes[n][i]);
		B ^= BIT(n);  /* bit n is set, so this clears it */
	}
}


void lattice_getCoveringRelation(flags32* co, lattice* L);
/*
 * Set co[i] to a flag indicating the covers of element i of the lattice L.
 * The array co must be allocated.
 */


void lattice_getDepths(flags32* dep, flags32* co, elt n);
/*
 * Set dep[i] to the depth of element i of the lattice on n elements defined by the covering
 * relation co.  The array dep must be allocated.
 */


bool lattice_isCanonical(lattice* L);
/*
 * TEST FUNCTION: Return: TRUE if the levellised lattice L is canonical; FALSE otherwise.
 */


bool lattice_test(lattice* L);
/*
 * TEST FUNCTION: Return TRUE if the data for the lattice L are consistent; FALSE otherwise.
 */


void lattice_toString(char* buf, lattice* L);
/*
 * Store a representation of the covering relation of the levellised lattice L in the
 * string buf.  The string contains the part of the covering matrix below the diagonal,
 * including the upper and lower bounds, in row major order.  The order of rows/columns
 * is as follows:  T,0,1,..,n-1,B.
 * The string buf must be allocated and have at least size (L->n+2)*(L->n+1)/2+1.
 */


bool lattice_fromString(lattice* L, elt n, const char* s, permgrp* S, flags32 SI);
/*
 * Generate data for a levellised lattice on n elements (including upper and lower bounds)
 * from the string representation s of its covering relation [see lattice_toString] and
 * the stabiliser S, and write the data to L.  L must point to an allocated block of memory.
 * Return whether successful.  The reference count of *S is incremented.
 */


void lattice_toOldString(char* buf, lattice* L);
/*
 * Store a representation of the covering relation of the levellised lattice L in the
 * string buf.  The string contains the entire covering matrix below the diagonal,
 * including the upper and lower bounds, in row major order.  The order of rows/columns
 * is as follows:  T,0,1,..,n-1,B.
 * The string buf must be allocated and have at least size (L->n+2)*(L->n+2)+1.
 */


bool lattice_fromOldString(lattice* L, elt n, const char* s, permgrp* S, flags32 SI);
/*
 * Generate data for a levellised lattice on n elements (including upper and lower bounds)
 * from the string representation s of its covering relation [see lattice_toOldString] and
 * the stabiliser S, and write the data to L.  L must point to an allocated block of memory.
 * Return whether successful.  The reference count of *S is incremented.
 */


void lattice_print(lattice* L);
/*
 * Print the levellised lattice L to stdout.
 */


void lattice_init_2(lattice* L);
/*
 * Initialise L to the lattice with 2 elements.
 */


void lattice_init_kFan(lattice* L, elt k);
/*
 * Initialise L to the k-fan (the lattice with k elements covered by T and covering B).
 */


unsigned int lattice_numberOfMaximalChains(lattice* L);
/*
 * Return the number of maximal chains (from B to T) in L.
 */


#endif /* LATTICE_H_ */
