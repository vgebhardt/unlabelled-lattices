/*
 * permutation.c
 *
 * Created on: 22 Mar 2014
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include "permutation.h"


void perm_print(elt n, permutation p, elt offset)
/*
 * TEST FUNCTION:  Print p (in array notation).
 */
{
	elt  i;

	printf("%d", p[0]+offset);
	for (i=1; i<n; i++)
		printf(", %d", p[i]+offset);
	printf("\n");
}


bool perm_next(elt nLev, elt* lev, permutation p)
/*
 * Advance p to the next permutation fixing the levels
 * {0..lev[1]-1},..,{lev[nLev-2]..lev[nLev-1]-1}.
 *
 * Return value: 1 if success; 0 if no further permutation exists.
 */
{
	elt  lpos, thislev, i, j, t;

	t = 0;  /* initialisation value unused; just to prevent warning */
	lpos = 0;
	thislev = 0;
	do {
		/* advance to next non-trivial level */
		while (lev[lpos] == thislev+1) {
			thislev++;
			if (++lpos == nLev)
				return FALSE;
		}
		for (i=thislev+1; i<lev[lpos] && !(t=(p[i-1]<p[i])); i++)
			;
		if (!t) {
			/* set current level to id & advance to next level */
			for (i=thislev; i<lev[lpos]; i++)
				p[i] = i;
			thislev = lev[lpos];
			if (++lpos == nLev)
				return FALSE;
			continue;
		}
		for (j=thislev; !(p[j]<p[i]); j++)
			;
		/* swap entries i and j */
		t = p[i];
		p[i] = p[j];
		p[j] = t;
		/* reverse level up to position i-1 */
		i--;
		j = thislev;
		while (j < i) {
			t = p[i];
			p[i] = p[j];
			p[j] = t;
			i--;
			j++;
		}
		break;
	} while (1);
	return TRUE;
}
