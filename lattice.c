/*
 * lattice.c
 *
 * Created on: 22 Mar 2014
 * Last modified: 23 Feb 2019
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2019 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lattice.h"
#include "permutation.h"


void lattice_getCoveringRelation(flags32* co, lattice* L)
/*
 * Set co[i] to a flag indicating the covers of element i of the lattice L.
 * The array co must be allocated.
 */
{
	elt      i, j;
	flags32  Ui;

	for (i=L->n; i--;) {
		co[i] = 0;
		Ui = L->up[i]^BIT(i);
		while (get_MSB32(Ui,&j)) {
			co[i] |= BIT(j);
			Ui &= ~(L->up[j]);
		}
	}
}


void lattice_getDepths(flags32* dep, flags32* co, elt n)
/*
 * Set dep[i] to the depth of element i of the lattice on n elements defined by the covering relation co.
 * The array dep must be allocated.
 */
{
	elt  l, i, j;

	for (i=0; i<n; i++)
		dep[i] = 0;
	for (l=n; l--; )
		for (i=n; i--; )
			for (j=n; j--; )
				if (co[i] & BIT(j)) {
					if (dep[i] < dep[j]+1)
						dep[i] = dep[j]+1;
				}

}


bool lattice_isCanonical(lattice* L)
/*
 * TEST FUNCTION: Return: TRUE if the levellised lattice L is canonical; FALSE otherwise.
 */
{
	elt          i, j, li, lj;
	permutation  permbuf;
	flags32      co[MAXN-2];
	flags32      dep[MAXN-2];

#define COV(i,j)  (co[i] & BIT(j))
	perm_init(L->n, permbuf);
	lattice_getCoveringRelation(co, L);
	lattice_getDepths(dep, co, L->n);
	while (perm_next(L->nLev, L->lev, permbuf))  /* skip identity */ {
		for (li=1; li<L->nLev; li++)
			for (lj=li; lj--;)
				for (i=L->lev[li]; i<L->n && dep[i]==li; i++)
					for (j=L->lev[lj+1]; j-- && dep[j]==lj;) {
						if (!COV(i,j)) {
							if (COV(permbuf[i],permbuf[j]))
								goto next;
						} else {
							if (!COV(permbuf[i],permbuf[j]))
								return FALSE;
						}
					}
		next:;
	}

	return TRUE;
#undef COV
}


bool lattice_test(lattice* L)
/*
 * TEST FUNCTION: Return TRUE if the data for the lattice L are consistent; FALSE otherwise.
 */
{
	elt           i, j, v, gen;
	elt           _dep[MAXN-2];
	flags32       _up[MAXN-2];
	flags32       _lo[MAXN-2];
	flags32       m;
	flags32       co[MAXN-2];

#define COV(i,j)  (co[i] & BIT(j))
	if (L->nLev < 1 || L->lev[L->nLev-1] != L->n) {
		printf(">>> basic levels\n");
		return FALSE;
	}
	for (i=0; i<L->n; i++) {
		_up[i] = _lo[i] = BIT(i);
		_dep[i] = 0;
	}
	lattice_getCoveringRelation(co, L);
	for (v=L->nLev; v--; )
		for (i=L->n; i--; )
			for (j=L->n; j--; )
				if (COV(i,j)) {
					_up[i] |= _up[j];
					_lo[j] |= _lo[i];
					if (_dep[i] < _dep[j]+1)
						_dep[i] = _dep[j]+1;
				}
	for (i=0,v=-1; i<L->n; i++) {
		if (i == L->lev[v+1])
			v++;
		if (_dep[i] != v) {
			printf(">>> levels\n");
			return FALSE;
		}
	}
	if (memcmp(_up, L->up, L->n*sizeof(flags32))) {
		printf(">>> upper sets\n");
		for (i=0; i<L->n; i++)
			printf("%i: %d [%d]\n", i, L->up[i], _up[i]);
		return FALSE;
	}
	if (memcmp(_lo, L->lo, L->n*sizeof(flags32))) {
		printf(">>> lower sets\n");
		for (i=0; i<L->n; i++)
			printf("%i: %d [%d]\n", i, L->lo[i], _lo[i]);
		return FALSE;
	}
	for (i=L->n; i--;)
		for (j=i; j--;) {
			m = L->lo[i] & L->lo[j];
			if (get_LSB32(m,&v) && m != (m & L->lo[v])) {
				printf(">>> meet(%d,%d) [%d]", i, j, v);
				return FALSE;
			}
			m = L->up[i] & L->up[j];
			if (get_MSB32(m,&v) && m != (m & L->up[v])) {
				printf(">>> join(%d,%d) [%d]", i, j, v);
				return FALSE;
			}
		}
	for (gen=L->S->ngens; gen--;)
		for (i=0; i<L->n; i++)
			for (j=L->lev[_dep[i]]; j--;)
				if (!COV(i,j)) {
					if (COV(L->S->perm[gen][i],L->S->perm[gen][j])) {
						printf(">>> bad stabiliser generator ");
						perm_print(L->S->n, L->S->perm[gen], 0);
						return FALSE;
					}
				} else {
					if (!COV(L->S->perm[gen][i],L->S->perm[gen][j])) {
						printf(">>> bad stabiliser generator ");
						perm_print(L->S->n, L->S->perm[gen], 0);
						return FALSE;
					}
				}
	m = L->SI;
	while (extract_MSB32(&m, &v)) {
		if (v > L->n || _dep[v-1] != _dep[v]) {
			printf(">>> bad implicit stabiliser generator (%d,%d)\n", v-1, v);
			return FALSE;
		}
		for (i=0; i<L->n; i++) {
			if ((COV(i,v-1) && !COV(i,v)) || (!COV(i,v-1) && COV(i,v))) {
				printf(">>> bad implicit stabiliser generator (%d,%d)\n", v-1, v);
				return FALSE;
			}
			if ((COV(v-1,i) && !COV(v,i)) || (!COV(v-1,i) && COV(v,i))) {
				printf(">>> bad implicit stabiliser generator (%d,%d)\n", v-1, v);
				return FALSE;
			}
		}
	}
#ifdef FILTER_GRADED
	for (v=1; v<L->nLev-1; v++) {
		m = 0;
		for (i=L->lev[v]; i<L->lev[v+1]; i++)
			m |= co[i];
		if (m != BIT(L->lev[v])-BIT(L->lev[v-1])) {
			printf(">>> not graded (%d)\n", v);
			return FALSE;
		}
	}
#endif
	return TRUE;
#undef COV
}


void lattice_toString(char* buf, lattice* L)
/*
 * Store a representation of the covering relation of the levellised lattice L in the
 * string buf.  The string contains the part of the covering matrix below the diagonal,
 * including the upper and lower bounds, in row major order.  The order of rows/columns
 * is as follows:  T,0,1,..,n-1,B.
 * The string buf must be allocated and have at least size (L->n+2)*(L->n+1)/2+1.
 */
{
	elt      i, j;
	int      pos;
	flags32  co[MAXN-2];

	lattice_getCoveringRelation(co, L);
	pos = 0;
	for (i=0; i<L->n; i++) {
		buf[pos++] = (!co[i]) ? '1' : '.';              /* i \prec T ? */
		for (j=0; j<i; j++)
			buf[pos++] = (co[i] & BIT(j)) ? '1' : '.';  /* i \prec j ? */
	}
	buf[pos++] = (L->nLev == 1) ? '1' : '.';            /* B \prec T ? */
	for (j=0; j<L->n; j++)
		buf[pos++] = (L->lo[j] == BIT(j)) ? '1' : '.';  /* B \prec j ? */
	buf[pos] = 0;
}


bool lattice_fromString(lattice* L, elt n, const char* s, permgrp* S, flags32 SI)
/*
 * Generate data for a levellised lattice on n elements (including upper and lower bounds) from
 * the string representation s of its covering relation [see above] and the stabiliser S, and
 * write the data to L.  L must point to an allocated block of memory.  Return whether successful.
 * The reference count of *S is incremented.
 */
{
	elt      d, i, j;
	int      pos;
	flags32  co[MAXN-2];
	flags32  dep[MAXN-2];

	if (strlen(s) != (n*(n-1))/2) {
#ifdef DOTEST
		printf("BAD PARAMETERS in lattice_fromString: given string doesn't match lattice size!\n");
		erri(-1);
#endif
		return FALSE;
	}
	n -= 2;  /* We don't store the upper and lower bounds. */
	memset(L, 0, sizeof(lattice));
	memset(co, 0, n*sizeof(flags32));
    L->n = n;
    /* first extract covering relation... */
	pos = 0;
	for (i=0; i<n; i++) {
		pos++;                    /* i \prec T ? (ignored) */
		for (j=0; j<i; j++)
			if (s[pos++] == '1')
				co[i] |= BIT(j);  /* i \prec j ? */
	}                             /* B \prec j ? (ignored) */
    /* ...then use it to generate the remaining data, except for the stabiliser... */
	for (i=0; i<n; i++) {
		L->up[i] = L->lo[i] = BIT(i);
		dep[i] = 0;
	}
	for (d=0; d<n; d++)
		for (i=1; i<n; i++)
			for (j=0; j<i; j++)
				if (co[i] & BIT(j)) {
					L->up[i] |= L->up[j];
					L->lo[j] |= L->lo[i];
					if (dep[i] < dep[j]+1)
						dep[i] = dep[j]+1;
				}
	L->lev[0] = 0;
	for (i=0, d=0; i<n; d++) {
		while (i<n && dep[i]==d)
			i++;
		L->lev[d+1] = i;
	}
	L->nLev = d+1;  /* count the level containing B */
	/* ...finally set the stabiliser */
	lattice_setStabiliser(L, S, SI);
#ifdef DOTEST
	return lattice_test(L);
#else
	return TRUE;
#endif
}


void lattice_toOldString(char* buf, lattice* L)
/*
 * Store a representation of the covering relation of the levellised lattice L in the
 * string buf.  The string contains the entire covering matrix below the diagonal,
 * including the upper and lower bounds, in row major order.  The order of rows/columns
 * is as follows:  T,0,1,..,n-1,B.
 * The string buf must be allocated and have at least size (L->n+2)*(L->n+2)+1.
 */
{
	elt      i, j;
	int      pos;
	flags32  co[MAXN-2];

	lattice_getCoveringRelation(co, L);
	pos = 0;
	for (j=0; j<L->n+2; j++)
		buf[pos++] = '.';                               /* T \prec T/j/B ? */
	for (i=0; i<L->n; i++) {
		buf[pos++] = (!co[i]) ? '1' : '.';              /* i \prec T ? */
		for (j=0; j<L->n; j++)
			buf[pos++] = (co[i] & BIT(j)) ? '1' : '.';  /* i \prec j ? */
		buf[pos++] = '.';                               /* i \prec B ? */
	}
	buf[pos++] = (L->nLev == 1) ? '1' : '.';            /* B \prec T ? */
	for (j=0; j<L->n; j++)
		buf[pos++] = (L->lo[j] == BIT(j)) ? '1' : '.';  /* B \prec j ? */
	buf[pos++] = '.';                                   /* B \prec B ? */
	buf[pos] = 0;
}


bool lattice_fromOldString(lattice* L, elt n, const char* s, permgrp* S, flags32 SI)
/*
 * Generate data for a levellised lattice on n elements (including upper and lower bounds)
 * from the string representation s of its covering relation [see lattice_toOldString] and
 * the stabiliser S, and write the data to L.  L must point to an allocated block of memory.
 * Return whether successful.  The reference count of *S is incremented.
 */
{
	elt      d, i, j;
	int      pos;
	flags32  co[MAXN-2];
	flags32  dep[MAXN-2];

	if (strlen(s) != n*n) {
#ifdef DOTEST
		printf("BAD PARAMETERS in lattice_fromString: given string doesn't match lattice size!\n");
		erri(-1);
#endif
		return FALSE;
	}
	n -= 2;  /* We don't store the upper and lower bounds. */
	memset(L, 0, sizeof(lattice));
	memset(co, 0, n*sizeof(flags32));
    L->n = n;
    /* first extract covering relation... */
	pos = n+2;                    /* T \prec T/j/B ? (ignored) */
	for (i=0; i<n; i++) {
		pos++;                    /* i \prec T ? (ignored) */
		for (j=0; j<n; j++)
			if (s[pos++] == '1')
				co[i] |= BIT(j);  /* i \prec j ? */
		pos++;                    /* i \prec B ? (ignored) */
	}                             /* B \prec T/j/B ? (ignored) */
    /* ...then use it to generate the remaining data, except for the stabiliser... */
	for (i=0; i<n; i++) {
		L->up[i] = L->lo[i] = BIT(i);
		dep[i] = 0;
	}
	for (d=0; d<n; d++)
		for (i=1; i<n; i++)
			for (j=0; j<i; j++)
				if (co[i] & BIT(j)) {
					L->up[i] |= L->up[j];
					L->lo[j] |= L->lo[i];
					if (dep[i] < dep[j]+1)
						dep[i] = dep[j]+1;
				}
	L->lev[0] = 0;
	for (i=0, d=0; i<n; d++) {
		while (i<n && dep[i]==d)
			i++;
		L->lev[d+1] = i;
	}
	L->nLev = d+1;  /* count the level containing B */
	/* ...finally set the stabiliser */
	lattice_setStabiliser(L, S, SI);
#ifdef DOTEST
	return lattice_test(L);
#else
	return TRUE;
#endif
}


void lattice_print(lattice* L)
/*
 * Print the levellised lattice L to stdout.
 */
{
	elt      d, i, j, first;
	flags32  co[MAXN-2];
	flags32  SI;

	lattice_getCoveringRelation(co, L);

	printf("depth -1: T\n");
	for (d=0; d<L->nLev-1; d++) {
		printf("depth %2d: ", d);
		for (i=L->lev[d]; i<L->lev[d+1]; i++) {
			printf("%d[", i);
			if (!co[i]) {
				printf("T");
			} else {
				for (j=0,first=1; j<i; j++)
					if (co[i] & BIT(j)) {
						if (first) {
							printf("%d",j);
							first = 0;
						} else {
							printf(",%d",j);
						}
					}
			}
			printf("] ");
		}
		printf("\n");
	}
	printf("depth %2d: B[", L->nLev-1);
	if (L->nLev == 1) {
		printf("T");
	} else {
		for (j=0,first=1; j<L->n; j++)
			if (L->lo[j] == BIT(j)) {
				if (first) {
					printf("%d",j);
					first = 0;
				} else {
					printf(",%d",j);
				}
			}
	}
	printf("] \n");
	printf("stabiliser [%d]:\n", L->S->n);
	permgrp_printGenerators(L->S, 0);
	SI = L->SI;
	while (extract_LSB32(&SI,&i)) {
		printf("(%d,%d) implicit\n", i-1, i);
	}
}


void lattice_init_2(lattice* L)
/*
 * Initialise L to the lattice with 2 elements.
 */
{
	permgrp*  S;

	S = permgrp_alloc();
	permgrp_init(S, 0);
    lattice_fromString(L, 2, "1", S, 0);
    permgrp_delete(S);
}


void lattice_init_kFan(lattice* L, elt k)
/*
 * Initialise L to the k-fan (the lattice with k elements covered by T and covering B).
 */
{
	elt       i;
	permgrp*  S;
	flags32   SI;

	for (i=0; i<k; i++)
		L->up[i] = L->lo[i] = BIT(i);
	S = permgrp_alloc();
	permgrp_init(S, 0);
	SI = (1 << k) - 2;  /* bits 1,..,(k-1) set */
	lattice_setStabiliser(L, S, SI);
    permgrp_delete(S);
	L->lev[0] = 0;
	L->lev[1] = k;
	L->n = k;
	L->nLev = 2;
}


unsigned int lattice_numberOfMaximalChains(lattice* L)
/*
 * Return the number of maximal chains (from B to T) in L.
 */
{
	flags32       co[MAXN-2];
	unsigned int  mc = 0;
	unsigned int  p[2][MAXN-2];
	elt           i, l;

	lattice_getCoveringRelation(co, L);
	for (i=0; i<L->n; i++)
		p[0][i] = (L->lo[i] == BIT(i)) ? 1 : 0;
	for (l=1; l<L->nLev; l++) {
		memset(p[l&1], 0, L->n*sizeof(unsigned int));
		for (i=0; i<L->n; i++) {
			flags32 C = co[i];
			if (!C)
				mc += p[(l-1)&1][i];
			else {
				elt j;
				while (get_LSB32(C, &j)) {
					C ^= BIT(j);
					p[l&1][j] += p[(l-1)&1][i];
				}
			}
		}
	}
	return mc;
}
