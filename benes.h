/*
 * benes.h
 *
 * Created on: 8 Jun 2015
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef BENES_H_
#define BENES_H_

#include "permutation.h"


struct benes_p1 {
	long     refcount;              /* reference count; -1 = persistent */
	flags64  mask[2*LD_MAXN_2-1];   /* masks for stages */
	elt      shift[2*LD_MAXN_2-1];  /* shift amounts for stages */
	elt      depth;                 /* number of stages */
};

struct benes_p2 {
	long     refcount;              /* reference count; -1 = persistent */
	flags64  mask[2*LD_MAXN_2-1];   /* masks for stages */
	elt      shift[2*LD_MAXN_2-1];  /* shift amounts for stages */
	elt      depth;                 /* number of stages */
	flags64  mask1[2*LD_MAXN_2-1];  /* masks for stages; second flags64 */
};

typedef struct benes_p2 benes;  /* We actually use struct benes_p1 where the second flags64 is not needed... */


extern benes***  benes_small;  /* global storage for precomputed Beneš networks */


void benes_init_small();
/*
 * Precompute Beneš networks for all permutations on at most BENES_SMALL points.
 */


void benes_free_small();
/*
 * Free the memory allocated for the precomputed Beneš networks for all permutations
 * on at most BENES_SMALL points.
 */


benes* benes_create(elt n, permutation p, permutation pi);
/*
 * Return a newly created Beneš network (carrying a reference count) realising as many copies of the
 * action of the permutation p on n points as fit into one flags64.  NOTE: p AND pi ARE MODIFIED.
 */


static inline benes* benes_incref(benes* B)
/*
 * Increment the reference count for *B and return *B.
 */
{
	if (B->refcount>0)
		B->refcount++;
	return B;
}


static inline void benes_delete(benes* B)
/*
 * Decrement the reference count for *B, and free the allocated memory if the reference count reaches 0.
 */
{
	if (B->refcount>0 && !(--(B->refcount)))
		free(B);
}


static inline void benes_get(permutation p, permutation pi, elt lo, elt hi, benes** B)
/*
 * Set *B to a pointer to a Beneš network realising the action of the permutation p on the points lo..hi-1,
 * where pi is the inverse of p.  The masks indicate the lower bit of each pair.
 */
{
	elt          j, n;
	permutation  src, inv_src;

	n = hi-lo;
	if (n <= BENES_SMALL) {
		for (j=0; j<n; j++)
			src[j] = p[lo+j] - lo;
		*B = benes_small[n][perm_toInteger(n,src)];  /* no incref: Beneš network is persistent */
	} else {
		for (j=0; j<n; j++) {
			src[j] = p[lo+j] - lo;
			inv_src[j] = pi[lo+j] - lo;
		}
		*B = benes_create(n, src, inv_src);
	}
#if BENES_DOPREFETCH
	__builtin_prefetch((void*)*B);
	__builtin_prefetch((void*)*B+64);
#endif
}


static inline void benes_get_blocked(permutation p, permutation pi, elt lo, elt hi, elt m, benes** B)
/*
 * Set *B to a pointer to a Beneš network realising the action of the permutation p on the points lo..hi-1
 * as a permutation of blocks of width m, where pi is the inverse of p.  The masks for blocked Beneš
 * networks indicate the higher bit of each pair.
 */
{
	elt          i, j, n, apf;
	permutation  src, inv_src;
	benes*       T;
	flags64      mask;

	n = hi-lo;
	if (n <= BENES_SMALL) {
		for (j=0; j<n; j++)
			src[j] = p[lo+j] - lo;
		T = benes_small[n][perm_toInteger(n,src)];  /* no incref: Beneš network is persistent */
#if BENES_DOPREFETCH
	    __builtin_prefetch((void*)(T));
	    __builtin_prefetch((void*)(T+64));
#endif
	} else {
		for (j=0; j<n; j++) {
			src[j] = p[lo+j] - lo;
			inv_src[j] = pi[lo+j] - lo;
		}
		T = benes_create(n, src, inv_src);
	}
	apf = BITSPERFLAGS64/m;
	mask = BIT(m)-1;
	if (n > apf) {
		*B = (benes*)calloc(1, sizeof(struct benes_p2));
		(*B)->refcount = 1;
		(*B)->depth = T->depth;
		for (i=0; i<T->depth; i++) {
			(*B)->shift[i] = m*T->shift[i];
			(*B)->mask[i] = (*B)->mask1[i] = 0;
			mask = BIT(m)-1;
			for (j=n-1; j>=n-apf;j--) {  /* this order: the lower member of a pair becomes the higher block */
				if (T->mask[i] & BIT(j))
					(*B)->mask1[i] |= mask;
				mask <<= m;
			}
			mask = BIT(m)-1;
			for (; j>=0; j--) {  /* this order: the lower member of a pair becomes the higher block */
				if (T->mask[i] & BIT(j))
					(*B)->mask[i] |= mask;
				mask <<= m;
			}
		}
#if BENES_DOPREFETCH
        __builtin_prefetch((void*)*B);
        __builtin_prefetch((void*)*B+64);
#endif
	} else {
		*B = (benes*)calloc(1, sizeof(struct benes_p1));
		(*B)->refcount = 1;
		(*B)->depth = T->depth;
		for (i=0; i<T->depth; i++) {
			(*B)->shift[i] = m*T->shift[i];
			(*B)->mask[i] = 0;
			mask = BIT(m)-1;
			for (j=n; j--;) {  /* this order: the lower member of a pair becomes the higher block */
				if (T->mask[i] & BIT(j))
					(*B)->mask[i] |= mask;
				mask <<= m;
			}
		}
#if BENES_DOPREFETCH
        __builtin_prefetch((void*)*B);
        __builtin_prefetch((void*)*B+64);
        __builtin_prefetch((void*)*B+128);
#endif
	}
	benes_delete(T);
}


static inline void benes_apply_p1(benes* B, flags64* R)
/*
 * Apply the Beneš network *B to *R.  The masks indicate the lower bit of each pair.
 */
{
	elt      i;
	flags64  t, A;

	A = *R;
	for (i=0; i<B->depth; ++i) {
		t = ((A >> B->shift[i]) ^ A) & B->mask[i];
		A ^= t;
		t <<= B->shift[i];
		A ^= t;
	}
	*R = A;
}


static inline void benes_apply_blocked_p1(benes* B, flags64* R)
/*
 * Apply the Beneš network *B acting on the positions of the packed antichain list *R.
 * The masks for blocked Beneš networks indicate the higher bit of each pair.
 */
{
	elt      i;
	flags64  t, A;

	A = *R;
	for (i=0; i<B->depth; ++i) {
		t = ((A << B->shift[i]) ^ A) & B->mask[i];
		A ^= t;
		t >>= B->shift[i];
		A ^= t;
	}
	*R = A;
}


static inline void benes_apply_p2(benes* B, flags64 A[])
/*
 * Apply the Beneš network *B to A[0],A[1].  The masks indicate the lower bit of each pair.
 *
 */
{
	elt      i, sft;
	flags64  t;

	for (i=0; i<B->depth; ++i) {
		sft = B->shift[i];
		t = ((A[0] >> sft) ^ A[0]) & B->mask[i];
		A[0] ^= t;
		t <<= sft;
		A[0] ^= t;
		t = ((A[1] >> sft) ^ A[1]) & B->mask[i];
		A[1] ^= t;
		t <<= sft;
		A[1] ^= t;
	}
}


static inline void benes_apply_blocked_p2(benes* B, flags64 A[2], elt width)
/*
 * Apply the Beneš network *B acting on the positions of the packed antichain list A[0],A[1], with width bits of
 * A[1] used.  The masks for blocked Beneš networks indicate the higher bit of each pair.
 */
{
	elt      i, sft, xsft;
	flags64  t0, t0_, t1;

	for (i=0; i<B->depth; ++i) {
		sft = B->shift[i];
		xsft = width-sft;
		t0 = (((A[0] << sft) | (A[1] >> xsft)) ^ A[0]) & B->mask[i];
		t1 = ((A[1] << sft) ^ A[1]) & B->mask1[i];
		A[0] ^= t0;
		A[1] ^= t1;
		t0_ = t0;
		t0 >>= sft;
		t1 = (t1 >> sft) | (t0_ << xsft);
		A[0] ^= t0;
		A[1] ^= t1;
	}
}


#endif /* BENES_H_ */
