/*
 * threaded.h
 *
 * Created on: 07 July 2015
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef THREADED_H_
#define THREADED_H_

#ifdef THREADED

#include <pthread.h>

#ifdef NUMA
#include <numa.h>
#endif

#include "lattEnum.h"



typedef struct lattEnum_Buffer lattEnum_Buffer;
struct lattEnum_Buffer {
	void (*reg)(lattEnum* E, lattice* L);  /* function called for registering lattices */
	globals*             GD;               /* "global" data for process/thread         */
	lattice*             L;                /* lattice whose descendants we enumerate   */
	long long            count;            /* number of lattices found                 */
	elt                  N;                /* target size                              */
	elt                  Nmin;             /* minimum size for recursion               */
	bool                 done;             /* whether enumeration is complete          */
	int                  bufsize;          /* number of available slots                */
	elt                  nSlotFlags;       /* number of flags needed                   */
	flags64*             freeslots;        /* flags marking empty slots                */
	flags64*             fullslots;        /* flags marking slots to be processed      */
	lattice**            lattices;         /* buffer for lattices                      */
	unsigned long long*  indices;          /* the index of the lattices in the buffer  */
	unsigned int         depth;            /* depth of the enumeration                 */
	char*                prefix;           /* prefix of the enumeration for state file */
	unsigned long long   sumskipped;
};


typedef struct threaded_arg threaded_arg;
struct threaded_arg {
	lattEnum_Buffer*    ES;
#ifdef NUMA
	int                 numanode;
#endif
	elt                 N;
	elt                 Nmin;
};


typedef struct task task;
struct task {
	unsigned int  depth;   /* depth of the root lattice in the enumeration hierarchy */
	lattice*      L;
	char*         prefix;
	elt           Nmin;    /* minimal size not covered by any parent enumerations */
};


void lattEnum_Buffer_register(lattEnum* E, lattice* L);
/*
 * Register the lattice L.
 */


void lattEnum_Buffer_free(lattEnum_Buffer* E);
/*
 * Free all memory.
 */


void lattEnum_Buffer_finalise(lattEnum_Buffer* E);
/*
 * Notify all watchers that there won't be any more lattices.
 */


void tasks_init();
/*
 * Initialise the global tasks list to contain the lattice with 2 elements.
 */


void tasks_register(lattice* L, elt Nmin, unsigned int depth, char* prefix);
/*
 * Add the lattice *L to the global task list.  L and prefix are transferred.
 */


task* tasks_next();
/*
 * Return a pointer to the next task, or NULL if there is no more task.
 */


task* tasks_all();
/*
 * Return a pointer to the task list.
 */


int tasks_count();
/*
 * Return the number of tasks in the task list.
 */


void tasks_free();
/*
 * Clean up the global tasks list.
 */


void tasks_print();
/*
 * Test function:  Print the global task list.
 */


void* threaded_thread(void* arg);
/*
 * Function launching a slave thread.
 *
 * arg should be of type threaded_arg.  The function starts an enumeration with parameters given by EN for every
 * lattice in ES; the latter must have the mode lattEnumModeArray.
 */


unsigned long long threaded_master(task* t, int nTasks, elt N, elt M, int nThreads, globals* GD);
/*
 * Start a master thread launching a multi-threaded enumeration with nThreads for the task list t containing nTasks tasks
 * with target lattice size N and master enumeration target size M.  All tasks must have the same minimum size.
 */

#endif

#endif /* THREADED_H_ */
