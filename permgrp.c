/*
 * permgrp.c
 *
 * Created on: 23 May 2014
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include "permgrp.h"
#include "antichain.h"


static bool JerrumCreatesCycle(permgrpc* G, elt i, elt j, permutation p, permutation h, elt* m, elt* nm)
/*
 * Return whether adding an edge i--j, for the permutation p, creates a cycle in the Jerrum graph.
 * If so, the product of the generators along the cycle is returned in k, and (*m)--(*nm) is the
 * first edge of the cycle.
 */
{
	elt       anc[MAXN-2];
	elt       todo[MAXN-2];
	elt       ntodo, pos, min, u, v;
	flags32   unseen;

	todo[0] = i;
	ntodo = 1;
	unseen = NBIT(i);
	for (pos=0; pos<ntodo; pos++) {
		flags32  nu;
		u = todo[pos];
		nu = G->Jerrum[u].neighbours;
		while (get_LSB32(nu&unseen,&v)) {
			anc[v] = u;
			if (v == j)
				goto cycle_found;
			todo[ntodo++] = v;
			unseen ^= BIT(v);  /* bit v is set, so this clears it */
		}
	}
	return FALSE;
	cycle_found:
	for (min=i,u=j; u!=i; u=anc[u])
		min = u<min ? u : min;
	*m = min;
	*nm = anc[min];
	if (min == i) {
		perm_cpy(G->G->n, p, h);
		for (u=j; u!=i; u=v) {
			v = anc[u];
			perm_mult(G->G->n, h, (u<v ? G->G->perm : G->G->invperm)[G->Jerrum[u].perm[v]], h);
		}
	} else {
		perm_cpy(G->G->n, G->G->perm[G->Jerrum[min].perm[anc[min]]], h);
		for (u=anc[min]; u!=i; u=v) {
			v = anc[u];
			perm_mult(G->G->n, h, (u<v ? G->G->perm : G->G->invperm)[G->Jerrum[u].perm[v]], h);
		}
		perm_mult(G->G->n, h, p, h);
		for (u=j; u!=min; u=v) {
			v = anc[u];
			perm_mult(G->G->n, h, (u<v ? G->G->perm : G->G->invperm)[G->Jerrum[u].perm[v]], h);
		}
	}
	return TRUE;
}


#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
static inline void JerrumInsertGenerator(permgrpc* G, permutation p, elt i, elt j)
/*
 * Insert the generator p, where i is the smallest point in the support of p and j=p[i].
 */
{
	elt  g;

	extract_LSB32(&(G->freeperm), &g);  /* sorry GCC: there must be a free slot, so g *is* initialised here */
	perm_cpy(G->G->n, p, G->G->perm[g]);
	perm_inv(G->G->n, p, G->G->invperm[g]);
	G->Jerrum[i].neighbours |= BIT(j);
	G->Jerrum[i].perm[j] = g;
	G->Jerrum[j].neighbours |= BIT(i);
	G->Jerrum[j].perm[i] = g;
}
#pragma GCC diagnostic pop


static inline void JerrumRemoveGenerator(permgrpc* G, elt i, elt j)
/*
 * Remove the generator associated to the edge from i to j.
 */
{
	elt  g;

	g = G->Jerrum[i].perm[j];
	G->freeperm |= BIT(g);
	G->Jerrum[i].neighbours ^= BIT(j);  /* bit j is set, so this clears it */
	G->Jerrum[j].neighbours ^= BIT(i);  /* bit i is set, so this clears it */
}


void permgrpc_addGenerator(permgrpc* G, permutation p)
/*
 * Add the permutation p as a generator of G.  The permutation p *must* be nontrivial!
 */
{
	elt  i, j;

	i = perm_minSupport(G->G->n, p);
	j = p[i];
	if (G->Jerrum[i].neighbours & BIT(j)) {
		elt          k;
		permutation  h;
		/* j is already a neighbour of i; unless we have a duplicate generator... */
		if (perm_cmp(G->G->n, p, G->G->perm[k=G->Jerrum[i].perm[j]])
				&& perm_cmp(G->G->n, p, G->G->invperm[k])) {
			/* ...there is a generator k so that h=g*k^-1 fixes i; add h instead of g */
			perm_mult(G->G->n, p, G->G->invperm[k], h);
			permgrpc_addGenerator(G, h);
		}
	} else {
		permutation  h;
		elt          m, nm;
		if (JerrumCreatesCycle(G, i, j, p, h, &m, &nm)) {
			if (m != i) {
				JerrumRemoveGenerator(G, m, nm);
				JerrumInsertGenerator(G, p, i, j);
			}
			if (!perm_isId(G->G->n, h))
				permgrpc_addGenerator(G, h);
		} else {
			JerrumInsertGenerator(G, p, i, j);
		}
	}

}


void permgrp_printGenerators(permgrp* G, elt offset)
/*
 * TEST FUNCTION:  Print current generators (in array notation).
 */
{
	elt  i;

	for (i=0; i<G->ngens; i++)
		perm_print(G->n, G->perm[i], offset);
}
