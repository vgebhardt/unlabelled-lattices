/*
 * build.h
 *
 * Created on: 22 Mar 2014
 * Last modified: 5 Feb 2019
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2019 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef BUILD_H_
#define BUILD_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>



/* === START TUNING PARAMETERS === */

/* Constants determining the size of data structures:
 *  - MAXN: maximal lattice size (including upper and lower bounds)
 *  - MAXN_EVEN: 1 if MAXN is even; 0 if MAXN is odd
 *  - LD_MAXN_2: ceiling( log_2(MAXN-2) )
 */
#ifdef TEST_P2_16
#define MAXN 12
#define MAXN_EVEN 1
#define LD_MAXN_2 4
#elif defined TEST_P2_32
#define MAXN 18
#define MAXN_EVEN 1
#define LD_MAXN_2 5
#elif defined TEST_MAXN_ODD
#define MAXN 13
#define MAXN_EVEN 0
#define LD_MAXN_2 4
#else
#define MAXN 22
#define MAXN_EVEN 1
#define LD_MAXN_2 5
#define HARDCODE_MAXN_22
#endif


/* Precomputation of Beneš networks for all permutations of degree up to BENES_SMALL; this requires tuning,
 * balancing saved computations against increased number of cache misses due to larger memory hoofprint.
 *
 * Memory requirements for precomputed Beneš networks are as follows:
 * - 32-bit architecture:
 *     BENES_SMALL = 8:  3.8 MB
 *     BENES_SMALL = 9: 32.8 MB
 * - 64-bit architecture:
 *     BENES_SMALL = 8:  5.7 MB
 *     BENES_SMALL = 9: 49.2 MB
 */
#define BENES_SMALL 10


/*
 * Whether a __builtin_prefetch command should be issued for any created Beneš network, requesting that the
 * data be kept in the cache for subsequent read operations [__builtin_prefetch (..., 0, 3)].
 */
#define BENES_DOPREFETCH 0


/*
 * Initial size for orbits of packed antichain lists.
 */
#define ORBITS_INITIAL_ORBSPACE 128


/*
 * Size of hashtable used for positions when computing orbits of packed antichain lists.
 */
#define ORBITS_HASHTABLE_LD_SIZE 16


/*
 * Initial size for secondary lists in hashtable nodes.
 */
#define ORBITS_HASHTABLE_INITIAL_LIST_SIZE 4


/*
 * Initial size for space for candidates in the evaluation of implicit stabiliser data when computing orbits
 * of packed antichain lists.
 */
#define ORBITS_INITIAL_SISPACE 32


/*
 * Size of output buffers (in bytes); used by threaded version only.
 */
#define THREADED_OUTPUT_BUFFER_SIZE 32768


/*
 * Buffer size per thread used for master enumeration; used by threaded version only.
 */
#define THREADED_LATTICE_BUFFER_SIZE_MULT 8



/* === END TUNING PARAMETERS === */



#if defined HARDCODE_MAXN_22 && MAXN != 22
#error "HARDCODE_MAXN_22 conflicts with setting for MAXN"
#endif


#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)

#define TRUE   1
#define FALSE  0

#define BIT(i)  (1UL << (i))
#define NBIT(i)  (~(1UL << (i)))


/* for (number) of elements, levels, positions, comparison of elements; some of these may be signed! */
typedef signed char    elt;

/* booleans */
typedef int            bool;

/* bit masks:
 *  - flags32: for sets of elements; signed to allow comparison by subtraction!
 *  - flags64: for packed tuples of sets of elements; signed to allow comparison by subtraction!
 */
#ifdef TEST_P2_16
typedef unsigned short      flags32;
typedef unsigned short      flags64;
#elif defined TEST_P2_32
typedef unsigned int        flags32;
typedef unsigned int        flags64;
#else
typedef unsigned int        flags32;
typedef unsigned long long  flags64;
#endif
#define BITSPERFLAGS64 (8*sizeof(flags64))

#if MAXN_EVEN
#define FSIZE (MAXN-2)*(MAXN-2)/4
#else
#define FSIZE (MAXN-3)*(MAXN-1)/4
#endif


#define PREFETCH(x)                                        \
            do {                                           \
            	void*  ptr = &x;                           \
            	int    i;                                  \
            	for (i=1+(sizeof(x)-1)/64; i--; ptr+=64)   \
            		__builtin_prefetch(ptr);               \
            } while (0);


static void erri(int error)
/*
 * TEST FUNCTION:  Hook for placing a debugger breakpoint.
 *
 * Error codes:  -1 -- user error; bad parameters
 *               -2 -- build error
 *               -3 -- not implemented
 *               -4 -- reality check failed
 */
{
	exit(error);
}


static inline void bitmap_init(elt N)
/*
 * Initialise global bitmaps to size N.
 */
{
	elt      bits, a1, b1, a2, b2;

	bits = BITSPERFLAGS64;
	if (MAXN > bits) {
		fprintf(stderr, "Build problem: flags64 cannot hold MAXN bits!\n");
		erri(-2);
	}
	b1 = (elt)lround(floor(sqrt(bits)));
	a1 = bits/b1;
	b2 = (elt)lround(ceil(sqrt(bits)));
	a2 = bits/b2;
	if (MAXN > 2*a1+b1+2 || MAXN > 2*a2+b2+2) {
		fprintf(stderr, "Build problem: packed antichain lists don't fit into two flags64!\n");
		erri(-2);
	}
	if (N > MAXN) {
		printf("The maximal number of elements with this executable is %d.\n", MAXN);
		erri(-1);
	}
	if (MAXN_EVEN & MAXN) {
		fprintf(stderr, "Build problem: Specified parity of MAXN is wrong!\n");
		erri(-2);
	}
	if (lround(ceil(log(MAXN-2)/log(2))) != LD_MAXN_2) {
		fprintf(stderr, "Build problem: Specified value of LD_MAXN_2 is wrong!\n");
		erri(-2);
	}
	if (FSIZE >= 1 << (8*sizeof(elt)-1)) {
		fprintf(stderr, "Build problem: elt cannot hold offsets in antichain.F!\n");
		erri(-2);
	}
}


static inline flags32 allBits32(elt n)
/*
 * Return a mask with bits 0..n-1 set.
 */
{
	return (1UL << n) - 1UL;
}


static inline flags64 allBits64(elt n)
/*
 * Return a mask with bits 0..n-1 set.
 */
{
	return (1ULL << n) - 1ULL;
}


static inline bool get_MSB32(flags32 f, elt* i)
/*
 * If f is zero, return 0.  Otherwise, set *i to the rank of the most significant bit set in f and return 1.
 * NOTE:
 */
{
	if (!f)
		return FALSE;
	*i = 31 - __builtin_clz(f);
	return TRUE;
}


static inline bool extract_MSB32(flags32* f, elt* i)
/*
 * If f is zero, return 0.  Otherwise, set *i to the rank of the most significant bit set in f, clear the bit and return 1.
 * NOTE:
 */
{
	if (!(*f))
		return FALSE;
	*i = 31 - __builtin_clz(*f);
	*f ^= BIT(*i);
	return TRUE;
}


static inline bool get_LSB32(flags32 f, elt* i)
/*
 * If f is zero, return 0.  Otherwise, set *i to the rank of the least significant bit set in f and return 1.
 */
{
	if (!f)
		return FALSE;
	*i = __builtin_ctz(f);
	return TRUE;
}


static inline bool extract_LSB32(flags32* f, elt* i)
/*
 * If f is zero, return 0.  Otherwise, set *i to the rank of the least significant bit set in f, clear the bit and return 1.
 */
{
	if (!(*f))
		return FALSE;
	*i = __builtin_ctz(*f);
	*f ^= BIT(*i);
	return TRUE;
}

#endif /* BUILD_H_ */
