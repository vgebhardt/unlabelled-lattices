/*
 * threaded.c
 *
 * Created on: 07 July 2015
 * Last modified: 23 Feb 2019
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2019 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef THREADED

#include <stdio.h>

#include "threaded.h"


static pthread_mutex_t  mutex_state = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t  mutex_buffer = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t  mutex_tasks = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t   cond_lattice = PTHREAD_COND_INITIALIZER;
static pthread_cond_t   cond_slot = PTHREAD_COND_INITIALIZER;

static task*  tasks = 0;
static unsigned int  tasks_wpos = 0;
static unsigned int  tasks_rpos = 0;

/* The state of the enumeration is managed by the following global variables:
 *
 * char*  statefilename  -- The file name of the statefile of an enumeration that is to be continued.
 * FILE*  continuing     -- FILE structure for the statefile of an enumeration that is to be continued.
 * char*  nextskip       -- The "prefix" identifying the next lattice of a master enumeration that has
 *                          already been processed; the master enumeration will read the number of
 *                          descendants from the state file instead of generating a slave job.
 * int    action         -- The state of the enumeration and the action of slaves is defined as follows:
 *                           1:  A "coarse" master enumeration is running, with the aim to produce fewer
 *                               slave jobs, each taking longer to complete.
 *                           0:  A preliminary master enumeration has completed and at least one slave
 *                               is idle; a switch to  a "finer" master enumeration is indicated, with
 *                               the aim to produce more slave jobs, each taking less time to complete.
 *                                  This instructs slaves to drop their current enumeration immediately
 *                               and discard its result. -- While this may result in wasting time that
 *                               had been spent on an "almost complete" enumeration, it avoids having
 *                               to wait for a few slaves that are stuck in a very long enumeration.
 *                          -1:  The finest master enumeration that is scheduled is running; all slaves
 *                               will finish their enumeration even if other slaves are idle.
 */
char*  statefilename;
FILE*  continuing = 0;
char*  nextskip = 0;
int    action = 1;



static inline bool getSlotToFill(lattEnum_Buffer* E, int* slot)
/*
 * If there is a free slot in *E, clear the corresponding flag in E->freeslots, set the
 * corresponding flag in E->fullslots, set *slot to the index of the slot, and return TRUE;
 * otherwise, return FALSE.
 */
{
	elt  flagpos, idx;

	for (flagpos=0; flagpos<E->nSlotFlags; flagpos++)
		if (E->freeslots[flagpos]) {
			idx = __builtin_ctzll(E->freeslots[flagpos]);
			E->freeslots[flagpos] ^= BIT(idx);
			E->fullslots[flagpos] ^= BIT(idx);
			*slot = flagpos*8*sizeof(flags64) + idx;
			return TRUE;
		}
	return FALSE;
}


static inline void markSlotFree(lattEnum_Buffer* E, int slot)
/*
 * Set the flag corresponding to slot in E->freeslots.
 */
{
	elt  slotpos, idx;

	slotpos = slot / (8*sizeof(flags64));
	idx = slot % (8*sizeof(flags64));
	E->freeslots[slotpos] ^= BIT(idx);
}


static inline bool getSlotToUse(lattEnum_Buffer* E, int* slot)
/*
 * If there is a full slot in *E, clear the corresponding flag in E->fullslots, set *slot to the index
 * of the slot, and return TRUE; otherwise, return FALSE.
 */
{
	elt  flagpos, idx;

	for (flagpos=0; flagpos<E->nSlotFlags; flagpos++)
		if (E->fullslots[flagpos]) {
			idx = __builtin_ctzll(E->fullslots[flagpos]);
			E->fullslots[flagpos] ^= BIT(idx);
			*slot = flagpos*8*sizeof(flags64) + idx;
			return TRUE;
		}
	return FALSE;
}


lattEnum_Buffer* lattEnum_Buffer_create(lattice* L, elt N, elt Nmin, int nThreads, unsigned int depth, char* prefix, globals* GD)
/*
 * Return a structure for enumerating descendants of *L of size at least Nmin and at most N, storing up to
 * THREADED_LATTICE_BUFFER_SIZE_MULT*nThreads descendants at a time.
 *
 * Side effect:  Sets static variable nSlotFlags.
 */
{
	lattEnum_Buffer*  E;
	int               nBuffers;
	elt               nSlotFlags, i;

	if (L && (N-2 < L->n || Nmin-2 < L->n)) {
		printf("BAD PARAMETERS in lattEnum_Buffer_create: given lattice larger than target size!\n");
		erri(-1);
	}
	E = (lattEnum_Buffer*)malloc(sizeof(lattEnum_Buffer));
	E->reg = lattEnum_Buffer_register;
	E->GD = GD;
	E->L = L;
	E->count = 0;
	E->N = N-2;
	E->Nmin = Nmin-2;
	E->done = FALSE;
	E->bufsize = nBuffers = nThreads*THREADED_LATTICE_BUFFER_SIZE_MULT;
	E->lattices = (lattice**)calloc(nBuffers, sizeof(lattice*));
	E->indices = (unsigned long long*)malloc(nBuffers*sizeof(unsigned long long));
	E->nSlotFlags = nSlotFlags = (1+(nBuffers-1)/(8*sizeof(flags64)));
	E->freeslots = (flags64*)malloc(nSlotFlags*sizeof(flags64));
	E->fullslots = (flags64*)calloc(nSlotFlags, sizeof(flags64));
	for (i=0; i<nSlotFlags-1; i++)
		E->freeslots[i] = ~((flags64)0);
	nBuffers %= (8*sizeof(flags64));
	E->freeslots[nSlotFlags-1] = nBuffers ? (((flags64)1) << nBuffers)-1 : ~((flags64)0);
	E->depth = depth;
	E->prefix = prefix;
	E->sumskipped = 0;
	return E;
}


void lattEnum_Buffer_register(lattEnum* E, lattice* L)
/*
 * Register the lattice L.
 */
{
	lattEnum_Buffer*  EE = (lattEnum_Buffer*)E;
	int               wpos;

	if (nextskip) {
		char*  match;
		bool   skip;
		if (strlen(EE->prefix)) {
			match = strstr(nextskip, EE->prefix)+strlen(EE->prefix)+1;
			skip = match && !strchr(match,':');
		} else {
			match = strstr(nextskip, "::")+2;
			skip = match && !strchr(match,':');
		}
		if (skip) {
			unsigned long long   idx;
			long long            skippedcount;
			sscanf(match, "%llu %lld", &idx, &skippedcount);
			if (idx == EE->count) {
				size_t  nread;
				if (skippedcount == -1) {
					lattice*  LL = (lattice*)malloc(sizeof(lattice));
					char*  pre = (char*)malloc((strlen(EE->prefix)+(idx?log10(idx)+1:1)+2)*sizeof(char));
					lattice_cpy(L, LL);
					sprintf(pre, "%s:%llu", EE->prefix, idx);
					tasks_register(LL, EE->N+1+2, EE->depth+1, pre);
				} else {
					EE->sumskipped += skippedcount;
				}
				free(nextskip);
				nextskip = 0;
				if (getline(&nextskip, &nread, continuing) <= 0) {
					free(nextskip);
					nextskip = 0;
					fclose(continuing);
					continuing = 0;
				}
				EE->count++;
				return;
			}
		}
	}
	pthread_mutex_lock(&mutex_buffer);
	while (!getSlotToFill(EE, &wpos))
		pthread_cond_wait(&cond_slot, &mutex_buffer);
	if (EE->lattices[wpos])
		lattice_clearStabiliser(EE->lattices[wpos]);
	else
		EE->lattices[wpos] = (lattice*)malloc(sizeof(lattice));
	lattice_cpy(L, EE->lattices[wpos]);
	EE->indices[wpos] = EE->count++;
	pthread_mutex_unlock(&mutex_buffer);
	pthread_cond_signal(&cond_lattice);
}


void lattEnum_Buffer_free(lattEnum_Buffer* E)
/*
 * Free all memory.
 */
{
	int       i;
	lattice*  L;

	for (i=0; i<E->bufsize; i++)
		if ((L=E->lattices[i]))
			lattice_free(L);
	free(E->lattices);
	free(E->indices);
	free(E->freeslots);
	free(E->fullslots);
	free(E);
}


void lattEnum_Buffer_finalise(lattEnum_Buffer* E)
/*
 * Notify all watchers that there won't be any more lattices.
 */
{
	pthread_mutex_lock(&mutex_buffer);
	E->done = TRUE;
	pthread_mutex_unlock(&mutex_buffer);
	pthread_cond_broadcast(&cond_lattice);
}


void tasks_init()
/*
 * Initialise the global tasks list to contain the lattice with 2 elements.
 */
{
	tasks = (task*)malloc(sizeof(task));
	tasks[0].depth = 0;
	tasks[0].L = (lattice*)malloc(sizeof(lattice));
	lattice_init_2(tasks[0].L);
	tasks[0].prefix = (char*)calloc(1, sizeof(char));
	tasks[tasks_wpos].Nmin = 3;
	tasks_wpos++;
}


void tasks_register(lattice* L, elt Nmin, unsigned int depth, char* prefix)
/*
 * Add the lattice *L to the global task list.  L and prefix are transferred.
 */
{
	pthread_mutex_lock(&mutex_tasks);
	tasks = (task*)realloc(tasks, (tasks_wpos+1)*sizeof(task));
	tasks[tasks_wpos].depth = depth;
	tasks[tasks_wpos].L = L;
	tasks[tasks_wpos].prefix = prefix;
	tasks[tasks_wpos].Nmin = Nmin;
	tasks_wpos++;
#if 0
	tasks_print();
#endif
	pthread_mutex_unlock(&mutex_tasks);
}


task* tasks_next()
/*
 * Return a pointer to the next task, or NULL if there is no more task.
 */
{
	if (tasks_rpos < tasks_wpos)
		return tasks+(tasks_rpos++);
	else
		return (task*)0;
}


task* tasks_all()
/*
 * Return a pointer to the task list.
 */
{
	return tasks;
}


int tasks_count()
/*
 * Return the number of tasks in the task list.
 */
{
	return tasks_wpos;
}


void tasks_free()
/*
 * Clean up the global tasks list.
 */
{
	int  i;
	for (i=0; i<tasks_wpos; i++) {
		lattice_free(tasks[i].L);
		free(tasks[i].prefix);
	}
	free(tasks);
}


void tasks_print()
/*
 * Test function:  Print the global task list.
 */
{
	int  i;
	printf("\n\nGLOBAL TASK LIST:\n\n");
	for (i=0; i<tasks_wpos; i++) {
		lattice_print(tasks[i].L);
		printf("-----------------------\n\n");
	}
}


void* threaded_thread(void* arg)
/*
 * Function launching a slave thread.
 *
 * arg should be of type threaded_arg.  The function starts an enumeration with parameters given by EN for every
 * lattice in ES; the latter must have the mode lattEnumModeArray.
 */
{
	threaded_arg*        args;
	unsigned long long   idx;
	int                  rpos = -1;
	FILE*                statefile;
	long long*           pcount;
	globals              GD;
	lattEnum*            EN;
	char*                statebuf;
	int                  statepos;

	args = (threaded_arg*)arg;
#ifdef NUMA
	numa_set_localalloc();
	numa_run_on_node(args->numanode);
#ifdef VERBOSE
	printf("NUMA: Bound thread to node %d.\n", args->numanode);
#endif
#endif
	globals_init(&GD);
#ifdef TARGET_COUNT
	EN = lattEnum_Count_create(0, args->N, args->Nmin, &GD);
#elif defined TARGET_CATALOGUE
	EN = lattEnum_stdout_create(0, args->N, args->Nmin, &GD);
#endif
	pcount = (long long*)malloc(sizeof(unsigned long long));
	*pcount = 0;
	statebuf = (char*)malloc(THREADED_OUTPUT_BUFFER_SIZE);
	*statebuf = 0;
	statepos = 0;
	while (TRUE) {
		pthread_mutex_lock(&mutex_buffer);
		if (rpos >= 0) {
			markSlotFree(args->ES, rpos);
			pthread_cond_signal(&cond_slot);
		}
		rpos = -1;
		while (!(getSlotToUse(args->ES, &rpos) || args->ES->done))  /* must test for full slots even if done! */
			pthread_cond_wait(&cond_lattice, &mutex_buffer);
		if (rpos == -1)
			break;
		EN->L = args->ES->lattices[rpos];
		idx = args->ES->indices[rpos];
		pthread_mutex_unlock(&mutex_buffer);
		EN->count = 0;
		lattEnum_doEnumeration(EN);  /* will be aborted if action is changed to 0 by another thread */
		if (action) {
#ifdef TARGET_CATALOGUE
			lattEnum_stdout_flush((lattEnum_stdout*)EN);
#endif
			statepos += sprintf(statebuf+statepos, "%d:%s:%llu %llu\n", args->ES->depth, args->ES->prefix, idx, EN->count);
			*pcount += EN->count;
		} else {
			char* prefix;
#ifdef TARGET_CATALOGUE
			((lattEnum_stdout*)EN)->outpos = 0;
#endif
			prefix = (char*)calloc(strlen(args->ES->prefix)+2+(idx?log10(idx)+1:1), sizeof(char));
			sprintf(prefix, "%s:%llu", args->ES->prefix, idx);
			tasks_register(args->ES->lattices[rpos], args->Nmin, args->ES->depth+1, prefix);
			args->ES->lattices[rpos] = 0;
			statepos += sprintf(statebuf+statepos, "%d:%s:%llu -1\n", args->ES->depth, args->ES->prefix, idx);
		}
		if (statepos > THREADED_OUTPUT_BUFFER_SIZE-(THREADED_OUTPUT_BUFFER_SIZE>>3)) {
			/*
			 * Buffering the output to the state file may result in duplicating lattices for TARGET_CATALOGUE.
			 * However, this can easily be resolved using sort and uniq, and is probably preferable to requesting
			 * a mutex and opening/closing a file every time an enumeration is completed.
			 */
			pthread_mutex_lock(&mutex_state);
			statefile = fopen(statefilename, "a");
			fprintf(statefile, "%s", statebuf);
			fclose(statefile);
			pthread_mutex_unlock(&mutex_state);
			statepos = 0;
		}
	}
	pthread_mutex_unlock(&mutex_buffer);
	if (statepos) {
		pthread_mutex_lock(&mutex_state);
		statefile = fopen(statefilename, "a");
		fprintf(statefile, "%s", statebuf);
		fclose(statefile);
		pthread_mutex_unlock(&mutex_state);
	}
#ifdef TARGET_COUNT
	lattEnum_Count_free(EN);
#elif defined TARGET_CATALOGUE
	lattEnum_stdout_free(EN);
#endif
	globals_free(&GD);
	if (action > 0)
		action = 0;
	free(statebuf);
	return (void*)pcount;
}


unsigned long long threaded_master(task* t, int nTasks, elt N, elt M, int nThreads, globals* GD)
/*
 * Start a master thread launching a multi-threaded enumeration with nThreads for the task list t containing nTasks tasks
 * with target lattice size N and master enumeration target size M.  All tasks must have the same minimum size.
 */
{
	pthread_t*          T;
	int                 i;
	lattEnum_Buffer*    E;
	threaded_arg*       args;
	unsigned long long  count;

#if 0
	printf("### starting master with N=%d, M=%d, Nmin=%d\n", N, M, t->Nmin);
#endif

	T = (pthread_t*)malloc(nThreads*sizeof(pthread_t));
	args = (threaded_arg*)malloc(nThreads*sizeof(threaded_arg));
	E = (lattEnum_Buffer*)lattEnum_Buffer_create(t->L, M, t->Nmin, nThreads, t->depth, t->prefix, GD);
	for (i=nThreads; i--; ) {
		int  ret;
		args[i].ES = E;
		args[i].N = N;
		args[i].Nmin = M+1;
#ifdef NUMA
		args[i].numanode = i % (numa_max_node()+1);
#endif
		if ((ret=pthread_create(T+i, NULL, threaded_thread, (void*)(args+i)))) {
			fprintf(stderr,"Error - pthread_create() return code: %d\n",ret);
			erri(-4);
		}
	}
	lattEnum_doEnumeration((lattEnum*)E);
	for (i=1; i<nTasks; i++) {
		E->L = t[i].L;
		E->prefix = t[i].prefix;
		lattEnum_doEnumeration((lattEnum*)E);
	}
	lattEnum_Buffer_finalise(E);
	count = E->sumskipped;
	for (i=nThreads; i--; ) {
		unsigned long long*  pcount;
		pthread_join(T[i], (void**)(&pcount));
		count += *pcount;
		free(pcount);
	}
	lattEnum_Buffer_free(E);
	free(T);
	free(args);
	return count;
}

#endif
