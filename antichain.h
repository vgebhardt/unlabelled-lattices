/*
 * antichain.h
 *
 * Created on: 22 Mar 2014
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef ANTICHAIN_H_
#define ANTICHAIN_H_

#include "build.h"
#include "permutation.h"
#include "permgrp.h"
#include "lattice.h"
#include "hashtable.h"
#include "globals.h"


/* We enumerate lists of k lattice-antichains by enumerating lists of k up-closed sets
 * satisfying the lattice-antichain condition.
 *
 * Lattice:
 *    lattice*   L            -- the underlying lattice
 *
 * Pointer to the process'/thread's global data:
 *    globals*   GD
 *
 * Additional lattice-antichain data:
 *    elt         k            -- number of elements to be constructed on the new level
 *    elt         cp           -- current position for construction
 *    elt         cl           -- current level of construction
 *    flags32     cmc          -- mask for the elements on the current level
 *    flags32     cm           -- mask for the elements on or below the current level
 *    flags32     cop[MAXN-3]  -- cop[i] & BIT[j] indicates whether i \meet j = 0
 *    stabiliser  SD[MAXN-2]   -- stabiliser data for each level; see below
 *    elt         Fpos         -- current position in the array F; Fpos=(nLev-2-cl)*k+cp
 *    flags32     O[MAXN-3]    -- O[j]: all (optional) elements chosen for antichain j (on any level!)
 *    flags32     F[FSIZE]     -- F[(nLev-2-i)*k+j]: all elements for antichain j forced by choice on
 *                                  levels i, i+1,...,nLev-2
 *
 * Stabiliser data:
 *    permgrpc*  ST_  -- space for constructing stabilisers of (partial) antichain configurations
 *    permgrp*   ST   -- indirection to ST_[i]->G (without reference count); ST[nLev-1] points to stabiliser of L
 *    flags32    SI   -- SI[i] & BIT[j] iff (j-1 j) stabilises L and the antichains on levels >= i
 *    flags32    bl   -- bl[i] & BIT[j] indicates whether the restrictions to levels >= i of the up-closed sets
 *                          at positions j-1 and j are identical
 */


typedef struct stabiliser stabiliser;
struct stabiliser {
	permgrpc*  ST_;
	permgrp*   ST;
	flags32    SI;
	flags32    bl;
};


typedef struct antichaindata antichaindata;
struct antichaindata {
	lattice*    L;
	globals*    GD;
	flags32     cop[MAXN-3];
	elt         k;
	elt         cl;
	flags32     cmc;
	flags32     cm;
#ifndef FILTER_GRADED
	stabiliser  SD[MAXN-2];
#else
	stabiliser  SD[2];
#endif
	elt         cp;
	elt         Fpos;
#ifndef FILTER_GRADED
	flags32     O[MAXN-3];
	flags32     F[FSIZE];
#else
	flags32     O[MAXN-3];
	flags32     F[MAXN-3];
#endif
} __attribute__((aligned(64)));


/* macro to access stabiliser level corresponding to AD->cl */
#ifndef FILTER_GRADED
#define AD_CSL (AD->cl)
#else
#define AD_CSL 0
#endif


static inline int flags32_cmp(flags32 A, flags32 B)
/*
 * Compare the weight of the antichains A and B.
 *
 * Return value: <0 if wt(A) < wt(B)
 *               0  if A = B
 *               >0 if wt(A) > wt(B)
 */
{
	if (A < B)
		return -1;
	else if (A > B)
		return 1;
	else
		return 0;
}


void antichaindata_init(lattice* L, elt k, antichaindata* AD, globals* GD);
/*
 * Initialise antichain data *AD for the lattice L with k elements to be added on the new level.
 * AD should be allocated.
 */


void antichaindata_reinit(antichaindata* AD, elt k);
/*
 * Modify antichain data *AD for k elements to be added on the new level.
 * AD should be initialised.
 */


static inline permgrpc* antichaindata_ensureStabiliser(antichaindata* AD, elt lev)
/*
 * Ensure that the stabiliser AD->SD[lev].ST_ exists, and return AD->SD[lev].ST_.
 */
{
	if (!AD->SD[lev].ST_)
		AD->SD[lev].ST_ = permgrpc_alloc();
	return AD->SD[lev].ST_;
}


static inline void antichaindata_clear(antichaindata* AD)
/*
 * Free dynamically allocated memory; calls to antichaindata_init and antichaindata_clear should match.
 */
{
#ifndef FILTER_GRADED
	elt  i;

	for (i=0; i<AD->L->nLev-1; i++)
		if (AD->SD[i].ST_)
			permgrpc_delete(AD->SD[i].ST_);
#else
	if (AD->SD[0].ST_)
				permgrpc_delete(AD->SD[0].ST_);
#endif
}


static inline void antichaindata_updateBlocks(antichaindata* AD, flags32 P, flags32* Q)
/*
 *  Given a partition P of elements, return in *Q a refinement of P such that the optional
 *  choices on the current level of AD are constant on every element of *Q.
 */
{
	flags32  B;
	elt      i;

	B = *Q = P;
	while (get_LSB32(B, &i)) {
		B ^= BIT(i);  /* bit i is set, so this clears it */
		if ((AD->O[i-1] ^ AD->O[i]) & AD->cmc)
			*Q ^= BIT(i);  /* bit i is set, so this clears it */
	}
}


static inline void antichaindata_incrementLevel(antichaindata *AD)
/*
 * Increment the current level of *AD.
 */
{
	(AD->cl)++;
	AD->Fpos--;
	AD->cmc = (BIT(AD->L->lev[AD->cl+1]) - BIT(AD->L->lev[AD->cl]));
	AD->cm = (BIT(AD->L->n) - BIT(AD->L->lev[AD->cl]));
	AD->cp = AD->k-1;
}


static inline void antichaindata_incrementLevel_1(antichaindata *AD)
/*
 * Increment the current level of *AD.
 */
{
	(AD->cl)++;
	AD->Fpos--;
	AD->cmc = (BIT(AD->L->lev[AD->cl+1]) - BIT(AD->L->lev[AD->cl]));
	AD->cm = (BIT(AD->L->n) - BIT(AD->L->lev[AD->cl]));
}


static inline bool antichaindata_stepCurrentPosition(antichaindata* AD)
/*
 * If possible, step antichaindata counter for the current position.  The lattice-antichain condition
 * is NOT verified.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */
{
	flags32  W, nW;
	elt      F1pos;

	if (!(AD->O[AD->cp] & AD->cmc))
		return FALSE;

	AD->O[AD->cp] -= BIT(AD->L->lev[AD->cl]);
	if ((F1pos = AD->Fpos - AD->k) >= 0){
		AD->O[AD->cp] &= ~(AD->F[F1pos] & AD->cmc);
		W = (AD->F[F1pos] | AD->O[AD->cp]) & AD->cm;
		if (AD->cp)
			nW = (AD->F[F1pos-1] | AD->O[AD->cp-1]) & AD->cm;
		else
			nW = (AD->L->up[AD->L->n-1] & AD->cm) ^ BIT(AD->L->n-1);
	} else {
		W = AD->O[AD->cp];
		if (AD->cp)
			nW = AD->O[AD->cp-1];
		else
			nW = (AD->L->up[AD->L->n-1] & AD->cm) ^ BIT(AD->L->n-1);
	}

	/* We only want antichains that have a weight no less than the weight of the preceding
	 * element, and that intersect the lowest level of the old lattice.                    */
	if (flags32_cmp(W,nW) < 0 || !W)  /* catches antichains not intersecting the lowest level, i.e. */
		return FALSE;                   /* AD->cl == AD->L->nLev-2 && !(AD->O[AD->cp] & AD->cm)       */

	return TRUE;
}


static inline bool antichaindata_stepCurrentPosition_1(antichaindata* AD)
/*
 * If possible, step antichaindata counter for the current position.  The lattice-antichain condition
 * is NOT verified.  Special case of a single antichain.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */
{
	flags32  W, nW;

	if (!(AD->O[0] & AD->cmc))
		return FALSE;

	AD->O[0] -= BIT(AD->L->lev[AD->cl]);
	if (AD->Fpos) {
		AD->O[0] &= ~(AD->F[AD->Fpos-1] & AD->cmc);
		W = (AD->F[AD->Fpos-1] | AD->O[0]) & AD->cm;
	} else {
		W = AD->O[0];
	}

	/* We only want antichains that have a weight no less than the weight of the preceding element. */
	nW = (AD->L->up[AD->L->n-1] & AD->cm) ^ BIT(AD->L->n-1);
	if (flags32_cmp(W,nW) < 0 || !W)    /* catches antichains not intersecting the lowest level, i.e. */
		return FALSE;                     /* AD->cl == AD->L->nLev-2 && !(AD->O[0] & AD->cm)            */

	return TRUE;
}


static inline bool antichaindata_step(antichaindata* AD)
/*
 * If possible, step antichaindata counters starting from the current on the current level.
 * The lattice-antichain conditions are NOT verified.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */
{
	while (!antichaindata_stepCurrentPosition(AD)) {
		if (AD->cp) {
			AD->cp--;
			AD->Fpos--;
#ifdef FILTER_GRADED
		}
#else
		} else if (AD->cl < AD->L->nLev-2)
			antichaindata_incrementLevel(AD);
#endif
		else
			return FALSE;
	}

	return TRUE;
}


static inline bool antichaindata_step_1(antichaindata* AD)
/*
 * If possible, step antichaindata counters starting from the current on the current level.
 * The lattice-antichain conditions are NOT verified.  Special case of a single antichain.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */
{
	while (!antichaindata_stepCurrentPosition_1(AD)) {
#ifndef FILTER_GRADED
		if (AD->cl < AD->L->nLev-2)
			antichaindata_incrementLevel_1(AD);
		else
#endif
			return FALSE;
	}
	return TRUE;
}


bool antichaindata_step_FUNC(antichaindata* AD);
/*
 * If possible, step antichaindata counters starting from the current on the current level.
 * The lattice-antichain conditions are NOT verified.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */


bool antichaindata_step_1_FUNC(antichaindata* AD);
/*
 * If possible, step antichaindata counters starting from the current on the current level.
 * The lattice-antichain conditions are NOT verified.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */


bool antichaindata_next(antichaindata* AD);
/*
 * Advance *AD to the next canonical configuration of lattice-antichains.
 *
 * Return value: TRUE if successful; FALSE if no further canonical configuration exists.
 */


bool antichaindata_next_1(antichaindata* AD);
/*
 * Advance *AD to the next canonical configuration of lattice-antichains.  Special case of a single antichain.
 *
 * Return value: TRUE if successful; FALSE if no further canonical configuration exists.
 */


void antichaindata_prepareLattice(antichaindata* AD, lattice* L, lattice* LA);
/*
 * Set generic data in *LA that applies to every lattice obtained from *L by adding AD->k atoms
 * covered by lattice antichains:  number of elements, levels, as well as
 *    up[i]  .....
 *   (lo[i]) lo[j]
 * for 1 < i < L->n <= j < LA->n.  lo[i] may be modified in antichaindata_generateLattice.
 */


void antichaindata_generateLattice(antichaindata* AD, lattice* L, lattice* LA);
/*
 * Set *LA to the lattice obtained from *L by adding AD->k new atoms covered by the elements in the
 * lattice antichains described by *AD:  stabiliser, as well as
 *   .....  up[j]
 *   lo[i]  .....
 * for 1 < i < L->n <= j < LA->n.  ..... denotes data set in antichaindata_prepareLattice.
 * Note: The stabiliser of *LA carries a reference count.
 */


void antichaindata_generateLattice_1(antichaindata* AD, lattice* L, lattice* LA);
/*
 * Set *LA to the lattice obtained from *L by adding AD->k new atoms covered by the elements in the
 * lattice antichains described by *AD:  stabiliser, as well as
 *   .....  up[j]
 *   lo[i]  .....
 * for 1 < i < L->n <= j < LA->n.  ..... denotes data set in antichaindata_prepareLattice.
 * Special case of a single antichain.
 * Note: The stabiliser of *LA carries a reference count.
 */


void antichaindata_printAntichains(antichaindata* AD);
/*
 * TEST FUNCTION: Print all set bits in each antichain in *AD.
 */


void antichaindata_printCounters(antichaindata* AD);
/*
 * TEST FUNCTION: Print all counters for each position in *AD.
 */


void antichaindata_printCountersF(antichaindata* AD);
/*
 * TEST FUNCTION: Print the counters F[i][j] for each position and each level in *AD.
 */


bool antichaindata_test(antichaindata* AD);
/*
 * TEST FUNCTION: Return TRUE if every antichain in *AD is a lattice-antichain; return FALSE otherwise.
 */


#endif /* ANTICHAIN_H_ */
