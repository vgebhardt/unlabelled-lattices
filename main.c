/*
 * main.c
 *
 * Created on: 22 Mar 2014
 * Last modified: 23 Feb 2019
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2019 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>


#if defined TARGET_COUNT
#if defined THREADED
/* vvv multithreaded lattice count vvv */
#include <pthread.h>

#ifdef NUMA
#include <numa.h>
#endif

#include "build.h"
#include "globals.h"
#include "lattice.h"
#include "lattEnum.h"
#include "threaded.h"
#if 1
#include "antichain.h"
#endif

extern char*  statefilename;
extern FILE*  continuing ;
extern char*  nextskip;
extern int    action;


int main(int argc, char** argv) {
	elt                 N, DELTAN, DDELTAN;
	int                 nThreads, i;
	globals             GD;
	unsigned long long  count;

#if 0
	printf("sizeof(antichaindata) = %lu\n", sizeof(antichaindata));
#endif

#ifdef NUMA
	if (numa_available() < 0) {
		fprintf(stderr, "Executable uses NUMA, but the API is not available\n");
		erri(-2);
	}
	numa_set_localalloc();
	numa_run_on_node(0);
#endif
	if (argc < 5 || argc > 7 || (argc == 7 && strcmp(argv[5],"continue"))
			|| (N=atoi(argv[1])) <= 1+(DELTAN=atoi(argv[2]))
			|| (DDELTAN=atoi(argv[3])) < 1
			|| (nThreads=atoi(argv[4])) < 1) {
		fprintf(stderr, "Usage: %s <N> <DELTAN> <DDELTAN> <nThreads> [continue <statefile>], where N > 1+DELTAN and nThreads > 0\n", argv[0]);
		erri(-1);
	}
	printf("call:");
	for (i=0; i<argc; i++)
		printf(" %s", argv[i]);
	printf("\n");

	bitmap_init(N);
	benes_init_small();
	globals_init(&GD);
	tasks_init();

	if (argc == 7 && !strcmp(argv[5],"continue")) {
		char*   cmdtemplate = "sort --field-separator=':' -k 1n -k 3n -k 4n -k 5n -k 6n -k 7n -k 8n -k 9n -k 10n -k 10n -k 12n";
		char*   cmd;
		size_t  nread = 0;
		if (!((continuing = fopen(argv[6],"r")))) {
			fprintf(stderr, "Cannot open statefile %s.\n", argv[6]);
			erri(-1);
		}
		fclose(continuing);
		continuing = 0;
		statefilename = malloc(strlen(argv[6])+1);
		strcpy(statefilename, argv[6]);
		cmd = malloc(strlen(cmdtemplate) + strlen(" \"\" > \"\"\n")+2*strlen(argv[6])+strlen(".sorted")+1);
		sprintf(cmd, "%s \"%s\" > \"%s.sorted\"\n", cmdtemplate, argv[6], argv[6]);
		if (system(cmd)) {
			fprintf(stderr, "Cannot create sorted copy %s.sorted of statefile %s.\n", argv[6], argv[6]);
			erri(-1);
		}
		sprintf(cmd,"%s.sorted", argv[6]);
		if (!((continuing = fopen(cmd,"r")))) {
			fprintf(stderr, "Cannot open sorted copy %s of statefile.\n", cmd);
			erri(-1);
		}
		if (getline(&nextskip, &nread, continuing) <= 0) {
			free(nextskip);
			nextskip = 0;
			fclose(continuing);
			continuing = 0;
		}
		free(cmd);
	} else {
		statefilename = malloc(strlen("UnlabelledLattices.XXXXX")+1);
		sprintf(statefilename,"UnlabelledLattices.%u",getpid()&0xffff);
		if (!((continuing = fopen(statefilename,"w")))) {
			fprintf(stderr, "Cannot create statefile %s.\n", statefilename);
			erri(-1);
		}
		fclose(continuing);
		continuing = 0;
	}

#if 0
	count = 0;
	taskscount = 0;
	do {
		old_taskscount = taskscount;
		taskscount = tasks_count();
		action = DELTAN > DDELTAN ? 1 : -1;
		count += threaded_master(tasks_all()+old_taskscount, tasks_count()-old_taskscount, N, N-DELTAN, nThreads, &GD);
		DELTAN -= DDELTAN;
	} while (DELTAN > 0);
#else
	action = 1;
	count = threaded_master(tasks_all(), 1, N, N-DELTAN, nThreads, &GD);
	if (tasks_count() > 1) {
		action = -1;
		count += threaded_master(tasks_all()+1, tasks_count()-1, N, N-DELTAN+DDELTAN, nThreads, &GD);
	}
#endif
	printf("%llu\n", count);

	tasks_free();
	free(statefilename);
	globals_free(&GD);
	benes_free_small();
	return 0;
}
/* ^^^ multithreaded lattice count ^^^ */
#else
/* vvv serial lattice count vvv */
#include "build.h"
#include "globals.h"
#include "lattice.h"
#include "lattEnum.h"

int main(int argc, char** argv) {
	elt        N;
	lattEnum*  E;
	lattice    L;
	globals    GD;

	if (argc != 2 || (N=atoi(argv[1])) <= 1) {
		fprintf(stderr, "usage: %s <N>, where N > 1\n", argv[0]);
		erri(-1);
	}
	bitmap_init(N);
	benes_init_small();
	globals_init(&GD);
	lattice_init_2(&L);
	E = lattEnum_Count_create(&L, N, 3, &GD);
	lattEnum_doEnumeration(E);
	printf("%llu\n", lattEnum_getLatticeCount(E));
	lattEnum_Count_free(E);
	lattice_clearStabiliser(&L);
	globals_free(&GD);
	benes_free_small();
#ifdef COLLECTSTATISTICS
	printGarsideStatistics();
#endif
	return 0;
}
/* ^^^ serial lattice count ^^^ */
#endif
#elif defined TARGET_CATALOGUE
#if defined THREADED
/* vvv multithreaded lattice catalogue vvv */
#include <pthread.h>

#ifdef NUMA
#include <numa.h>
#endif

#include "build.h"
#include "globals.h"
#include "lattice.h"
#include "lattEnum.h"
#include "threaded.h"
#if 1
#include "antichain.h"
#endif

extern char*  statefilename;
extern FILE*  continuing ;
extern char*  nextskip;
extern int    action;


int main(int argc, char** argv) {
	elt                 N, DELTAN, DDELTAN;
	int                 nThreads, i;
	globals             GD;
	unsigned long long  count;

#if 0
	printf("sizeof(antichaindata) = %lu\n", sizeof(antichaindata));
#endif

#ifdef NUMA
	if (numa_available() < 0) {
		fprintf(stderr, "Executable uses NUMA, but the API is not available\n");
		erri(-2);
	}
	numa_set_localalloc();
	numa_run_on_node(0);
#endif
	if (argc < 5 || argc > 7 || (argc == 7 && strcmp(argv[5],"continue"))
			|| (N=atoi(argv[1])) <= 1+(DELTAN=atoi(argv[2]))
			|| (DDELTAN=atoi(argv[3])) < 1
			|| (nThreads=atoi(argv[4])) < 1) {
		fprintf(stderr, "Usage: %s <N> <DELTAN> <DDELTAN> <nThreads> [continue <statefile>], where N > 1+DELTAN and nThreads > 0\n", argv[0]);
		erri(-1);
	}
	fprintf(stderr, "call:");
	for (i=0; i<argc; i++)
		fprintf(stderr, " %s", argv[i]);
	fprintf(stderr, "\n");
	fprintf(stderr, "WARNING: Output may contain some lattices more than once; use 'sort -u' to remove duplicates.\n");

	bitmap_init(N);
	benes_init_small();
	globals_init(&GD);
	tasks_init();

	if (argc == 7 && !strcmp(argv[5],"continue")) {
		char*   cmdtemplate = "sort --field-separator=':' -k 1n -k 3n -k 4n -k 5n -k 6n -k 7n -k 8n -k 9n -k 10n -k 10n -k 12n";
		char*   cmd;
		size_t  nread = 0;
		if (!((continuing = fopen(argv[6],"r")))) {
			fprintf(stderr, "Cannot open statefile %s.\n", argv[6]);
			erri(-1);
		}
		fclose(continuing);
		continuing = 0;
		statefilename = malloc(strlen(argv[6])+1);
		strcpy(statefilename, argv[6]);
		cmd = malloc(strlen(cmdtemplate) + strlen(" \"\" > \"\"\n")+2*strlen(argv[6])+strlen(".sorted")+1);
		sprintf(cmd, "%s \"%s\" > \"%s.sorted\"\n", cmdtemplate, argv[6], argv[6]);
		if (system(cmd)) {
			fprintf(stderr, "Cannot create sorted copy %s.sorted of statefile %s.\n", argv[6], argv[6]);
			erri(-1);
		}
		sprintf(cmd,"%s.sorted", argv[6]);
		if (!((continuing = fopen(cmd,"r")))) {
			fprintf(stderr, "Cannot open sorted copy %s of statefile.\n", cmd);
			erri(-1);
		}
		if (getline(&nextskip, &nread, continuing) <= 0) {
			free(nextskip);
			nextskip = 0;
			fclose(continuing);
			continuing = 0;
		}
		free(cmd);
	} else {
		statefilename = malloc(strlen("UnlabelledLattices.XXXXX")+1);
		sprintf(statefilename,"UnlabelledLattices.%u",getpid()&0xffff);
		if (!((continuing = fopen(statefilename,"w")))) {
			fprintf(stderr, "Cannot create statefile %s.\n", statefilename);
			erri(-1);
		}
		fclose(continuing);
		continuing = 0;
	}

#if 0
	count = 0;
	taskscount = 0;
	do {
		old_taskscount = taskscount;
		taskscount = tasks_count();
		action = DELTAN > DDELTAN ? 1 : -1;
		count += threaded_master(tasks_all()+old_taskscount, tasks_count()-old_taskscount, N, N-DELTAN, nThreads, &GD);
		DELTAN -= DDELTAN;
	} while (DELTAN > 0);
#else
	action = 1;
	count = threaded_master(tasks_all(), 1, N, N-DELTAN, nThreads, &GD);
	if (tasks_count() > 1) {
		action = -1;
		count += threaded_master(tasks_all()+1, tasks_count()-1, N, N-DELTAN+DDELTAN, nThreads, &GD);
	}
#endif
	fprintf(stderr, "%llu\n", count);

	tasks_free();
	free(statefilename);
	globals_free(&GD);
	benes_free_small();
	return 0;
}
/* ^^^ multithreaded lattice catalogue ^^^ */
#else
/* vvv serial lattice catalogue vvv */
#include "build.h"
#include "lattice.h"
#include "lattEnum.h"

int main(int argc, char** argv) {
	elt        N;
	lattEnum*  E;
	lattice    L;
	globals    GD;

	if (argc < 2 || (N=atoi(argv[1])) <= 1) {
		fprintf(stderr, "usage: %s <N>, where N > 1\n", argv[0]);
		erri(-1);
	}
	bitmap_init(N);
	benes_init_small();
	globals_init(&GD);
	lattice_init_2(&L);
	E = lattEnum_stdout_create(&L, N, 3, &GD);
	lattEnum_doEnumeration(E);
	lattEnum_stdout_free(E);
	lattice_clearStabiliser(&L);
	globals_free(&GD);
	benes_free_small();
	return 0;
}
/* ^^^ serial lattice catalogue ^^^ */
#endif
#elif defined TARGET_UTEST_SI
/* vvv unit test implicit stabiliser vvv */
#include "build.h"
#include "permutation.h"
#include "canonical.h"

int main(int argc, char** argv) {
	globals      GD;
	elt          n, a0, m, k;
	flags64      L, M;
	flags32      bl, SI;
	permutation  p;

#if 1
	n = 10; k = 7; a0 = 4; m = 6;
	L = 0x108421083eUL;
	M = 0x1041041041UL;
	SI = 0x3e0;
	bl = 0x7e;
#else
	n = 8; k = 5; a0 = 4; m = 4;
	L = 0x1248eUL;
	M = 0x11111UL;
	SI = 0xe0;
	bl = 0x1e;
#endif
	benes_init_small();
	globals_init(&GD);
	perm_init(n+k, p);
	antichainList_applySI_p1_f(n, k, a0, m, bl, &GD, &L, SI, M, p);
	globals_free(&GD);
	benes_free_small();
	return 0;
}
/* ^^^ unit test implicit stabiliser ^^^ */
#elif defined TARGET_UTEST_LATTENUM
/* vvv unit test lattenum vvv */
#include "build.h"
#include "permutation.h"
#include "permgrp.h"
#include "lattice.h"
#include "lattEnum.h"
#include "canonical.h"

int main(int argc, char** argv) {
	elt          N;
	lattEnum*    E;
	lattice      L;
	globals      GD;

	N = 7;
	bitmap_init(N);
	benes_init_small();
	globals_init(&GD);
	lattice_init_2(&L);
	E = lattEnum_Count_create(&L, N, 3, &GD);
	lattEnum_doEnumeration(E);
	printf("%llu\n", lattEnum_getLatticeCount(E));
	lattEnum_Count_free(E);
	lattice_clearStabiliser(&L);
	globals_free(&GD);
	benes_free_small();
	return 0;
}
/* ^^^ unit test lattenum ^^^ */
#elif defined TARGET_UTEST_LATTICE
/* vvv unit test lattice vvv */
#include "build.h"
#include "permutation.h"
#include "permgrp.h"
#include "lattice.h"
#include "lattEnum.h"
#include "canonical.h"

int main(int argc, char** argv) {
	elt          n;
	lattice      L;
	permgrpc*    S;
	char         buf[MAXN*(MAXN-1)/2+1];

	/* starting easy... */
	lattice_init_2(&L);
	if (!lattice_test(&L)) {
	    fprintf(stderr, "Invalid lattice!\n");
	    erri(-4);
	}
	lattice_print(&L);
	lattice_toString(buf, &L);
	printf("%s\n", buf);
	lattice_clearStabiliser(&L);

	/* testing k-fan */
	for (n=1; n<6; n++) {
		printf("\n");
		lattice_init_kFan(&L, n);
		lattice_print(&L);
		lattice_toString(buf, &L);
		printf("%s\n", buf);
		lattice_clearStabiliser(&L);
	}

	/* testing from string */
	while (1) {
		printf("\nNumber of elements: ");
		scanf("%hhd", &n);
		if (!n)
			break;
		printf("Enter string (length %d): ", n*(n-1)/2);
		scanf("%s", buf);
		printf("echo: %s\n", buf);
		S = permgrpc_alloc();
		permgrpc_init(S, n-2);
		lattice_fromString(&L, n, buf, permgrpc_get_permgrp(S), 0);
		if (!lattice_test(&L)) {
			fprintf(stderr, "Invalid lattice!\n");
			erri(-4);
		}
		lattice_print(&L);
		lattice_toString(buf, &L);
		printf("%s\n", buf);
		lattice_clearStabiliser(&L);
		permgrpc_delete(S);
	}

	return 0;
}
/* ^^^ unit test lattice ^^^ */
#elif defined TARGET_UTEST_PERMGRP
/* vvv unit test permgrp vvv */
#include "build.h"
#include "permutation.h"
#include "permgrp.h"

int main(int argc, char** argv) {
	int        n, ngens, i;
	permgrpc*  G;

	printf("Number of points: ");
	scanf("%d", &n);
	bitmap_init(n);
	G = permgrpc_alloc();
	permgrpc_init(G,n);
	printf("Number of generators: ");
	scanf("%d", &ngens);
	for (i=1; i<=ngens; i++) {
		int          j, buf;
		permutation  p;

		perm_init(n, p);
		printf("Enter generator %d:\n", i);
		for (j=0; j<n; j++) {
			printf("   image of %d: ", j+1);
			scanf("%d", &buf);
			p[j] = buf-1;
		}
		printf("\n");
		perm_print(n, p, 1);
		permgrpc_addGenerator(G, p);
	}
	printf("==============\n");
	permgrpc_printGenerators(G, 1);
	permgrpc_delete(G);
	return 0;
}
/* ^^^ unit test permgrp ^^^ */
#elif defined TARGET_UTEST_PERMUTATION
/* vvv unit test permutation vvv */
#include "build.h"
#include "permutation.h"

int main(int argc, char** argv) {
	elt                 i, n;
	elt                 lev[MAXN-2];
	permutation         p;
	unsigned long long  count;

	printf("number of levels: %d\n", argc-1);
	for (i=1,n=0; i<argc; i++) {
		n += atoi(argv[i]);
		lev[i-1] = n;
		printf("  level %d: %d\n", i, n);
	}
	if (n < 1) {
		fprintf(stderr, "The total number of elements must be at least 1.\n");
		erri(-1);
	}

	perm_init(n, p);
	count = 1;  /* identity */
	while (perm_next(argc-1, lev, p))
		count++;

	printf(">>> %llu\n", count);

	return 0;
}
/* ^^^ unit test permutation ^^^ */
#elif defined TARGET_GARSIDE
/* vvv test/time possibleGarside vvv */
#include "garside.h"

void wrapper(lattice* LL) {
	possiblyGarside(LL);
}

int main(int argc, char** argv) {
	elt       N;
	FILE*     infile;
	char*     inbuf = 0;
	size_t    nread = 0;
	long      nL = 0;
	permgrp*  S;

	if (argc !=3 || (N=atoi(argv[1])) < 2) {
		fprintf(stderr, "Usage: %s <N> <infile>", argv[0]);
		erri(-1);
	}
	if (!((infile = fopen(argv[2],"r")))) {
		fprintf(stderr, "Cannot open catalogue file %s.", argv[2]);
		erri(-1);
	}
	S = permgrp_alloc();
	permgrp_init(S, 0);

	while (getline(&inbuf, &nread, infile) > 0) {
		lattice LL;
		inbuf[strlen(inbuf)-1] = 0;
		if (!lattice_fromString(&LL, N, inbuf, S, 0)) {
			fprintf(stderr, "Cannot convert string %s to lattice of size %d.", inbuf, N);
			erri(-1);
		}
		wrapper(&LL);
		nL++;
	}
    permgrp_delete(S);
	free(inbuf);
	inbuf = 0;
	fclose(infile);
	infile = 0;

	printf("%ld lattices read\n\n", nL);
#ifdef COLLECTSTATISTICS
	printGarsideStatistics();
#endif

	return 0;
}
/* ^^^ test/time possibleGarside ^^^ */
#else
#error "No build target specified!"
#endif
