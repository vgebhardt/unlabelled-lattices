/*
 * antichain.c
 *
 * Created on: 22 Mar 2014
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include "antichain.h"
#include "canonical.h"


#ifdef THREADED
extern int  action;
#endif


void antichaindata_init(lattice* L, elt k, antichaindata* AD, globals* GD)
/*
 * Initialise antichain data *AD for the lattice L with k elements to be added on the new level.
 * AD should be allocated.
 */
{
	elt      i;
	flags32  biti;

	PREFETCH(AD->L->lo);
	PREFETCH(AD->cop);

	AD->L = L;
	AD->GD = GD;
	AD->L->lev[AD->L->nLev] = AD->L->n + k;  /* levels for the new lattice; for Beneš network creation */
	AD->k = k;
	AD->cp = 0;
	AD->cl = L->nLev-2;
	AD->Fpos = 0;
	memset(AD->cop, 0, AD->L->n*sizeof(flags32));
	for (i=0,biti=1; i<AD->L->n-1; i++,biti<<=1) {
		elt      j;
		for (j=i+1; j<AD->L->n; j++) {
			if(!(AD->L->lo[i] & AD->L->lo[j])) {
				AD->cop[i] |= BIT(j);
				AD->cop[j] |= biti;
			}
		}
	}
	for (i=0; i<=AD_CSL; i++) {
		AD->SD[i].ST_ = 0;
		AD->SD[i].ST = 0;
	}
	AD->SD[AD_CSL+1].ST = AD->L->S;
	AD->SD[AD_CSL+1].SI = AD->L->SI;
	AD->SD[AD_CSL+1].bl = (1 << k) - 2;
	memset(AD->O, 0, k*sizeof(flags32));
	AD->O[0] = AD->cmc = BIT(L->n) - BIT(L->lev[AD->cl]);
	AD->cm = AD->cmc;
#ifdef VERBOSE
	AD->F[0] = 0;
#endif
}


void antichaindata_reinit(antichaindata* AD, elt k)
/*
 * Modify antichain data *AD for k elements to be added on the new level.
 * AD should be initialised.
 */
{
	AD->L->lev[AD->L->nLev] = AD->L->n + k;  /* levels for the new lattice; for Beneš network creation */
	AD->k = k;
	AD->cp = 0;
	AD->cl = AD->L->nLev-2;
	AD->Fpos = 0;
	AD->SD[AD_CSL+1].bl = (1 << k) - 2;
	memset(AD->O, 0, k*sizeof(flags32));
	AD->O[0] = AD->cmc = BIT(AD->L->n) - BIT(AD->L->lev[AD->cl]);
	AD->cm = AD->cmc;
}


#ifndef FILTER_GRADED
static inline void antichaindata_decrementLevel(antichaindata *AD)
/*
 * Decrement the current level of *AD.
 */
{
	antichaindata_updateBlocks(AD, AD->SD[AD_CSL+1].bl, &(AD->SD[AD_CSL].bl));
	(AD->cl)--;
	AD->Fpos++;
	AD->cmc = (BIT(AD->L->lev[AD->cl+1]) - BIT(AD->L->lev[AD->cl]));
	AD->cm = (BIT(AD->L->n) - BIT(AD->L->lev[AD->cl]));
	AD->cp = 0;
}


static inline void antichaindata_decrementLevel_1(antichaindata *AD)
/*
 * Decrement the current level of *AD.  Special case of a single antichain.
 */
{
	(AD->cl)--;
	AD->Fpos++;
	AD->cmc = (BIT(AD->L->lev[AD->cl+1]) - BIT(AD->L->lev[AD->cl]));
	AD->cm = (BIT(AD->L->n) - BIT(AD->L->lev[AD->cl]));
}
#endif


static inline void antichaindata_initialiseCurrentPosition(antichaindata* AD)
/*
 * Initialise antichaindata counter for the current position.  The lattice-antichain condition
 * is NOT verified.
 */
{
	AD->O[AD->cp] |= AD->cmc;
	if (AD->Fpos-AD->k >= 0)
		AD->O[AD->cp] &= ~(AD->F[AD->Fpos-AD->k] & AD->cmc);
#ifdef VERBOSE
	AD->F[AD->Fpos] = 0;
#endif
}


#ifndef FILTER_GRADED
static inline void antichaindata_initialiseCurrentPosition_1(antichaindata* AD)
/*
 * Initialise antichaindata counter for the current position.  The lattice-antichain condition
 * is NOT verified.  Special case of a single antichain.
 */
{
	AD->O[0] |= AD->cmc;
	if (AD->Fpos)
		AD->O[0] &= ~(AD->F[AD->Fpos-1] & AD->cmc);
#ifdef VERBOSE
	AD->F[AD->Fpos] = 0;
#endif
}
#endif


static inline bool antichaindata_validateCurrentPosition(antichaindata* AD)
/*
 * Test whether the lattice-antichain condition holds for the currently considered antichain (AD->cp)
 * up to the current level (AD->cl); if successful, and if AD->cl > 0, initialise optional elements
 * on the next lower level (AD->cl-1).
 *
 * Return value: 1 if lattice-antichain condition holds; 0 if contradiction to choice on lower levels
 */
{
	elt      i, j, m, F1pos;
	flags32  done, todo, F;

#ifdef DOTEST
	if (AD->Fpos != (AD->L->nLev-2-AD->cl)*AD->k+AD->cp) {
		printf ("antichaindata: inconsistent value of Fpos\n");
		erri(-4);
	}
#endif

	/* start with inherited constraints from lower levels */
	F1pos = AD->Fpos - AD->k;
	F = done = (F1pos >= 0 ? AD->F[F1pos] : 0);

	/* up-close optional elements added on current level */
	for (i=AD->L->lev[AD->cl]; i<AD->L->lev[AD->cl+1]; i++)
		if (AD->O[AD->cp] & BIT(i))
			F |= AD->L->up[i];

	/* meet-close new elements with everything; record constraints & test compatibility with earlier lattice-antichains */
#ifdef VERBOSE
	printf("...... in antichaindata_validateCurrentPosition: meet close  ");
	antichaindata_printCounters(AD);
#endif
	todo = F & ~done;
	while (extract_MSB32(&todo, &i)) {
		flags32  tocheck, biti, loi, C;
		bool     allnonzero;
		biti = BIT(i);
		/* first test compatibility with other lattice antichains... */
		C = AD->cop[i] & F;
		for (j=(F1pos>0?F1pos:0); j<AD->Fpos; j++)
			if ((biti & AD->F[j]) && (C & AD->F[j]))
				return FALSE;
		/* ...now meet-close & test lattice antichain condition */
		tocheck = done & ~(AD->L->up[i] | AD->L->lo[i]);
		allnonzero = TRUE;
		loi = AD->L->lo[i];
		done |= biti;
		while (extract_MSB32(&tocheck, &j)) {
			if (get_LSB32(loi&(AD->L->lo[j]), &m)) {
				if (!(BIT(m) & (done | todo))) {  /* m cannot equal i by choice of j, so removing i early is fine */
#ifndef FILTER_GRADED
					if (m >= AD->L->lev[AD->cl]) {  /* equivalent to AD->L->dep[m] >= AD->cl */
						return FALSE;
					} else {
						todo |= (AD->L->up[m] & ~done);
						F |= AD->L->up[m];
					}
#else
					return FALSE;  /* either contradicts choice, or adds a cover on a higher level */
#endif
				}
				tocheck &= ~AD->L->up[j];
			} else {
				allnonzero = FALSE;
			}
		}
		if (unlikely(allnonzero)) {
			done |= (todo & AD->L->up[i]);  /* For any j we have i\meet j \ne 0 for any j, so the gcd */
			todo &= ~AD->L->up[i];          /* of j with any element in up[i] is in up[i\meet j].     */
		}
	}
#ifdef VERBOSE
	printf("...... in antichaindata_validateCurrentPosition: OK\n");
#endif
	AD->F[AD->Fpos] = F;
	return TRUE;
}


static inline bool antichaindata_validateCurrentPosition_1(antichaindata* AD)
/*
 * Test whether the lattice-antichain condition holds for the currently considered antichain (AD->cp)
 * up to the current level (AD->cl); if successful, and if AD->cl > 0, initialise optional elements
 * on the next lower level (AD->cl-1).  Special case of a single antichain.
 *
 * Return value: 1 if lattice-antichain condition holds; 0 if contradiction to choice on lower levels
 */
{
	elt      i, j, m;
	flags32  done, todo, F;

#ifdef DOTEST
	if (AD->Fpos != (AD->L->nLev-2-AD->cl)*AD->k+AD->cp) {
		printf ("antichaindata: inconsistent value of Fpos\n");
		erri(-4);
	}
#endif

	/* start with inherited constraints from lower levels */
	F = done = (AD->Fpos ? AD->F[AD->Fpos-1] : 0);

	/* up-close optional elements added on current level */
	for (i=AD->L->lev[AD->cl]; i<AD->L->lev[AD->cl+1]; i++)
		if (AD->O[0] & BIT(i))
			F |= AD->L->up[i];

	/* meet-close new elements with everything; record constraints */
#ifdef VERBOSE
	printf("...... in antichaindata_validateCurrentPosition_1: meet close  ");
	antichaindata_printCounters(AD);
#endif
	todo = F & ~done;
	while (extract_MSB32(&todo, &i)) {
		flags32  tocheck, loi;
		bool     allnonzero;
		/* meet-close & test lattice antichain condition */
		tocheck = done & ~(AD->L->up[i] | AD->L->lo[i]);
		done |= BIT(i);
		allnonzero = TRUE;
		loi = AD->L->lo[i];
		while (extract_MSB32(&tocheck, &j)) {
			if (get_LSB32(loi&(AD->L->lo[j]), &m)) {
				if (!(BIT(m) & (done | todo))) {  /* m cannot equal i by choice of j, so removing i early is fine */
#ifndef FILTER_GRADED
					if (m >= AD->L->lev[AD->cl]) {  /* equivalent to AD->L->dep[m] >= AD->cl */
						return FALSE;
					}
					else {
						todo |= (AD->L->up[m] & ~done);
						F |= AD->L->up[m];
					}
#else
					return FALSE;  /* either contradicts choice, or adds a cover on a higher level */
#endif
				}
				tocheck &= ~AD->L->up[j];  /* gcd of i with any element in up[j] is in up[m] */
			} else {
				allnonzero = FALSE;
			}
		}
		if (unlikely(allnonzero)) {
			done |= (todo & AD->L->up[i]);  /* For any j we have i\meet j \ne 0 for any j, so the gcd */
			todo &= ~AD->L->up[i];          /* of j with any element in up[i] is in up[i\meet j].     */
		}
	}
#ifdef VERBOSE
	printf("...... in antichaindata_validateCurrentPosition_1: OK\n");
#endif

	AD->F[AD->Fpos] = F;
	return TRUE;
}


bool antichaindata_step_FUNC(antichaindata* AD)
/*
 * If possible, step antichaindata counters starting from the current on the current level.
 * The lattice-antichain conditions are NOT verified.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */
{
	return antichaindata_step(AD);
}


bool antichaindata_step_1_FUNC(antichaindata* AD)
/*
 * If possible, step antichaindata counters starting from the current on the current level.
 * The lattice-antichain conditions are NOT verified.
 *
 * Return value: TRUE if success; FALSE if there are no further useful configurations.
 */
{
	return antichaindata_step_1(AD);
}


#ifdef FILTER_GRADED
bool antichaindata_next(antichaindata* AD)
/*
 * Advance *AD to the next canonical configuration of lattice-antichains.
 *
 * Return value: TRUE if successful; FALSE if no further canonical configuration exists.
 */
{
#ifdef THREADED
	while (action) {
#else
	while (TRUE) {
#endif
#ifdef VERBOSE
		printf("... in antichaindata_next: "); antichaindata_printCounters(AD);
#endif
		while (!antichaindata_validateCurrentPosition(AD)) {
#ifdef VERBOSE
			printf("                           invalid\n");
#endif
			if (!antichaindata_step(AD)) {
#ifdef VERBOSE
				printf("                                   and done!\n");
#endif
				return FALSE;
			}
		}
		if (AD->cp != AD->k-1) {
			AD->cp++;
			AD->Fpos++;
			antichaindata_initialiseCurrentPosition(AD);
		} else {
			flags32  m;
			elt      i;

			for (m=0, i=0; i<AD->k; i++)
				m |= AD->O[i];
			if (m != BIT(AD->L->lev[AD->cl+1])-BIT(AD->L->lev[AD->cl])) {
#ifdef VERBOSE
				printf("                           not graded\n");
#endif
				if (!antichaindata_step(AD)) {
#ifdef VERBOSE
					printf("                                         and done!\n");
#endif
					return FALSE;
				}
			} else {
				if (!antichaindata_isCanonical(AD)) {
#ifdef VERBOSE
					printf("                           not canonical\n");
#endif
					/* antichaindata_isCanonical has set AD->cp and AD->Fpos to the correct backtracking position  */
					if (!antichaindata_step(AD)) {
#ifdef VERBOSE
						printf("                                         and done!\n");
#endif
						return FALSE;
					}
				} else {
#ifdef VERBOSE
					printf("                           valid --> "); antichaindata_printCountersF(AD);
#endif
					return TRUE;
				}
			}
		}
	}
	return FALSE;
}


bool antichaindata_next_1(antichaindata* AD)
/*
 * Advance *AD to the next canonical configuration of lattice-antichains.  Special case of a single antichain.
 *
 * Return value: TRUE if successful; FALSE if no further canonical configuration exists.
 */
{
#ifdef THREADED
	while (action) {
#else
	while (TRUE) {
#endif
#ifdef VERBOSE
		printf("... in antichaindata_next_1: "); antichaindata_printCounters(AD);
#endif
		while (!antichaindata_validateCurrentPosition_1(AD)) {
#ifdef VERBOSE
			printf("                           invalid\n");
#endif
			if (!antichaindata_step_1(AD)) {
#ifdef VERBOSE
				printf("                                   and done!\n");
#endif
				return FALSE;
			}
		}
		if (AD->O[0] != BIT(AD->L->lev[AD->cl+1])-BIT(AD->L->lev[AD->cl])) {
#ifdef VERBOSE
			printf("                           not graded\n");
#endif
			if (!antichaindata_step_1(AD)) {
#ifdef VERBOSE
				printf("                                         and done!\n");
#endif
				return FALSE;
			}
		} else {
			if (!antichaindata_isCanonical_1(AD)) {
#ifdef VERBOSE
				printf("                           not canonical\n");
#endif
				if (!antichaindata_step_1(AD)) {
#ifdef VERBOSE
					printf("                                         and done!\n");
#endif
					return FALSE;
				}
			} else {
#ifdef VERBOSE
				printf("                           valid --> "); antichaindata_printCountersF(AD);
#endif
				return TRUE;
			}
		}
	}
	return FALSE;
}
#else
bool antichaindata_next(antichaindata* AD)
/*
 * Advance *AD to the next canonical configuration of lattice-antichains.
 *
 * Return value: TRUE if successful; FALSE if no further canonical configuration exists.
 */
{
#ifdef THREADED
	while (action) {
#else
	while (TRUE) {
#endif
#ifdef VERBOSE
		printf("... in antichaindata_next: "); antichaindata_printCounters(AD);
#endif
		while (!antichaindata_validateCurrentPosition(AD)) {
#ifdef VERBOSE
			printf("                           invalid\n");
#endif
			if (!antichaindata_step(AD)) {
#ifdef VERBOSE
				printf("                                   and done!\n");
#endif
				return FALSE;
			}
		}
		if (AD->cp != AD->k-1) {
			AD->cp++;
			AD->Fpos++;
			antichaindata_initialiseCurrentPosition(AD);
		} else {
			if (!antichaindata_isCanonical(AD)) {
#ifdef VERBOSE
				printf("                           not canonical\n");
#endif
				/* antichaindata_isCanonical has set AD->cp and AD->Fpos to the correct backtracking position  */
				if (!antichaindata_step(AD)) {
#ifdef VERBOSE
					printf("                                         and done!\n");
#endif
					return FALSE;
				}
			} else {
				if (AD->cl) {
					antichaindata_decrementLevel(AD);
					antichaindata_initialiseCurrentPosition(AD);
				} else {
#ifdef VERBOSE
					printf("                           valid --> "); antichaindata_printCountersF(AD);
#endif
					return TRUE;
				}
			}
		}
	}
	return FALSE;
}


bool antichaindata_next_1(antichaindata* AD)
/*
 * Advance *AD to the next canonical configuration of lattice-antichains.  Special case of a single antichain.
 *
 * Return value: TRUE if successful; FALSE if no further canonical configuration exists.
 */
{
#ifdef THREADED
	while (action) {
#else
	while (TRUE) {
#endif
#ifdef VERBOSE
		printf("... in antichaindata_next_1: "); antichaindata_printCounters(AD);
#endif
		while (!antichaindata_validateCurrentPosition_1(AD)) {
#ifdef VERBOSE
			printf("                           invalid\n");
#endif
			if (!antichaindata_step_1(AD)) {
#ifdef VERBOSE
				printf("                                   and done!\n");
#endif
				return FALSE;
			}
		}
		if (!antichaindata_isCanonical_1(AD)) {
#ifdef VERBOSE
			printf("                           not canonical\n");
#endif
			if (!antichaindata_step_1(AD)) {
#ifdef VERBOSE
				printf("                                         and done!\n");
#endif
				return FALSE;
			}
		} else {
			if (AD->cl) {
				antichaindata_decrementLevel_1(AD);
				antichaindata_initialiseCurrentPosition_1(AD);
			} else {
#ifdef VERBOSE
				printf("                           valid --> "); antichaindata_printCountersF(AD);
#endif
				return TRUE;
			}
		}
	}
	return FALSE;
}
#endif


void antichaindata_prepareLattice(antichaindata* AD, lattice* L, lattice* LA)
/*
 * Set generic data in *LA that applies to every lattice obtained from *L by adding AD->k atoms
 * covered by lattice antichains:  number of elements, levels, as well as
 *    up[i]  .....
 *   (lo[i]) lo[j]
 * for 1 < i < L->n <= j < LA->n.  lo[i] may be modified in antichaindata_generateLattice.
 */
{
	elt  j;

	LA->n = L->n + AD->k;                          /* number of elements  */
	LA->nLev = L->nLev+1;                          /* number of levels    */
	memcpy(LA->lev, L->lev, L->nLev*sizeof(elt));  /* old levels          */
	LA->lev[LA->nLev-1] = LA->n;                   /* new level           */
	for (j=L->n; j<LA->n; j++) {
		LA->lo[j] = BIT(j);                        /* lo[j]               */
	}
	memcpy(LA->up, L->up, L->n*sizeof(flags32));   /* up[i]               */
	memcpy(LA->lo, L->lo, L->n*sizeof(flags32));   /* lo[i] (preliminary) */
}


void antichaindata_generateLattice(antichaindata* AD, lattice* L, lattice* LA)
/*
 * Set *LA to the lattice obtained from *L by adding AD->k new atoms covered by the elements in the
 * lattice antichains described by *AD:  stabiliser, as well as
 *   .....  up[j]
 *   lo[i]  .....
 * for 1 < i < L->n <= j < LA->n.  ..... denotes data set in antichaindata_prepareLattice.
 * Note: The stabiliser of *LA carries a reference count.
 */
{
	elt      i, j, k;

	PREFETCH(AD->F);
	PREFETCH(LA->up);
	PREFETCH(LA->lo);

	for (k=AD->k,j=L->n; k--; j++) {
		/* set up[j] and lo[i] for 1 < i < L->n <= j < LA->n */
		LA->up[j] = BIT(j) | AD->F[AD->Fpos-k];       /* up[j] */
		for (i=0; i<L->n; i++) {
			if (AD->F[AD->Fpos-k] & BIT(i))
				LA->lo[i] |= BIT(j);
			else                                      /* lo[i] */
				LA->lo[i] &= ~BIT(j);
		}
	}
	/* set stabiliser */
	lattice_setStabiliser(LA,AD->SD[0].ST,AD->SD[0].SI);  /* stabiliser          */
}


void antichaindata_generateLattice_1(antichaindata* AD, lattice* L, lattice* LA)
/*
 * Set *LA to the lattice obtained from *L by adding AD->k new atoms covered by the elements in the
 * lattice antichains described by *AD:  stabiliser, as well as
 *   .....  up[j]
 *   lo[i]  .....
 * for 1 < i < L->n <= j < LA->n.  ..... denotes data set in antichaindata_prepareLattice.
 * Special case of a single antichain.
 * Note: The stabiliser of *LA carries a reference count.
 */
{
	elt      i;

	PREFETCH(AD->F);
	PREFETCH(LA->up);
	PREFETCH(LA->lo);

	/* set up[j] and lo[i] for 1 < i < L->n <= j < LA->n */
	LA->up[L->n] = BIT(L->n) | AD->F[AD->Fpos];   /* up[j] */
	for (i=0; i<L->n; i++) {    /* lo[i] */
		if (AD->F[AD->Fpos] & BIT(i))
			LA->lo[i] |= BIT(L->n);
		else                                      /* lo[i] */
			LA->lo[i] &= ~BIT(L->n);
	}
	/* set stabiliser */
	lattice_setStabiliser(LA,AD->SD[0].ST,AD->SD[0].SI);  /* stabiliser          */
}


void antichaindata_printAntichains(antichaindata* AD)
/*
 * TEST FUNCTION: Print all set bits in each antichain in *AD.
 */
{
	elt      i, j;
	flags32  A;

	for (j=AD->k; j--; ) {
		/* extract the j-th lattice-antichain described by *AD, i.e. the minimal elements of the up-closed set */
		A = 0;
		for (i=0; i<AD->L->n; i++)
			if ((AD->F[AD->Fpos-j] & BIT(i)) && ((AD->F[AD->Fpos-j] & AD->L->lo[i]) == BIT(i)))
				A |= BIT(i);
		while (extract_LSB32(&A, &i))
			printf("%d ", i);
		printf(" | ");
	}
	printf("\n");
}


void antichaindata_printCounters(antichaindata* AD)
/*
 * TEST FUNCTION: Print all counters for each position and each level in *AD.
 */
{
	elt      i, j, m;
	flags32  A;

	printf("cl:%d, cp:%d   ", AD->cl, AD->cp);
	for (j=0; j<AD->k; j++) {
		for (m=AD->L->nLev-2; m>=AD->cl; m--) {
			A = (j>AD->cp ? 0 : AD->F[(AD->L->nLev-2-m)*AD->k+j]) | (AD->O[j] & (BIT(AD->L->lev[m+1]) - BIT(AD->L->lev[m])));
			while (extract_LSB32(&A, &i))
				printf("%d ", i);
			printf("; ");
		}
		printf(" | ");
	}
	printf("\n");
}


void antichaindata_printCountersF(antichaindata* AD)
/*
 * TEST FUNCTION: Print the counters F for each position and each level in *AD.
 */
{
	elt      i, j, m;
	flags32  A;

	printf("cl:%d, cp:%d   ", AD->cl, AD->cp);
	for (j=0; j<AD->k; j++) {
		for (m=AD->L->nLev-2; m>=AD->cl; m--) {
			A = j>AD->cp ? 0 : AD->F[(AD->L->nLev-2-m)*AD->k+j];
			while (extract_LSB32(&A, &i))
				printf("%d ", i);
			printf("; ");
		}
		printf(" | ");
	}
	printf("\n");
}


bool antichaindata_test(antichaindata* AD)
/*
 * TEST FUNCTION: Return TRUE if every antichain in *AD is a lattice-antichain; return FALSE otherwise.
 */
{
	elt      i, j, k, d;
	flags32  A, upA, m;

	for (k=0; k<AD->k; k++) {
		/* extract the k-th lattice-antichain described by *AD, i.e. the minimal elements of the up-closed set */
		A = 0;
		for (i=0; i<AD->L->n; i++)
			if ((AD->F[AD->Fpos-k] & BIT(i)) && ((AD->F[AD->Fpos-k] & AD->L->lo[i]) == BIT(i)))
				A |= BIT(i);
		/* test that A is an antichain */
		for (i=0; i<AD->L->n; i++)
			if (A & BIT(i))
				for (j=i+1; j<AD->L->n; j++)
					if (A & BIT(j))
						if ((BIT(i)&AD->L->up[j]) || (BIT(j)&AD->L->up[i]) || (BIT(i)&AD->L->lo[j]) || (BIT(j)&AD->L->lo[i])) {
							printf(">>> not an antichain %d:(%d,%d)\n", k, i, j);
							return FALSE;
						}
		/* up-close */
		upA = 0;
		for (i=0; i<AD->L->n; i++)
			if (A & BIT(i))
				upA |= AD->L->up[i];
		if (upA != AD->F[AD->Fpos-k]) {
			printf(">>> not up-closed %d:(%d)\n", k, i);
			return FALSE;
		}
		/* test gcd condition */
		for (i=0; i<AD->L->n; i++)
			if (upA & BIT(i))
				for (j=i+1; j<AD->L->n; j++)
					if (upA & BIT(j)) {
						m = AD->L->lo[i] & AD->L->lo[j];
						for (d=0; d<AD->L->n && !(m&BIT(d)); d++)
							;
						if (d < AD->L->n && !(upA&BIT(d))) {
							printf(">>> not a lattice-antichain %d:(%d,%d) [%d]\n", k, i, j, d);
							return FALSE;
						}

					}
	}
	return TRUE;
}
