/*
 * hashtable.h
 *
 * Created on: 1 Jun 2015
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef HASHTABLE_H_
#define HASHTABLE_H_

#include "build.h"


// TODO: Is using a hashtable still a good idea, now that most large orbits are gone?  A sorted list would be more
//       local, and binary search might be fast enough.


#ifdef PRINTHASHTABLESTATS
	static unsigned long maxlist=0;
	static unsigned long entries=0;
	static unsigned long maxentries=0;
#endif


typedef struct hashtableentry hashtableentry;
struct hashtableentry {
	flags64         key0;
	flags64         key1;
	unsigned long   data;
};


typedef struct hashtablenode hashtablenode;
struct hashtablenode {
	unsigned long    timestamp;
	unsigned long    nlist;
	unsigned long    slist;
	hashtableentry*  list;
	hashtableentry   entry;
};


typedef struct hashtable hashtable;
struct hashtable {
	unsigned long   timestamp;
	hashtablenode*  table;
	unsigned long   size;
};


static inline hashtable* hashtable_init(unsigned long ldN)
/*
 * Allocate space for a hashtable with 2^ldN entries; the range of the hash function must
 * be (contained in) 0..(2^ldN-1).
 */
{
	hashtable* ht;

	ht = (hashtable*)malloc(sizeof(hashtable));
	ht->timestamp = 1UL;
	ht->table = (hashtablenode*)calloc(1<<ldN, sizeof(hashtablenode));
	ht->size=1<<ldN;
	return ht;
}


static inline void hashtable_clear(hashtable* ht)
/*
 * Clear *ht, but don't free the allocated memory.
 */
{
	ht->timestamp++;
#ifdef PRINTHASHTABLESTATS
	if (entries > maxentries) {
		maxentries = entries;
		printf("maximal hash table size: %lu\n", maxentries);
	}
	entries = 0;
#endif
#ifdef VERBOSE
	printf("### hashtable cleared\n");
#endif
}


static inline void hashtable_free(hashtable* ht)
/*
 * Free memory allocated for *ht.
 */
{
	int  i;

	for (i=0; i<ht->size; i++)
		if (ht->table[i].list)
			free(ht->table[i].list);
	free(ht->table);
	free(ht);
}


static inline void ensureListSpace(hashtablenode* node)
/*
 * Make sure that the secondary list has space for one more item.
 */
{
	if (node->slist > node->nlist)
		return;
	node->list = (hashtableentry*)(node->nlist ? realloc(node->list, (node->slist*=2)*sizeof(hashtableentry))
		: malloc((node->slist=ORBITS_HASHTABLE_INITIAL_LIST_SIZE)*sizeof(hashtableentry)));
}


static inline void hashtable_insert_1(hashtable* ht, flags64 key, unsigned long hash, unsigned long data)
/*
 * Insert (key,data) into *ht, where hash is the hash value of key.  It is assumed that *ht does not contain
 * any entry (key,.).
 */
{
	hashtablenode*  node;

#ifdef DOTEST
	if (hash >= ht->size) {
		printf("BAD HASH VALUE [hashtable_insert]: %lu (max %lu)\n", hash, ht->size-1);
		erri(-4);
	}
#endif
	node = ht->table + hash;
	if (node->timestamp != ht->timestamp) {
		node->timestamp = ht->timestamp;
		node->entry.key0 = key;
		node->entry.data = data;
		node->nlist = 0;
#ifdef VERBOSE
	printf("+++ hashtable insert (%llx, %lu)\n", (unsigned long long)key, data);
#endif
		return;
	}
	ensureListSpace(node);
	node->list[node->nlist].key0 = key;
	node->list[node->nlist].data = data;
	node->nlist++;
#ifdef PRINTHASHTABLESTATS
	entries++;
	if (node->nlist > maxlist) {
		maxlist = node->nlist;
		printf("maximal secondary list size: %lu (key: %llx, hash: %lx); table has %lu entries\n",
				maxlist, (unsigned long long)key, hash, entries);
	}
#endif
#ifdef VERBOSE
	printf("+++ hashtable insert (%llx, %lu) -- secondary: %lu\n", (unsigned long long)key, data, node->nlist);
#endif
}


static inline void hashtable_insert_2(hashtable* ht, flags64 key[2], unsigned long hash, unsigned long data)
/*
 * Insert (key,data) into *ht, where hash is the hash value of key.  It is assumed that *ht does not contain
 * any entry (key,.).
 */
{
	hashtablenode*  node;

#ifdef DOTEST
	if (hash >= ht->size) {
		printf("BAD HASH VALUE [hashtable_insert]: %lx (max %lx)\n", hash, ht->size-1);
		erri(-4);
	}
#endif
	node = ht->table + hash;
	if (node->timestamp != ht->timestamp) {
		node->timestamp = ht->timestamp;
		node->entry.key0 = key[0];
		node->entry.key1 = key[1];
		node->entry.data = data;
		node->nlist = 0;
#ifdef VERBOSE
	printf("+++ hashtable insert (%llx:%llx, %lu)\n", (unsigned long long)key[0], (unsigned long long)key[1], data);
#endif
		return;
	}
	ensureListSpace(node);
	node->list[node->nlist].key0 = key[0];
	node->list[node->nlist].key1 = key[1];
	node->list[node->nlist].data = data;
	node->nlist++;
#ifdef PRINTHASHTABLESTATS
	if (node->nlist > maxlist) {
		entries++;
		maxlist = node->nlist;
		printf("maximal secondary list size: %lu (key: %llx:%llx, hash: %lx); table has %lu entries\n",
				maxlist, (unsigned long long)key[0], (unsigned long long)key[1], hash, entries);
	}
#endif
#ifdef VERBOSE
	printf("+++ hashtable insert (%llx:%llx, %lu) -- secondary: %lu\n",
			(unsigned long long)key[0], (unsigned long long)key[1], data, node->nlist);
#endif
}


static inline bool hashtable_query_1(hashtable* ht, flags64 key, unsigned long hash, unsigned long* data)
/*
 * Return whether *ht contains an entry with the given key, where hash is the hash value of key;
 * if yes, set *data accordingly.
 */
{
	hashtablenode*  node;
	unsigned long   i;

#ifdef DOTEST
	if (hash >= ht->size) {
		printf("BAD HASH VALUE [hashtable_query]: %lx (max %lx)\n", hash, ht->size-1);
		erri(-4);
	}
#endif
	node = ht->table + hash;
	if (node->timestamp != ht->timestamp)
		return FALSE;
	if (node->entry.key0 == key) {
		*data = node->entry.data;
#ifdef VERBOSE
	printf("--- hashtable query %llx => %lu\n", (unsigned long long)key, *data);
#endif
		return TRUE;
	}
	for (i=0; i<node->nlist; i++)
		if (node->list[i].key0 == key) {
			*data = node->list[i].data;
#ifdef VERBOSE
	printf("--- hashtable query %llx => %lu -- secondary: %lu\n", (unsigned long long)key, *data, node->nlist);
#endif
			return TRUE;
		}
#ifdef VERBOSE
	printf("--- hashtable query %llx => ---\n", (unsigned long long)key);
#endif
	return FALSE;
}


static inline bool hashtable_query_2(hashtable* ht, flags64 key[2], unsigned long hash, unsigned long* data)
/*
 * Return whether *ht contains an entry with the given key, where hash is the hash value of key;
 * if yes, set *data accordingly.
 */
{
	hashtablenode*  node;
	unsigned long   i;

#ifdef DOTEST
	if (hash >= ht->size) {
		printf("BAD HASH VALUE [hashtable_query]: %lx (max %lx)\n", hash, ht->size-1);
		erri(-4);
	}
#endif
	node = ht->table + hash;
	if (node->timestamp != ht->timestamp)
		return FALSE;
	if (node->entry.key0 == key[0] && node->entry.key1 == key[1]) {
		*data = node->entry.data;
#ifdef VERBOSE
	printf("--- hashtable query %llx:%llx => %lu\n", (unsigned long long)key[0], (unsigned long long)key[1], *data);
#endif
		return TRUE;
	}
	for (i=0; i<node->nlist; i++)
		if (node->list[i].key0 == key[0] && node->list[i].key1 == key[1]) {
			*data = node->list[i].data;
#ifdef VERBOSE
	printf("--- hashtable query %llx:%llx => %lu -- secondary: %lu\n",
			(unsigned long long)key[0], (unsigned long long)key[1], *data, node->nlist);
#endif
			return TRUE;
		}
#ifdef VERBOSE
	printf("--- hashtable query %llx:%llx => ---\n", (unsigned long long)key[0], (unsigned long long)key[1]);
#endif
	return FALSE;
}


static inline bool hashtable_query_insert_1(hashtable* ht, flags64 key, unsigned long hash, unsigned long* data)
/*
 * Return whether *ht contains an entry with the given key, where hash is the hash value of key;
 * if no, insert (key,*data) into *ht; if yes, change the value of *data accordingly.
 */
{
	hashtablenode*  node;
	unsigned long   i;

#ifdef DOTEST
	if (hash >= ht->size) {
		printf("BAD HASH VALUE [hashtable_query]: %lx (max %lx)\n", hash, ht->size-1);
		erri(-4);
	}
#endif
	node = ht->table + hash;
	if (node->timestamp != ht->timestamp) {
		node->timestamp = ht->timestamp;
		node->entry.key0 = key;
		node->entry.data = *data;
		node->nlist = 0;
#ifdef PRINTHASHTABLESTATS
		entries++;
#endif
#ifdef VERBOSE
		printf("+++ hashtable query/insert (%llx, %lu)\n", (unsigned long long)key, *data);
#endif
		return FALSE;
	}
	if (node->entry.key0 == key) {
		*data = node->entry.data;
#ifdef VERBOSE
	printf("--- hashtable query/insert %llx => %lu\n", (unsigned long long)key, *data);
#endif
		return TRUE;
	}
	for (i=0; i<node->nlist; i++)
		if (node->list[i].key0 == key) {
			*data = node->list[i].data;
#ifdef VERBOSE
	printf("--- hashtable query/insert %llx => %lu -- secondary: %lu\n", (unsigned long long)key, *data, node->nlist);
#endif
			return TRUE;
		}
	ensureListSpace(node);
	node->list[node->nlist].key0 = key;
	node->list[node->nlist].data = *data;
	node->nlist++;
#ifdef PRINTHASHTABLESTATS
	entries++;
	if (node->nlist > maxlist) {
		maxlist = node->nlist;
		printf("maximal secondary list size: %lu (key: %llx, hash: %lx); table has %lu entries\n",
				maxlist, (unsigned long long)key, hash, entries);
	}
#endif
#ifdef VERBOSE
	printf("+++ hashtable query/insert (%llx, %lu) -- secondary: %lu\n", (unsigned long long)key, *data, node->nlist);
#endif
	return FALSE;
}


static inline bool hashtable_query_insert_2(hashtable* ht, flags64 key[2], unsigned long hash, unsigned long* data)
/*
 * Return whether *ht contains an entry with the given key, where hash is the hash value of key;
 * if no, insert (key,*data) into *ht; if yes, change the value of *data accordingly.
 */
{
	hashtablenode*  node;
	unsigned long   i;

#ifdef DOTEST
	if (hash >= ht->size) {
		printf("BAD HASH VALUE [hashtable_query]: %lx (max %lx)\n", hash, ht->size-1);
		erri(-4);
	}
#endif
	node = ht->table + hash;
	if (node->timestamp != ht->timestamp) {
		node->timestamp = ht->timestamp;
		node->entry.key0 = key[0];
		node->entry.key1 = key[1];
		node->entry.data = *data;
		node->nlist = 0;
#ifdef PRINTHASHTABLESTATS
		entries++;
#endif
#ifdef VERBOSE
		printf("+++ hashtable query/insert (%llx:%llx, %lu)\n", (unsigned long long)key[0], (unsigned long long)key[1], *data);
#endif
		return FALSE;
	}
	if (node->entry.key0 == key[0] && node->entry.key1 == key[1]) {
		*data = node->entry.data;
#ifdef VERBOSE
	printf("--- hashtable query/insert %llx:%llx => %lu\n", (unsigned long long)key[0], (unsigned long long)key[1], *data);
#endif
		return TRUE;
	}
	for (i=0; i<node->nlist; i++)
		if (node->list[i].key0 == key[0] && node->list[i].key1 == key[1]) {
			*data = node->list[i].data;
#ifdef VERBOSE
	printf("--- hashtable query/insert %llx:%llx => %lu -- secondary: %lu\n",
			(unsigned long long)key[0], (unsigned long long)key[1], *data, node->nlist);
#endif
			return TRUE;
		}
	ensureListSpace(node);
	node->list[node->nlist].key0 = key[0];
	node->list[node->nlist].key1 = key[1];
	node->list[node->nlist].data = *data;
	node->nlist++;
#ifdef PRINTHASHTABLESTATS
	entries++;
	if (node->nlist > maxlist) {
		maxlist = node->nlist;
		printf("maximal secondary list size: %lu (key: %llx:%llx, hash: %lx); table has %lu entries\n",
				maxlist, (unsigned long long)key[0], (unsigned long long)key[1], hash, entries);
	}
#endif
#ifdef VERBOSE
	printf("+++ hashtable query/insert (%llx:%llx, %lu) -- secondary: %lu\n",
			(unsigned long long)key[0], (unsigned long long)key[1], *data, node->nlist);
#endif
	return FALSE;
}


#endif /* HASHTABLE_H_ */
