# README #

This repository contains the implementation of the algorithm for constructing unlabelled lattices that is described in the paper [V. Gebhardt, S. Tawn: Constructing unlabelled lattices, J. Algebra 545 (2020), 213-236](http://arxiv.org/abs/1609.08255).  Please cite the above paper when publishing work that uses this code or results obtained with it.

Contact:  v.gebhardt@westernsydney.edu.au


## BUILD INSTRUCTIONS ##

A basic Makefile is provided; adjust to your needs as required.  Additional flags can be defined by setting the variable CFLAGS; for example, the command for running make with the symbol TARGET_COUNT defined is "make CFLAGS=-DTARGET_COUNT".


### BUILD TARGETS ###

Exactly one build target must be specified by defining one of the following symbols:

- TARGET_COUNT -- The number of lattices is printed to stdout.
- TARGET_CATALOGUE -- A line encoding the above-diagonal part of the covering matrix is printed to stdout for each lattice.


### Modifiers ###

The behaviour of the enumeration can be determined at compile time by defining the following symbols:

- THREADED -- Multithreaded enumeration using pthreads.  (The master initially enumerates lattices up to size N-DELTAN; each of these lattices is passed to a slave.  Once slaves are idle, the enumeration is completed in a second stage in which the master enumerates lattices up to size N-DELTAN+DDELTAN.)
- FILTER_INDECOMPOSABLE -- Only vertically indecomposable lattices are enumerated.
- FILTER_GRADED -- Only graded lattices are enumerated.
