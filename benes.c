/*
 * benes.c
 *
 * Created on: 8 Jun 2015
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include "benes.h"


benes***  benes_small = 0;  /* global storage for precomputed Beneš networks */


static inline elt ceil_ld(elt x)
/*
 * Return \lceil \log_2(x) \rceil, where 0 < x < 32, and return 0 for x \eq 0; this is the correct
 * semantics for benes_create.
 */
{
	if (x < 9) {
		if (x < 3) {
			if (x < 2)
				return 0;  /* x \le 1 */
			else
				return 1;  /* x \eq 2 */
		} else {
			if (x < 5)
				return 2;  /* x \in {3,4} */
			else
				return 3;  /* x \in {5,..,8} */
		}
	} else {
		if (x < 17)
			return 4;  /* x \in {9,..,16} */
		else
			return 5;  /* x \in {17,..,32} */
	}
}


benes* benes_create(elt n, permutation src, permutation inv_src)
/*
 * Return a newly created Beneš network (carrying a reference count) realising as many copies of the
 * action of the permutation p on n points as fit into one flags64.
 * NOTE: src AND inv_src ARE MODIFIED.
 */
{
	benes*       B;
	permutation  tgt, inv_tgt;
	elt          i, ld_n, stage, main_idx, src_idx, tgt_idx, idx2, t, Fpos, Bpos;
	flags64      mask, nmask, src_set, cfg_src, cfg_tgt;

#ifdef DOTEST
	permutation  p;
	perm_cpy(n, src, p);
#endif

	B = (benes*)calloc(1, sizeof(struct benes_p1));
	B->refcount = 1;
	ld_n = ceil_ld(n);
	Fpos = 0;
	Bpos = 2*ld_n-1;
	perm_init(n, tgt);
	perm_init(n, inv_tgt);
#ifdef BENESVERBOSE
	printf("=== entering benes_create for ");
	perm_print(n, src, 0);
#endif
	for (stage=0,mask=1; stage<ld_n; stage++,mask<<=1) {
		nmask = ~mask;
#ifdef BENESVERBOSE
		printf("+++ stage %d, mask=%llx\n", stage, mask);
#endif
		src_set = cfg_src = cfg_tgt = 0;
		for (main_idx=BIT(ld_n); main_idx--;) { /* this ensures that no crossings contain padded bits */
			if (main_idx & mask) { /* loop over pairs... */
				/* ... upper representative */
				src_idx = main_idx;
				while (!(BIT(src_idx) & src_set)) {
					src_set |=  BIT(src_idx); /* ...mark that source index as seen... */
#ifdef BENESVERBOSE
					printf(">>> [u] src_idx: %d", src_idx);
#endif
					tgt_idx = src_idx < n ? inv_tgt[src[src_idx]] : src_idx; /* tgt_idx maps to src_idx */
#ifdef BENESVERBOSE
					printf(" --> tgt_idx: %d", tgt_idx);
#endif
					if ((src_idx^tgt_idx) & mask) {
						/* cross (at the endpoint, to preserve path constructed so far) */
#ifdef BENESVERBOSE
						printf(" CROSS\n");
#endif
						cfg_tgt |= BIT(tgt_idx & nmask); /* set the appropriate bit in the Beneš network */
						idx2 = tgt_idx ^ mask; /* the other part of the pair; to be swapped */
						t = tgt[tgt_idx];                /* update the target side... */
						tgt[tgt_idx] = tgt[idx2];        /* ...configuration that the... */
						tgt[idx2] = t;                   /* ...next stage sees; */
						inv_tgt[tgt[idx2]] = idx2;       /* the other part of the reordered pair... */
						inv_tgt[tgt[tgt_idx]] = tgt_idx; /* ...on the target side is tgt_idx! */
					} else {
						/* straight */
#ifdef BENESVERBOSE
						printf(" straight\n");
#endif
						tgt_idx ^= mask; /* the other part of the pair on the target side */
					}
#ifdef BENESVERBOSE
					printf(">>> tgt_idx: %d", tgt_idx);
#endif
					src_idx = tgt_idx < n ? inv_src[tgt[tgt_idx]] : tgt_idx; /* src_idx maps to tgt_idx */
#ifdef BENESVERBOSE
					printf(" --> src_idx: %d", src_idx);
#endif
					if ((src_idx^tgt_idx) & mask) {
						/* cross (at the endpoint, to preserve path constructed so far) */
#ifdef BENESVERBOSE
						printf(" CROSS\n");
#endif
						cfg_src |= BIT(src_idx & nmask); /* set the appropriate bit in the Beneš network */
						idx2 = src_idx ^ mask; /* the other part of the pair; to be swapped */
						src_set |= BIT(idx2);            /* mark that source index as seen... */
						t = src[src_idx];                /* ...and update the source side */
						src[src_idx] = src[idx2];        /* ...configuration that the */
						src[idx2] = t;                   /* ...next stage sees; */
						inv_src[src[idx2]] = idx2;       /* the other part of the reordered pair... */
						inv_src[src[src_idx]] = src_idx; /* ...on the source side is src_idx! */
					} else {
						/* straight */
#ifdef BENESVERBOSE
						printf(" straight\n");
#endif
						src_set |= BIT(src_idx); /* mark that source index as seen */
						src_idx ^= mask; /* the other part of the pair on the source side */
					}
				}
				/* ... lower representative */
				src_idx = main_idx ^ mask;
				while (!(BIT(src_idx) & src_set)) {
					src_set |=  BIT(src_idx); /* ...mark that source index as seen... */
#ifdef BENESVERBOSE
					printf(">>> [l] src_idx: %d", src_idx);
#endif
					tgt_idx = src_idx < n ? inv_tgt[src[src_idx]] : src_idx; /* tgt_idx maps to src_idx */
#ifdef BENESVERBOSE
					printf(" --> tgt_idx: %d", tgt_idx);
#endif
					if ((src_idx^tgt_idx) & mask) {
						/* cross (at the endpoint, to preserve path constructed so far) */
#ifdef BENESVERBOSE
						printf(" CROSS\n");
#endif
						cfg_tgt |= BIT(tgt_idx & nmask); /* set the appropriate bit in the Beneš network */
						idx2 = tgt_idx ^ mask; /* the other part of the pair; to be swapped */
						t = tgt[tgt_idx];                /* update the target side... */
						tgt[tgt_idx] = tgt[idx2];        /* ...configuration that the... */
						tgt[idx2] = t;                   /* ...next stage sees; */
						inv_tgt[tgt[idx2]] = idx2;       /* the other part of the reordered pair... */
						inv_tgt[tgt[tgt_idx]] = tgt_idx; /* ...on the target side is tgt_idx! */
					} else {
						/* straight */
#ifdef BENESVERBOSE
						printf(" straight\n");
#endif
						tgt_idx ^= mask; /* the other part of the pair on the target side */
					}
#ifdef BENESVERBOSE
					printf(">>> tgt_idx: %d", tgt_idx);
#endif
					src_idx = tgt_idx < n ? inv_src[tgt[tgt_idx]] : tgt_idx; /* src_idx maps to tgt_idx */
#ifdef VBENESERBOSE
					printf(" --> src_idx: %d", src_idx);
#endif
					if ((src_idx^tgt_idx) & mask) {
						/* cross (at the endpoint, to preserve path constructed so far) */
#ifdef BENESVERBOSE
						printf(" CROSS\n");
#endif
						cfg_src |= BIT(src_idx & nmask); /* set the appropriate bit in the Beneš network */
						idx2 = src_idx ^ mask; /* the other part of the pair; to be swapped */
						src_set |= BIT(idx2);            /* mark that source index as seen... */
						t = src[src_idx];                /* ...and update the source side */
						src[src_idx] = src[idx2];        /* ...configuration that the */
						src[idx2] = t;                   /* ...next stage sees; */
						inv_src[src[idx2]] = idx2;       /* the other part of the reordered pair... */
						inv_src[src[src_idx]] = src_idx; /* ...on the source side is src_idx! */
					} else {
						/* straight */
#ifdef BENESVERBOSE
						printf(" straight\n");
#endif
						src_set |= BIT(src_idx); /* mark that source index as seen */
						src_idx ^= mask; /* the other part of the pair on the source side */
					}
				}
			}
		}
		/* record the configurations for the current stage */
		if (cfg_src) {
			flags64  smask;
			B->shift[Fpos] = BIT(stage);
			smask = 0;
			t = n;
			for (i=BITSPERFLAGS64/n; i--;) {
				smask |= cfg_src;
				cfg_src = (cfg_src | (cfg_src << t)) << t;
				t <<= 1;
			}
			B->mask[Fpos] = smask;
			Fpos++;
		}
		if (cfg_tgt) {
			flags64  smask;
			Bpos--;
			B->shift[Bpos] = BIT(stage);
			smask = 0;
			t = n;
			for (i=BITSPERFLAGS64/n; i--;) {
				smask |= cfg_tgt;
				cfg_tgt = (cfg_tgt | (cfg_tgt << t)) << t;
				t <<= 1;
			}
			B->mask[Bpos] = smask;
		}
	}
	/* make sure non-trivial stages are consecutive */
	if (Bpos > Fpos) {
		for (; Bpos<2*ld_n-1; Fpos++,Bpos++) {
			B->shift[Fpos] = B->shift[Bpos];
			B->mask[Fpos] = B->mask[Bpos];
		}
		B->depth = Fpos;
	} else {
		B->depth = 2*ld_n-1;
	}
#ifdef DOTEST
	if (n)
		for (i=0,mask=1UL; i<BITSPERFLAGS64/n; i++,mask<<=1) {
			nmask = mask;
			benes_apply_p1(B, &nmask);
			if (nmask != (1UL << (p[i%n] + n*(i/n)))) {
				printf("BAD BENES NETWORK on level %d: ", i);
				perm_print(n, p, 0);
				for (i=0; i<B->depth; i++)
					printf("(%d,%lx) ", B->shift[i], (unsigned long)B->mask[i]);
				printf("\n");
				erri(-4);
			}
		}
#endif
	return B;
}


static unsigned long factorial(elt n)
/*
 * Return n!.
 */
{
	return n<=1 ? 1 : n*factorial(n-1);
}


void benes_init_small()
/*
 * Precompute Beneš networks for all permutations on at most BENES_SMALL points.
 */
{
	elt          n;
	permutation  p;
	benes*       B;

	benes_small = (benes***)calloc(BENES_SMALL+1, sizeof(benes**));
	for (n=0; n<=BENES_SMALL; n++) {
		benes_small[n] = (benes**)calloc(factorial(n), sizeof(benes*));
		perm_init(n, p);
		do {
			permutation p_, pi_;
			perm_cpy(n, p, p_);
			perm_inv(n, p, pi_);
			B = benes_create(n, p_, pi_);
			B->refcount = -1;
			benes_small[n][perm_toInteger(n,p)] = B;
		} while (perm_next(1, &n, p));  /* a single level: 0..n-1 */
	}
}


void benes_free_small()
/*
 * Free the memory allocated for the precomputed Beneš networks for all permutations
 * on at most BENES_SMALL points.
 */
{
	elt            n;
	unsigned long  i;

	if (benes_small) {
		for (n=0; n<=BENES_SMALL; n++) {
			for (i=0; i<factorial(n); i++)
				free(benes_small[n][i]);
			free(benes_small[n]);
		}
		free(benes_small);
		benes_small = 0;
	}
}
