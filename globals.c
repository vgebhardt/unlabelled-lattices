/*
 * globals.c
 *
 * Created on: 11 Jul 2015
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#include "globals.h"


void globals_init(globals* GD)
/*
 * Initialise storage space for orbit/stabiliser computation.
 */
{
	GD->orbspace = ORBITS_INITIAL_ORBSPACE;
	GD->orbsize = 0;
	GD->orb = (orbitelt*)malloc(ORBITS_INITIAL_ORBSPACE*sizeof(orbitelt));
	GD->orbpos = hashtable_init(ORBITS_HASHTABLE_LD_SIZE);
	GD->SIspace = ORBITS_INITIAL_SISPACE;
	GD->SI0 = (SIdata*)malloc(ORBITS_INITIAL_SISPACE*sizeof(SIdata));
	GD->SI1 = (SIdata*)malloc(ORBITS_INITIAL_SISPACE*sizeof(SIdata));
}


void globals_free(globals* GD)
/*
 * Free allocated storage space for orbit/stabiliser computation.
 */
{
    free(GD->orb);
	hashtable_free(GD->orbpos);
	free(GD->SI0);
	free(GD->SI1);
}
