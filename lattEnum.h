/*
 * lattEnum.h
 *
 * Created on: 22 Mar 2014
 * Last modified: 23 Feb 2019
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2019 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef LATTENUM_H_
#define LATTENUM_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "lattice.h"


/*
 * "The C99 standard permits you to cast any struct pointer to a pointer to its first member, and the other
 *  way (6.7.2.1 Structure and union specifiers):
 *     13 Within a structure object, the non-bit-field members and the units in which bit-fields reside have
 *     addresses that increase in the order in which they are declared. A pointer to a structure object, suitably
 *     converted, points to its initial member (or if that member is a bit-field, then to the unit in which it
 *     resides), and vice versa. There may be unnamed padding within a structure object, but not at its beginning.
 */
typedef struct lattEnum lattEnum;
typedef lattEnum lattEnum_Count;
struct lattEnum {
	void (*reg)(lattEnum* E, lattice* L);  /* function called for registering lattices */
	globals*            GD;                /* "global" data for process/thread         */
	lattice*            L;                 /* lattice whose descendants we enumerate   */
	unsigned long long  count;             /* number of lattices found                 */
	elt                 N;                 /* target size                              */
	elt                 Nmin;              /* minimum size for recursion               */
#ifdef COUNT_MAXIMAL_CHAINS
    unsigned int        max_mc;
#endif
};


typedef struct lattEnum_stdout lattEnum_stdout;
struct lattEnum_stdout {
	void (*reg)(lattEnum* E, lattice* L);  /* function called for registering lattices */
	globals*            GD;                /* "global" data for process/thread         */
	lattice*            L;                 /* lattice whose descendants we enumerate   */
	unsigned long long  count;             /* number of lattices found                 */
	elt                 N;                 /* target size                              */
	elt                 Nmin;              /* minimum size for recursion               */
#ifdef THREADED
	char*               output;            /* output buffer                            */
	int                 outpos;            /* write position in output buffer          */
	int                 outsize;           /* current size of output buffer            */
#endif
};


lattEnum* lattEnum_Count_create(lattice* L, elt N, elt Nmin, globals* GD);
/*
 * Return a structure for counting the descendants of *L of size equal to N, for which all intermediate lattices
 * have size at most Nmin.
 */


lattEnum* lattEnum_stdout_create(lattice* L, elt N, elt Nmin, globals* GD);
/*
 * Return a structure for writing string representations of the descendants of *L of size N to stdout, for
 * which all intermediate lattices have size at most Nmin..
 */


void lattEnum_stdout_flush(lattEnum_stdout* E);
/*
 * Flush the output buffer.
 */


void lattEnum_Count_register(lattEnum* E, lattice* L);
/*
 * Register the lattice L.
 */


void lattEnum_stdout_register(lattEnum* E, lattice* L);
/*
 * Register the lattice L.
 */


void lattEnum_Count_free(lattEnum* E);
/*
 * Free all memory (and print the maximal number of maximal chains if enabled).
 */


void lattEnum_stdout_free(lattEnum* E);
/*
 * Free all memory (and flush the output buffer).
 */


static inline unsigned long long lattEnum_getLatticeCount(lattEnum* E)
/*
 * Return the number of registered lattices.
 */
{
	return E->count;
}


void lattEnum_growLattice(lattEnum* E, elt N, lattice* L, elt nmin);
/*
 * Construct canonical lattices with N elements by recursively adding new levels to the canonical lattice L,
 * where the first added level contains at least nmin elements.  For every canonical lattice found during
 * this process (also for those with fewer than N elements), the function *reg is called.
 *
 * The function assumes that *L contains at least one non-extremal element.
 */


void lattEnum_growLattice_2(lattEnum* E, elt N, lattice* L, elt nmin);
/*
 * Construct canonical lattices with N elements by recursively adding new levels to the lattice L with 2
 * elements, where the first added level contains at least nmin elements.  For every canonical lattice
 * found during this process (also for those with fewer than N elements), the function *reg is called.
 *
 * The function assumes that *L is the lattice with 2 elements.
 */


static inline void lattEnum_doEnumeration(lattEnum* E)
/*
 * Do the enumeration.
 */
{
	(*(E->reg))(E, E->L);
#if defined(FILTER_INDECOMPOSABLE) && defined(FILTER_GRADED)
	if (E->L->n < E->N-1) {
		if (E->L->n)
			lattEnum_growLattice(E, E->N, E->L, (E->Nmin)-(E->L->n)>1 ? (E->Nmin)-(E->L->n) : 2);
		else
			lattEnum_growLattice_2(E, E->N, E->L, (E->Nmin)-(E->L->n)>1 ? (E->Nmin)-(E->L->n) : 2);
	}
#else
	if (E->L->n < E->N) {
		if (E->L->n)
			lattEnum_growLattice(E, E->N, E->L, (E->Nmin)-(E->L->n));
		else
			lattEnum_growLattice_2(E, E->N, E->L, (E->Nmin)-(E->L->n));
	}
#endif
}


#endif /* LATTENUM_H_ */
