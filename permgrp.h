/*
 * permgrp.h
 *
 * Created on: 23 May 2014
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PERMGRP_H_
#define PERMGRP_H_

#include <stdlib.h>

#include "permutation.h"
#include "benes.h"
#include "build.h"
#include "globals.h"


typedef struct permgrp permgrp;
struct permgrp {
	unsigned long  refcount;               /* reference count */
	permutation    perm[MAXN-2];           /* permutations for generators; DATA IS NOT CONSECUTIVE DURING CONSTRUCTION */
	permutation    invperm[MAXN-2];        /* permutations for inverses of generators */
	flags32        invol;                  /* invol & BIT[i] indicates whether generator i is an involution */
	flags32        BenesValid;             /* which levels of array Benes contain valid pointers */
#ifndef FILTER_GRADED
	benes*         Benes[MAXN-2][MAXN-2];  /* *(Benes[i][j]): Beneš network for the action of generator j on level i */
#else
	benes*         Benes[1][MAXN-2];
#endif
	elt            n;                      /* number of points on which the group acts */
	elt            ngens;                  /* number of generators */
};


typedef struct JVertexT JVertexT;
struct JVertexT {
	flags32  neighbours;    /* indicating which vertices are neighbours */
	elt      perm[MAXN-2];  /* if i is a neighbour, perm[i] is the index in perm/invperm for the edge to i */
};


typedef struct permgrpc permgrpc;
struct permgrpc {
	permgrp*   G;
	JVertexT   Jerrum[MAXN-2];  /* graph used for Jerrum's filter */
	flags32    freeperm;        /* indicating unused indices in perm/inverm */
};


static inline permgrp* permgrp_alloc()
/*
 * Allocate space for a permutation group (permgrp); the group is NOT initialised.
 */
{
	permgrp*  G;

	G = (permgrp*)malloc(sizeof(permgrp));
	G->refcount = 1;
	G->BenesValid = 0;
	return G;
}

static inline permgrpc* permgrpc_alloc()
/*
 * Allocate space for a permutation group (permgrpc); the group is NOT initialised.
 */
{
	permgrpc*  G;

	G = (permgrpc*)malloc(sizeof(permgrpc));
	G->G = permgrp_alloc();
	return G;
}


static inline void permgrp_clearBenes(permgrp* G)
/*
 * Delete all Beneš networks stored in *G.
 */
{
	elt  i, j;

	while (extract_LSB32(&(G->BenesValid),&i)) {
		for (j=0; j<G->ngens; j++)
			benes_delete(G->Benes[i][j]);
	}
}


static inline void permgrp_init(permgrp* G, elt n)
/*
 * Set *G to the trivial permutation group on n points.
 */
{
	permgrp_clearBenes(G);
	G->n = n;
	G->ngens = 0;
}


static inline void permgrpc_init(permgrpc* G, elt n)
/*
 * Set G to the trivial permutation group on n points.
 */
{
	elt  i;

	permgrp_init(G->G, n);
	G->freeperm = allBits32(MAXN-2);
	for (i=0; i<n; i++)
		G->Jerrum[i].neighbours = 0;
}


static inline permgrp* permgrp_incref(permgrp* G)
/*
 * Increment the reference count for *G and return *G.
 */
{
	G->refcount++;
	return G;
}


static inline void permgrp_cpy(permgrp* G, permgrp* H)
/*
 * Copy G to H.
 */
{
	elt      i, n;

	permgrp_clearBenes(H);
	H->refcount = 1;
	n = H->n = G->n;
	H->ngens = G->ngens;
	for (i=0; i<n; i++) { /* We need G->n instead of G->ngens for permgrpc_cpy to work in the general case! */
		perm_cpy(n, G->perm[i], H->perm[i]);
		perm_cpy(n, G->invperm[i], H->invperm[i]);
	}
	H->invol = G->invol;
}


static inline void permgrpc_cpy(permgrpc* G, permgrpc* H)
/*
 * Copy G to H.
 */
{
	permgrp_cpy(G->G, H->G);
	H->freeperm = G->freeperm;
	memcpy(H->Jerrum, G->Jerrum, G->G->n*sizeof(JVertexT));
}


static inline permgrp* permgrpc_get_permgrp(permgrpc* G)
/*
 * Return the underlying permgrp of *G WITHOUT REFERENCE COUNT.
 */
{
	return G->G;
}


static inline void permgrp_delete(permgrp* G)
/*
 * Decrement the reference count for *G, and free the allocated memory if the reference count reaches 0.
 */
{
	if (!(--(G->refcount))) {
		permgrp_clearBenes(G);
		free(G);
	}
}


static inline void permgrpc_delete(permgrpc* G)
/*
 * Delete the group *G.
 */
{
	permgrp_delete(G->G);
	free(G);
}


void permgrpc_addGenerator(permgrpc* G, permutation p);
/*
 * Add the permutation p as a generator of G.  The permutation p *must* be nontrivial!
 */


void permgrp_printGenerators(permgrp* G, elt offset);
/*
 * TEST FUNCTION:  Print current generators (in array notation) with first point being offset.
 */


static inline void permgrpc_printGenerators(permgrpc* G, elt offset)
/*
 * TEST FUNCTION:  Print current generators (in array notation).  NOTE:  The function only works if the
 * generators are stored consecutively, i.e., after permgrpc_compactGenerators has been called.
 */
{
	permgrp_printGenerators(G->G, offset);
}


#endif /* PERMGRP_H_ */
