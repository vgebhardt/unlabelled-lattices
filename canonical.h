/*
 * canonical.h
 *
 * Created on: 1 Jun 2014
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef CANONICAL_H_
#define CANONICAL_H_

#include "antichain.h"


#ifdef TARGET_UTEST_SI
void antichainList_applySI_p1_f(elt n, elt k, elt a0, elt m, flags32 bl, globals* GD, flags64* L, flags32 SI,
		flags64 M, permutation p);
/*
 * Non-static wrapper for antichainList_applySI_p1.
 */
#endif


bool antichaindata_isCanonical_1(antichaindata* AD);
/*
 * Whether the antichain data given by AD may yield a canonical antichain list.  Special case of a single
 * antichain.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The function checks whether the multiset of sets chosen on the current level (AD->O[...] & AD->cmc) is
 * minimal under the action of the current stabiliser (AD->ST[AD->cl+1]).  If yes, and if the antichain
 * data is not complete (that is, AD->cl > 0]), then the new stabiliser is stored in AD->ST[AD->cl].  If
 * the antichain data is complete and canonical, AD->ST[0] will instead be set to the stabiliser of the
 * new lattice obtained from the antichain data.  (The points on the lowest (new) level of the new lattice
 * correspond to the antichains in that sequence; if a permutation fixes the multiset but permutes its
 * elements, the points of the lowest level of the new lattice need to be permuted accordingly to obtain
 * an element of the stabiliser of the new lattice.  Additional generators of the stabiliser of the new
 * lattice are given by all possible permutations of identical antichains, i.e., all possible permutations
 * of points of the lowest level of the new lattice that have identical covering sets.)
 */


bool antichaindata_isCanonical_p1(antichaindata* AD, elt bits);
/*
 * Whether the antichain data given by AD may yield a canonical antichain list.  Special case of a list
 * that, if packed, fits into one flags64.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The function checks whether the multiset of sets chosen on the current level (AD->O[...] & AD->cmc) is
 * minimal under the action of the current stabiliser (AD->ST[AD->cl+1]).  If yes, and if the antichain
 * data is not complete (that is, AD->cl > 0]), then the new stabiliser is stored in AD->ST[AD->cl].  If
 * the antichain data is complete and canonical, AD->ST[0] will instead be set to the stabiliser of the
 * new lattice obtained from the antichain data.  (The points on the lowest (new) level of the new lattice
 * correspond to the antichains in that sequence; if a permutation fixes the multiset but permutes its
 * elements, the points of the lowest level of the new lattice need to be permuted accordingly to obtain
 * an element of the stabiliser of the new lattice.  Additional generators of the stabiliser of the new
 * lattice are given by all possible permutations of identical antichains, i.e., all possible permutations
 * of points of the lowest level of the new lattice that have identical covering sets.)
 */


bool antichaindata_isCanonical_p2(antichaindata* AD, elt bits);
/*
 * Whether the antichain data given by AD may yield a canonical antichain list.  Special case of a list
 * that, if packed, fits into (and requires) two flags64.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The function checks whether the multiset of sets chosen on the current level (AD->O[...] & AD->cmc) is
 * minimal under the action of the current stabiliser (AD->ST[AD->cl+1]).  If yes, and if the antichain
 * data is not complete (that is, AD->cl > 0]), then the new stabiliser is stored in AD->ST[AD->cl].  If
 * the antichain data is complete and canonical, AD->ST[0] will instead be set to the stabiliser of the
 * new lattice obtained from the antichain data.  (The points on the lowest (new) level of the new lattice
 * correspond to the antichains in that sequence; if a permutation fixes the multiset but permutes its
 * elements, the points of the lowest level of the new lattice need to be permuted accordingly to obtain
 * an element of the stabiliser of the new lattice.  Additional generators of the stabiliser of the new
 * lattice are given by all possible permutations of identical antichains, i.e., all possible permutations
 * of points of the lowest level of the new lattice that have identical covering sets.)
 */


static inline bool antichaindata_isCanonical(antichaindata* AD)
/*
 * Whether the antichain data given by AD may yield a canonical antichain list.  Wrapper which dispatches
 * the work to antichaindata_isCanonical_p1 or antichaindata_isCanonical_p2.
 *
 * An antichain list is canonical if and only if, as a multiset, it is minimal in its orbit under the
 * stabiliser of the (full) old lattice.  As this stabiliser acts on each level, a violation of minimality
 * on any level precludes any completion of the antichain data on higher levels being canonical.
 *
 * The function checks whether the multiset of sets chosen on the current level (AD->O[...] & AD->cmc) is
 * minimal under the action of the current stabiliser (AD->ST[AD->cl+1]).  If yes, and if the antichain
 * data is not complete (that is, AD->cl > 0]), then the new stabiliser is stored in AD->ST[AD->cl].  If
 * the antichain data is complete and canonical, AD->ST[0] will instead be set to the stabiliser of the
 * new lattice obtained from the antichain data.  (The points on the lowest (new) level of the new lattice
 * correspond to the antichains in that sequence; if a permutation fixes the multiset but permutes its
 * elements, the points of the lowest level of the new lattice need to be permuted accordingly to obtain
 * an element of the stabiliser of the new lattice.  Additional generators of the stabiliser of the new
 * lattice are given by all possible permutations of identical antichains, i.e., all possible permutations
 * of points of the lowest level of the new lattice that have identical covering sets.)
 */
{
	elt  bits;

	bits = AD->L->lev[AD->cl+1] - AD->L->lev[AD->cl];
	if (AD->k*bits <= BITSPERFLAGS64)
		return antichaindata_isCanonical_p1(AD, bits);
	else
		return antichaindata_isCanonical_p2(AD, bits);
}


#endif
