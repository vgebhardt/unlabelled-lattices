/*
 * globals.h
 *
 * Created on: 11 Jul 2015
 * Last modified: 24 Jan 2018
 * Author: Volker Gebhardt, v.gebhardt@westernsydney.edu.au
 *
 *   Copyright (C) 2015-2018 by Volker Gebhardt
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef GLOBALS_H_
#define GLOBALS_H_


#include "hashtable.h"
#include "permutation.h"
#include "benes.h"
#include "build.h"


typedef struct orbitelt orbitelt;
struct orbitelt {
	flags64        data[2]; /* the antichains */
	permutation    toRoot;  /* permutation to root */
	elt            gen;     /* index of generator whose application yielded the antichains */
};


typedef struct SIdata SIdata;
struct SIdata {
	flags64        rep[2];  /* the partially resorted antichains */
	flags32        S;       /* S & BIT[j] indicates that the elements j-1 and j are in the same subset of the partition */
	permutation    p;       /* initial permutation right-multiplied by the performed reordering of elements and antichains */
};


typedef struct globals globals;
struct globals {
	unsigned long    orbspace;
	unsigned long    orbsize;
	orbitelt*        orb;
	unsigned long    SIspace;
	unsigned long    SI0size;
	SIdata*          SI0;
	SIdata*          SI1;
	hashtable*       orbpos;
};


void globals_init(globals* GD);
/*
 * Initialise storage space for orbit/stabiliser computation.
 */


static inline void globals_enlargen_orbitspace(globals* GD)
/*
 * Double the number of orbit elements that can be stored.
 */
{
	GD->orbspace *= 2;
	GD->orb = (orbitelt*)realloc(GD->orb, (GD->orbspace)*sizeof(orbitelt));
}


static inline void globals_enlargen_SIspace(globals* GD)
/*
 * Double the number of SI data recordss that can be stored.
 */
{
	GD->SIspace *= 2;
	GD->SI0 = (SIdata*)realloc(GD->SI0, (GD->SIspace)*sizeof(SIdata));
	GD->SI1 = (SIdata*)realloc(GD->SI1, (GD->SIspace)*sizeof(SIdata));
}


void globals_free(globals* GD);
/*
 * Free allocated storage space for orbit/stabiliser computation.
 */


#endif /* GLOBALS_H_ */
